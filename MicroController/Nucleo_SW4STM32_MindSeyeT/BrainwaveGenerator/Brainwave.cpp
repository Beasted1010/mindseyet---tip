#include <mbed.h>
#include "Brainwave.h"
#include <arm_math.h>

#define FB Frequency_band

//#define DOMINANT_MAGNITUDE 5
//#define SECONDARY_MAGNITUDE 3
#define DOMINANT_MAGNITUDE 100
#define SECONDARY_MAGNITUDE 50


//#define DOMINANT_AMPLITUDE_VARIANCE  2
//#define SECONDARY_AMPLITUDE_VARIANCE 1
#define DOMINANT_AMPLITUDE_VARIANCE  30
#define SECONDARY_AMPLITUDE_VARIANCE 10


Brainwave::Brainwave( FB::Bands band ) 
            : dominant_band(band), band_type(band), Delta(FB::DELTA, DELTA_LWR_LMT, DELTA_UPPR_LMT), Theta(FB::THETA, THETA_LWR_LMT, THETA_UPPR_LMT)
            , Alpha(FB::ALPHA, ALPHA_LWR_LMT, ALPHA_UPPR_LMT) , Beta(FB::BETA, BETA_LWR_LMT, BETA_UPPR_LMT), Gamma(FB::GAMMA, GAMA_LWR_LMT, GAMA_UPPR_LMT)
{
    if ( band == FB::DELTA )
        Delta.set_avg_amplitude(DOMINANT_MAGNITUDE);
    else if (band == FB::THETA)
        Theta.set_avg_amplitude(DOMINANT_MAGNITUDE);
    else if (band == FB::ALPHA)
        Alpha.set_avg_amplitude(DOMINANT_MAGNITUDE);
    else if (band == FB::BETA)
        Beta.set_avg_amplitude(DOMINANT_MAGNITUDE);
    else
        Gamma.set_avg_amplitude(DOMINANT_MAGNITUDE);
    
}

void Brainwave::set_dominant_band( Frequency_band::Bands band )
{
	band_type = band;

	switch( band )
	{
		case FB::Bands::DELTA:
		{
			dominant_band = Delta;
		} break;

		case FB::Bands::THETA:
		{
			dominant_band = Theta;
		} break;

		case FB::Bands::ALPHA:
		{
			dominant_band = Alpha;
		} break;

		case FB::Bands::BETA:
		{
			dominant_band = Beta;
		} break;

		case FB::Bands::GAMMA:
		{
			dominant_band = Gamma;
		} break;

		default: // Communicating intent with default block, compiler generated code should be same as if I didn't put this here (since empty) -> Source: https://stackoverflow.com/questions/8021321/what-if-i-dont-write-default-in-switch-case/25384606
		{
			// No default, possible future error handling
		} break;
	}
}

void Brainwave::simulation(uint16_t full_cycle_length, uint16_t signal_resolution_per_second, float stored_val[])
{
    for(int i=0;i< full_cycle_length;i++)
    {
        stored_val[i]=0; // reset previous values, otherwise they keep growing
        float t = (float) i / signal_resolution_per_second;
        for ( int freq =0; freq < NUM_OF_FREQUENCIES; freq++)
        {
            stored_val[i] += ((i < 40) ? i*0.01 : 1)*get_freq_amplitude(freq)*sin(freq*t*2*PI); // (i < 20) ? i*0.02 : 1) is a ramp saturating at 20 
        }
    }
}


int Brainwave::get_freq_amplitude(int freq)
{
    if ( (dominant_band.lower_limit <= freq) && (freq <= dominant_band.upper_limit) ) // _band.get_upper_limit()
        return  rand() % DOMINANT_AMPLITUDE_VARIANCE  + DOMINANT_MAGNITUDE;
    else
        return  rand() % SECONDARY_AMPLITUDE_VARIANCE + SECONDARY_MAGNITUDE;
}

void Brainwave::print_current_brainwave(Serial* serial_printer)
{
	const char* current_brainwave;

	switch( band_type )
	{
		case FB::Bands::DELTA:
		{
			current_brainwave = "Delta";
		} break;

		case FB::Bands::THETA:
		{
			current_brainwave = "Theta";
		} break;

		case FB::Bands::ALPHA:
		{
			current_brainwave = "Alpha";
		} break;

		case FB::Bands::BETA:
		{
			current_brainwave = "Beta";
		} break;

		case FB::Bands::GAMMA:
		{
			current_brainwave = "Gamma";
		} break;

		default: // Communicating intent with default block, compiler generated code should be same as if I didn't put this here (since empty) -> Source: https://stackoverflow.com/questions/8021321/what-if-i-dont-write-default-in-switch-case/25384606
		{
			// No default, possible future error handling
			current_brainwave = "ERROR, no current brainwave";
		} break;
	}

	serial_printer->printf("-----------------\n");
	serial_printer->printf("Current Brainwave: %s\n", current_brainwave);
	serial_printer->printf("-----------------\n");
}

