#ifndef BRAINWAVE_H
#define BRAINWAVE_H

#define NUM_OF_FREQUENCIES 50   // considering from 1 to 50Hz

// Now using those defined in the main file (named FFT_SIZE = FULL_CYCLE_LENGTH and SAMPLING_RATE_HZ = SIGNAL_RESOLUTION_PER_SECOND)
//#define FULL_CYCLE_LENGTH  256
//#define SIGNAL_RESOLUTION_PER_SECOND  300 // = (fundamental_frq * number_of_samples)*k where k is an integer with minimum 1

#include <mbed.h>

#include "Frequency_band.h"

class Brainwave
{
    public:
		Frequency_band dominant_band;
		Frequency_band::Bands band_type;

        Brainwave( Frequency_band::Bands band );   // create brainwave according to the selected state/band

        void simulation(uint16_t full_cycle_length, uint16_t signal_resolution, float stored_val[]);

        void set_dominant_band( Frequency_band::Bands band );
        int get_freq_amplitude( int freq );

        void print_current_brainwave( Serial* serial_printer );

    private:
        Frequency_band Delta;
        Frequency_band Theta;
        Frequency_band Alpha;
        Frequency_band Beta;
        Frequency_band Gamma;
        
};

#endif
