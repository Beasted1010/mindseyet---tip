#ifndef FREQUENCY_BAND_H
#define FREQUENCY_BAND_H
////////////////////////

#include <stdint.h> // Contains _t types (e.g. uint8_t)

#define DELTA_LWR_LMT  1
#define DELTA_UPPR_LMT 3
#define THETA_LWR_LMT  4
#define THETA_UPPR_LMT 7
#define ALPHA_LWR_LMT  8
#define ALPHA_UPPR_LMT 12
#define BETA_LWR_LMT   13
#define BETA_UPPR_LMT  30
#define GAMA_LWR_LMT   31
#define GAMA_UPPR_LMT  50

//#define AMPLITUDE_LWR_LMT  1
//#define AMPLITUDE_UPPR_LMT 40
#define AMPLITUDE_LWR_LMT  10
#define AMPLITUDE_UPPR_LMT 400

typedef enum {} Bands;

class Frequency_band
{
    public:
		// uint8_t -> 0-255, I'm not expecting these limits to be above 100, so this saves a few bytes per object (as opposed to using int)
		uint8_t lower_limit;
		uint8_t upper_limit;

         typedef enum {
            DELTA = 0, // ~1-3  Hz
            THETA, // ~4-7  Hz
            ALPHA, // ~8-12 Hz
            BETA,  //13-30  Hz
            GAMMA, // 31-50 Hz
            NUMBER_OF_BANDS
        } Bands;

        Frequency_band( Bands band_type, int common_amplitude = 1 );
        Frequency_band( int lower_limit, int upper_limit, int common_amplitude = 1 );

        void set_frequency_band_limits( Bands band );
        void set_avg_amplitude( int amp );

    private:
        int _avg_amplitude;

};

#endif
