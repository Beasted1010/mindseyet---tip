
#include <Frequency_band.h>

Frequency_band::Frequency_band(Bands band_type, int common_amplitude) : _avg_amplitude(common_amplitude)
{
	// Using a switch statement for scalability and speed related reasons.
	// 		I.e. The compiler will create a lookup table for a switch statement and given that all cases are same type the compiler can provide additional aid
	//			 Also, each case is equally likely so the order of execution isn't so important (i.e. switch statements often executed bottom up to first rule out the many possibilities that may fall within the "default" case)
	//																								  if-else chains are a good idea if one case more likely to happen and thus should be first checked.
	//			 If we choose to add more bands in the future this way also allows for cleaner scaling
	//			 Plus the fact that the bands are an enum value lends them nicely to a switch statement
	// Sources: https://www.geeksforgeeks.org/switch-vs-else/ and https://embeddedgurus.com/stack-overflow/2010/04/efficient-c-tip-12-be-wary-of-switch-statements/

	set_frequency_band_limits( band_type );
}

void Frequency_band::set_frequency_band_limits( Bands band )
{

	switch( band )
	{
		case DELTA:
		{
	        upper_limit = DELTA_UPPR_LMT;
	        lower_limit = DELTA_LWR_LMT;
		} break;

		case THETA:
		{
			upper_limit = THETA_UPPR_LMT;
			lower_limit = THETA_LWR_LMT;
		} break;

		case ALPHA:
		{
			upper_limit = ALPHA_UPPR_LMT;
			lower_limit = ALPHA_LWR_LMT;
		} break;

		case BETA:
		{
			upper_limit = BETA_UPPR_LMT;
			lower_limit = BETA_LWR_LMT;
		} break;

		case GAMMA:
		{
			upper_limit = GAMA_UPPR_LMT;
			lower_limit = GAMA_LWR_LMT;
		} break;

		default: // Communicating intent with default block, compiler generated code should be same as if I didn't put this here (since empty) -> Source: https://stackoverflow.com/questions/8021321/what-if-i-dont-write-default-in-switch-case/25384606
		{
			// No default, possible future error handling
		} break;
	}
}

Frequency_band::Frequency_band( int lower_limit, int upper_limit, int avg_amplitude )
                    : lower_limit(lower_limit), upper_limit(upper_limit), _avg_amplitude(avg_amplitude)
{}

void Frequency_band::set_avg_amplitude(int amp)
{
    if ( (AMPLITUDE_LWR_LMT  <= amp) && (amp <= AMPLITUDE_UPPR_LMT) )
        _avg_amplitude = amp;
}

