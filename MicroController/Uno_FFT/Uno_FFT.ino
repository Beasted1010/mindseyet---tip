#include <arduinoFFT.h>
#include <SoftwareSerial.h>

SoftwareSerial BTSerial(10, 11); // Arduino -> (Rx, Tx) - CONNECT BT RX PIN TO ARDUINO 11 PIN (as transmit)| CONNECT BT TX PIN TO ARDUINO 10 PIN

int sensorPin = A0; // This will be connected to the output of the Amplifier
int sensorValue = 0;

/* FFT Parameters

    Source: https://electronics.stackexchange.com/questions/12407/what-is-the-relation-between-fft-length-and-frequency-resolution

   Sampling Frequency, or Sampling Rate: The rate at which we sample data for the FFT.
        By "Nyquist-Shannon" sampling theorem -> To ensure an accurate output, we want (Sampling Rate / 2)    -> ???
   Samples: This is the number of samples we collect for the FFT
        The number of FFT bins we will have is (Samples / 2) -> e.g. 128 samples -> 64 FFT bins

   The Samples paired with the Sampling Frequency provide the bin width
      Bin Width = (Sampling Frequency / 2) / (Number of Samples / 2)

      Basically you can just do -> Sampling Rate / Number of Samples -> Since both numerator and denominator are halved.


   E.g. SAMPLES = 128, SAMPLING_FREQUENCY = 128
        # FFT Bins -> 64 Bins (128 Samples / 2)
        Frequency content -> 64 Hz (128 Sampling Frequency / 2)
        Bin width = (64 / 64) = 1 Hz


*/
//#define SAMPLES 128             //Must be a power of 2
//#define SAMPLES 128             //Must be a power of 2 -> FFT SIZE, I think
//#define SAMPLES 64
//#define SAMPLES 32
//#define SAMPLING_FREQUENCY 128 //Hz, must be less than 10000 due to ADC
//#define SAMPLING_FREQUENCY 64 //Hz, must be less than 10000 due to ADC
//#define SAMPLING_FREQUENCY 150 // 3 times max expected frequency
//#define SAMPLING_FREQUENCY 5000


// TODO: Ensure this holds -> Sampling frequency should be at least TWICE the smallest frequency that is present in underlying continuous signal
const uint16_t SAMPLES = 64;
const double SIGNAL_FREQUENCY = 10;
const double SAMPLING_FREQUENCY = 300;
const uint8_t AMPLITUDE = 100;

#define DELAY 1 // Used to let Arduino breathe a bit between sampling. Can be used independently of DEBUG and PLOT
#define DEBUG 0 // Used to print to Serial -> IF USED, PLOT MUST = 0
#define PLOT 0 // Used to print to Plotter -> IF USED, DEBUG MUST = 0
#define AT_COMMAND_MODE 0 // Used for Bluetooth setup stuff
#define TEST_MODE 0 // Overrides all else, used for testing
#define DC_REMOVAL 0
/*                     TODO : UPDATE AND COMPLETE
   HOW TO CONNECT (in AT_COMMAND_MODE) -> Basically... two commands: AT+INIT and AT+INQ
    1. Once in pairing mode you can pair
    2. AT+INIT -> Initialize ?
    3. AT+INQ -> Start being available to other devices

*/


#define HAT_OFF_THRESHOLD_MIN 0
#define HAT_OFF_THRESHOLD_MAX 500

#define MAX_NUMBER_ALLOWED_THRESHOLD_BREACHES 3

arduinoFFT FFT = arduinoFFT();

unsigned int sampling_period_for_one_sample_us;
unsigned long microseconds;

double vReal[SAMPLES];
double vImag[SAMPLES];
double normalized_values[SAMPLES];

bool hatOn = true;

enum WaveData
{
  EPSILON, // <0.5 Hz
  DELTA, // 0.5-3 Hz
  THETA, // ~4-7 Hz
  //LOW_ALPHA, // ~8-9 Hz
  //HIGH_ALPHA, // ~10-12 Hz
  ALPHA, // ~8-12 Hz
  //LOW_BETA, // 13-17 Hz
  //HIGH_BETA, // 18-30 Hz
  BETA, // 13-30 Hz
  //LOW_GAMMA, // 31-40 Hz
  //MID_GAMMA, // 41-50 Hz
  GAMMA, // 31-50 Hz

  NUM_WAVES // Keeps track of how many brain waves are in this enumeration
};

long waveData[NUM_WAVES]; // TODO: May want this to be a long int? Even better, a long unsigned int



void TestFFT();



void InitializeBrainWaveData()
{
  for ( int i = 0; i < NUM_WAVES; i++ )
  {
    waveData[i] = 0;
  }
}

void SampleAnalog()
{
  hatOn = true;
  int num_breached_threshold = 0;

  microseconds = micros();
  for (int i = 0; i < SAMPLES; i++)
  {
    //microseconds = micros();    // TODO: Overflows after around 70 minutes!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?

    // analogRead documentation: https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/ -> We could use this function to read from pins hooked up to a sine wave generator
    vReal[i] = analogRead(sensorPin); // Read in values from the sensor analog pin -> Will map input voltages between 0 and 5 V into integer values between 0 and 1023, and thus converts analog to digital

    if ( vReal[i] < HAT_OFF_THRESHOLD_MIN || vReal[i] > HAT_OFF_THRESHOLD_MAX )
    {
      num_breached_threshold++;
      hatOn = false;
    }

    vImag[i] = 0;

    //while( micros() < (microseconds + sampling_period_for_one_sample_us) );
    while(micros() - microseconds < sampling_period_for_one_sample_us);
    microseconds += sampling_period_for_one_sample_us;
  }

  if ( num_breached_threshold > MAX_NUMBER_ALLOWED_THRESHOLD_BREACHES )
  {
    hatOn = false;
  }
}

/* Crash Course FFT

    Windowing Functions
      Why? FFT makes assumption of int # of periods (stop point of "window" can be wrapped around to end point and treated as if a continuous signal)
           This assumption is often wrong and as a result causes leakage of a frequency's amplitude to other frequencies and thus distorts the true signal.
           Windowing is a solution to this.

      Functions -> Which to apply depends on signal. Choosing is usually tough and involves estimating the frequency content of the signal.
                   NOTE: This visualization of the windowing functions (e.g. sine (pimple) for HAMMING) is for the discrete "snapshot" only -> the entire signal at that frequency ("being windowed") will have a modified resulting form in the overall signal
                         This resulting form will not be a perfect, obvious spike at the particular frequency/bin (due to windowing functions not working perfectly), so the frequency's amplitude will still "leak" over to nearby frequencies which is what the resulting signal would see at that frequency/bin
                         The "resulting spectrum" = the final result of the distortion for that particular bin), since the resulting form will not be perfect, as mentioned above

                         I believe "side lobes" are just the up/down noise that tapers off the further you get from the specific frequency/bin.
                         Where the actual desired frequency/bin has the largest "bump", the surrounding parts will have smaller "bumps", decreasing in amplitude as you go further away from the speicifc/desired frequency/bin. These additional "bumps" = "side lobes"
                          A "main lobe" I believe is just the first lobe after the desired frequency/bin that the window operated on (i.e. closest to original bin and thus has the highest amplitude of a bump, bumps further out have less amplitude)

                         I believe "signal spectrum" means the waveform as it is represeneted by the summation of sinusoids (sine functions), each with a particular amplitude and phase (period)

                         I believe "Spectral flatness" (measured in dB) is a way to quantify how noisy a signal

                         "Noise floor" = Measure of signal created from sum of all noise sources and unwanted signals within a measurement system (where we are taking our measurement, i.e. environment, device, thermal, etc.)

        RECTANGLE: AKA "No window" -> Even with no windowing applied, due to the fact that the FFT can ever only deal with a snapshot of signal (not continuous, have to work with discrete signal), it is as if applying a "rectangular" shaped window of uniform height)
                   Use if signal spectrum is rather flat
        HAMMING: Similar to HANN, will try to apply a .5 period sine function form (like a pimple ready to pop) to signal such that the entire signal fits within, this causes the signal in the middle of the "pimple" to have height amplitude and signal near ends of sine waveform to be very small.
                    This helps to enforce a signal that can be "wrapped around" from start to finish (i.e. have an integer number of periods to signal)
                    HAMMING and HANN have this "pimple" (sine wave) like waveform going both up and below the signal (which creates the bounds for which the signal is to remain within, which ultimately causes the ends of signal to have smaller amplitude and stick within pimple)
                    This windowing function however, does not have a tight closing at ends of signal, HANN does. Essentially this means the top and bottom "pimples" do NOT meet at zero, but come close and ultimate level off (no slope) before reaching the other pimple
        HANN: Similar to HAMMING but enforces a tight closing of windowing function (sine wave, AKA "pimple"). Essentially this means the top and bottom "pimples" meet at 0.
              Has good frequency resolution (can get more granular, i.e. smaller bin sizes) and has reduced leakage (less impact on nearby frequencies.
              GREAT CHOICE for 95% of cases -> Choose this if you don't know nature of signal (what frequencies you may be seeing) but want to apply a smoothing window
        TRIANGLE:
        NUTTALL:
        BLACKMAN:
        BLACKMAN_NUTTALL:
        BLACKMAN_HARRIS: Similar to Hamming and Han windows. Resulting spectrum has a wide peak, but good side lobe compression (less additional bumps and amplitude of them are mitigated)
                         Two main types: 4-term Blackmann-Harris which is a good general-purpose window, haivng side lobe rejection in the high 90s dB and moderately wide main lobe.
                                         7-term Blackmann-Harris which has all the dynamic range you should ever need, but comes with a wide main lobe
        TOP: (AKA flat top?) sinusoidal, but actually crosses the zero line -> Causes a much broader peak in frequency domain -> CLOSER TO TRUE AMPLITUDE OF SIGNAL THAN OTHER WINDOWS
        WELCH:

        other windowing functions...
        KAISER-BESSEL: Balances between various conflicting goals of amplitude accuracy, side lobe distance, and side lobe ehight (amplitude).
                       Compares roughly to Blackman-Harris window functions but for the same main lobe width, the near side lobes tend to be higher, but the further out side lobes are lower.
                       This window often reveals signals close to the "noise floor" (i.e the measure of signal from sum of all noise sources)


*/
void ComputeFFT()
{
#if DC_REMOVAL
  FFT.DCRemoval(vReal, SAMPLES);
#endif
  FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
  //FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_HANN, FFT_FORWARD);
  //FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_TRIANGLE, FFT_FORWARD);
  //FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_RECTANGLE, FFT_FORWARD);
  //FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_BLACKMAN_HARRIS, FFT_FORWARD);
  //FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_BLACKMAN_NUTTALL, FFT_FORWARD);
  //FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_FLT_TOP, FFT_FORWARD);
  FFT.Compute(vReal, vImag, SAMPLES, FFT_FORWARD);
  FFT.ComplexToMagnitude(vReal, vImag, SAMPLES);
  //FFT.DCRemoval(vReal, SAMPLES);
}

void GroupFrequencies(double* realFFTValues, long* waveData, int numWaves)
{
  float frequency;
  // TODO: Make this more scalable (i.e. don't use hardcoded values)
  //float wave_frequencies[numWaves] = {0.5, 3, 7, 9, 12, 17, 30, 40, 50};
  float wave_frequencies[numWaves] = {0.5, 3, 7, 12, 30, 50};
  int numValuesInBand[numWaves] = {0};

  for (int i = 0; i < (SAMPLES / 2); i++)
  {
    frequency = (i * 1.0 * SAMPLING_FREQUENCY) / SAMPLES;

    // TODO: DC Offset hack -> Neglecting Epislon and Delta wave :(
    // TODO: Add a more sophisticated approach to handling the DC offset
    if ( frequency < 2 )
    {
      continue; // Frequency of 0.0 has noise peak due to DC offset, frequencies < 2 have inflated values as well
    }

    // Gather all frequencies within the frequency band for a brain wave

    // Find the frequency band to update
    for ( int j = 0; j < numWaves; j++ )
    {
      normalized_values[i] = ( (2 * sqrt( vReal[i] * vReal[i] + vImag[i] * vImag[i] )) / SAMPLES ) * 0.5; // Multiply by 0.5 since using Hann windowing function

      // We are starting from the smallest frequencies and always increasing,
      // so we will catch the smallest frequency band that this frequency fits in to
      if ( frequency < wave_frequencies[j] )
      {
        //waveData[j] += realFFTValues[i] * realFFTValues[i];
        waveData[j] += normalized_values[i] * normalized_values[i];
        numValuesInBand[j]++;
        break;
      }
    }
  }


  for ( int i = 0; i < numWaves; i++ )
  {
    // Divide by number of accumulated values to complete the calculation of the average
    if ( !numValuesInBand[i] )
    {
      // TODO: Handle this better...? I'm aiming to not divide by zero, but i haven't had a problem with this yet.
      numValuesInBand[i] = 1;
    }
    waveData[i] = waveData[i] / numValuesInBand[i]; // Integer division..

    // Finally, take the square root to complete the calculation of the root mean square for the brain wave signal
    waveData[i] = sqrt( waveData[i] );

    //waveData[i] = sqrt( waveData[i] ); // HACK to get smaller values
  }

}

int CalculateMean(int* valueArray, int arraySize)
{
  int mean = 0;
  for ( int i = 0; i < arraySize; i++ )
  {
    mean += valueArray[i];
  }

  return mean;
}

int ComputeRootMeanSquare(int* valueArray, int arraySize)
{
  float rms = 0;

  // Sum of squares
  for ( int i = 0; i < arraySize; i++ )
  {
    rms += valueArray[i] * valueArray[i];
  }

  // Average of the sum of squares
  rms /= arraySize;

  // Square root the result
  rms = sqrt(rms);

  return rms;
}

void UpdateBrainWaveData()
{
  GroupFrequencies(vReal, waveData, NUM_WAVES); // This handles the full Root Mean Square for all of the frequency bands
}

int FindMaxMagnitude(double* realFFTValues, int numSamples)
{
  if ( numSamples < 1 )
  {
    // ERROR
#if DEBUG
    Serial.print("ERROR: There are no samples to find the max magnitude");
#endif
  }

  int max_magnitude = realFFTValues[0];
  for ( int i = 1; i < numSamples; i++ )
  {
    max_magnitude = realFFTValues[i] > max_magnitude ? realFFTValues[i] : max_magnitude;
    /*if( realFFTValues[i] > max_magnitude )
      {
      max_magnitude = realFFTValues[i];
      }*/
  }

  return max_magnitude;
}

int FindMinMagnitude(double* realFFTValues, int numSamples)
{
  if ( numSamples < 1 )
  {
    // ERROR
#if DEBUG
    Serial.print("ERROR: There are no samples to find the min magnitude");
#endif
  }

  int min_magnitude = realFFTValues[0];
  for ( int i = 1; i < numSamples; i++ )
  {
    min_magnitude = realFFTValues[i] > min_magnitude ? realFFTValues[i] : min_magnitude;
    /*if( realFFTValues[i] < min_magnitude )
      {
      min_magnitude = realFFTValues[i];
      }*/
  }

  return min_magnitude;
}

void AdjustForDCOffset(double* realFFTValues, int numSamples)
{
  int max_magnitude = FindMaxMagnitude(realFFTValues, numSamples);
  int min_magnitude = FindMinMagnitude(realFFTValues, numSamples);

  int adjustment = max_magnitude - min_magnitude;

  for ( int i = 0; i < numSamples; i++ )
  {
    realFFTValues[i] -= adjustment;
  }

}

void PrintRawFFTOutputToSerial()
{
  double peak = FFT.MajorPeak(vReal, SAMPLES, SAMPLING_FREQUENCY);
  double x;
  double v;
  FFT.MajorPeak(vReal, SAMPLES, SAMPLING_FREQUENCY, &x, &v);
  Serial.println("PEAK:");
  Serial.print(x, 6);
  Serial.print(", ");
  Serial.println(v, 6);

  Serial.print("FREQUENCY MOST DOMINANT IS... ");
  Serial.println(peak);     //Print out what frequency is the most dominant.

  // There are SAMPLE/2 bins... So iterate over each bin and print out the REAL value, which corresponds to the amplitude
  Serial.println("\nThe recorded samples...");
  for (int i = 0; i < (SAMPLES / 2); i++)
  {
    /*View all these three lines in serial terminal to see which frequencies has which amplitudes*/
    /*NOTE: Due to DC offset there will be a peak at 0*/

    Serial.print("Frequency of... ");
    Serial.print((i * 1.0 * SAMPLING_FREQUENCY) / SAMPLES);
    Serial.print(" has amplitude of... ");
    Serial.println(vReal[i]);
    //Serial.println(normalized_values[i]); // TODO: LOW MEMORY and CAN'T UPLOAD WITH THIS... Actually it seems like I can?
  }
}


void PrintBrainWaveDataToSerial()
{
  Serial.println();
  Serial.println("Brain Wave Values...");
  Serial.println();

  Serial.print("Epsilon Wave : ");
  Serial.println(waveData[EPSILON]);

  Serial.print("Delta Wave : ");
  Serial.println(waveData[DELTA]);

  Serial.print("Theta Wave : ");
  Serial.println(waveData[THETA]);

  /*Serial.print("Low Alpha Wave : ");
    Serial.println(waveData[LOW_ALPHA]);

    Serial.print("High Alpha Wave : ");
    Serial.println(waveData[HIGH_ALPHA]);

    Serial.print("Low BetaWave : ");
    Serial.println(waveData[LOW_BETA]);

    Serial.print("High BetaWave : ");
    Serial.println(waveData[HIGH_BETA]);

    Serial.print("Low Gamma Wave : ");
    Serial.println(waveData[LOW_GAMMA]);

    Serial.print("Mid Gamma Wave : ");
    Serial.println(waveData[MID_GAMMA]);*/

  Serial.print("Alpha Wave : ");
  Serial.println(waveData[ALPHA]);

  Serial.print("Beta Wave : ");
  Serial.println(waveData[BETA]);

  Serial.print("Gamma Wave : ");
  Serial.println(waveData[GAMMA]);

  Serial.println();
}

void PrintToSerial()
{
  PrintRawFFTOutputToSerial();
  PrintBrainWaveDataToSerial();

  Serial.println("DONE PRINTING TO SERIAL!");
}

void PrintToPlotter()
{
  for (int i = 0; i < (SAMPLES / 2); i++)
  {
    Serial.println(vReal[i]);
  }
}

// Allow module time to receive command and reply
void WaitForResponse()
{
  delay(500);
  while (BTSerial.available()) {
    Serial.write(BTSerial.read());
  }
}

// OLD // Order sent: SYNC, EPSILON, DELTA, THETA, LOW_ALPHA, HIGH_ALPHA, LOW_BETA, HIGH_BETA, LOW_GAMMA, MID_GAMMA
// Order sent: SYNC, EPSILON, DELTA, THETA, ALPHA, BETA, GAMMA
void SendToDevice()
{
  BTSerial.print("C");
  BTSerial.print(",");
  for (int i = 0; i < NUM_WAVES; i++)
  {
    BTSerial.print(waveData[i]);
    if ( i != NUM_WAVES - 1 )
      BTSerial.print(",");
  }
  BTSerial.print(";");
}

void ATCommandModeDEBUGSetup() {
  /*                TODO : UPDATE AND COMPLETE
     HOW TO CONNECT
      1. Once in pairing mode you can pair
      2. AT+INIT -> Initialize ?
      3. AT+INQ -> Start being available to other devices

  */

  String setName = String("AT+NAME=MindSeyeT\r\n"); // Sets name to "MindSeyeT"
  String setPassword = String("AT+PSWD=1234\r\n"); // Sets password to "1234"

  pinMode(9, OUTPUT);  // this pin will pull the HC-05 pin 34 (key pin) HIGH to switch module to AT mode
  digitalWrite(9, HIGH);

  Serial.begin(9600);
  while (!Serial) ; // Needed for Arduino Micro/Leonardo since Software USB is being used instead of hardware USB-to-Serial adapter

  Serial.println("Enter AT commands:");
  BTSerial.begin(38400);  // HC-05 default speed in AT command more

  Serial.println("Checking response with AT...");
  BTSerial.print("AT\r\n");
  WaitForResponse();

  Serial.print("Setting name with "); Serial.println(setName);
  BTSerial.print(setName); // Send command to change name
  WaitForResponse();

  Serial.print("Setting password with "); Serial.println(setPassword);
  BTSerial.print(setPassword); // Send command to change password
  WaitForResponse();

  Serial.println("End of setup");
  Serial.println("Enter in AT Commands:");
}

void ATCommandModeDEBUGLoop() {
  // Keep reading from HC-05 and send to Arduino Serial Monitor
  if (BTSerial.available()) {
    Serial.write(BTSerial.read()); //Serial.write("RECEIVED ");
  }

  // Keep reading from Arduino Serial Monitor and send to HC-05
  if (Serial.available()) {
    BTSerial.write(Serial.read()); //Serial.write("SENT ");
  }
}



void setup() {
  sampling_period_for_one_sample_us = round(1000000 * (1.0 / SAMPLING_FREQUENCY)); // How long to sample before running FFT for a SINGLE SAMPLE (in microseconds) -> 1,000,000 microseconds * (1/SAMPLING_FREQUENCY)

#if !PLOT
  BTSerial.begin(38400);
  while (!BTSerial) ;
#endif


  // DELETE ME (comment me out) when wanting Bluetooth (HC-05) to work!
#if DEBUG || PLOT || TEST_MODE
  Serial.begin(9600);
  while (!Serial);

  #if DEBUG
    Serial.print("Sampling period in microseconds = "); Serial.println(SAMPLES * sampling_period_for_one_sample_us);
  #endif
#endif

#if AT_COMMAND_MODE
  Serial.begin(9600); // NOTE: Doing this causes Bluetooth module to not work correctly when not directly ran from IDE (i.e. hooked up to wall)
  while (!Serial) ; // Needed for Arduino Micro/Leonardo since Software USB is being used instead of hardware USB-to-Serial adapter
  Serial.write("In AT Command Mode...");
  ATCommandModeDEBUGSetup();
#endif

#if !TEST_MODE
  InitializeBrainWaveData();
#endif
}

void loop() {

#if AT_COMMAND_MODE
  ATCommandModeDEBUGLoop();
#elif TEST_MODE
  TestFFT();
#else
  if ( 1 ) // hatOn )
  {
    // This will read in data from the amplifier circuit
    //sensorValue = analogRead(sensorPin);

    /*SAMPLING*/
    SampleAnalog();

    /*HANDLE DC OFFSET*/
    //AdjustForDCOffset(vReal, SAMPLES);

    /*FFT*/
    ComputeFFT();

    /*HANDLE WAVE DATA*/
    InitializeBrainWaveData();
    UpdateBrainWaveData();

    /*PRINT RESULTS TO SERIAL*/
#if DEBUG
    PrintToSerial();
#elif PLOT
    PrintToPlotter();
#endif

    /* BLUETOOTH STUFF */
#if !PLOT
    SendToDevice();
#endif

#if DELAY
    delay(1000);  // Let the device breath. Serial printing acts up without (since we always printing to serial and port always busy)
#endif
    
  }
  else
  {
    // Hat is off!
  }
#endif
}









// Sampling Info
//#define SIGNAL_FREQUENCY 1000 // Frequency of signal that is be ing sampled
//#define AMPLITUDE 100 // Amplitude of signal that is being sampled

// How first component of vector should be interpreted (index/time/freq), i.e. what should the abscissa (x-coord) be interpreted as (an INDEX, s.a. 0,1,2,3,... or TIME i.e. 1/Hz or as FREQUENCY s.a. 0hz, 1hz, 2hz,...)
#define SCL_INDEX 0x00
#define SCL_TIME 0x01
#define SCL_FREQUENCY 0x02

#define SCL_PLOT 0x03



void BuildRawData();
void ExampleComputeFFTWithOutput();
void PrintTestResults();
void PrintVector(double *vData, uint16_t bufferSize, uint8_t scaleType);

// TODO: Have parameters to specify types of functions/amplitudes/whatnot? to test
void TestFFT()
{
  BuildRawData();
  
  //ComputeFFT();
  ExampleComputeFFTWithOutput();
  
  //PrintTestResults();

  // Run once
  while(1);
}

void ExampleComputeFFTWithOutput()
{
  FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_HAMMING, FFT_FORWARD);  /* Weigh data */
  Serial.println("Weighed data:");
  PrintVector(vReal, SAMPLES, SCL_TIME);
  FFT.Compute(vReal, vImag, SAMPLES, FFT_FORWARD); /* Compute FFT */
  Serial.println("Computed Real values:");
  PrintVector(vReal, SAMPLES, SCL_INDEX);
  Serial.println("Computed Imaginary values:");
  PrintVector(vImag, SAMPLES, SCL_INDEX);
  FFT.ComplexToMagnitude(vReal, vImag, SAMPLES); /* Compute magnitudes */
  Serial.println("Computed magnitudes:");
  PrintVector(vReal, (SAMPLES >> 1), SCL_FREQUENCY);

  double x;
  double v;
  FFT.MajorPeak(vReal, SAMPLES, SAMPLING_FREQUENCY, &x, &v);
  Serial.println("PEAK:");
  Serial.print(x, 6);
  Serial.print(", ");
  Serial.println(v, 6);
}

void BuildRawData()
{
 /* Build raw data */
  double cycles = (((SAMPLES-1) * SIGNAL_FREQUENCY) / SAMPLING_FREQUENCY); //Number of signal cycles that the sampling will read
  for (int i = 0; i < SAMPLES; i++)
  {
    vReal[i] = int8_t((AMPLITUDE * (sin((i * (twoPi * cycles)) / SAMPLES))) / 2.0);/* Build data with positive and negative values*/
    //vReal[i] = uint8_t((AMPLITUDE * (sin((i * (twoPi * cycles)) / SAMPLES) + 1.0)) / 2.0);/* Build data displaced on the Y axis to include only positive values*/
    vImag[i] = 0.0; //Imaginary part must be zeroed in case of looping to avoid wrong calculations and overflows
  }
  /* Print the results of the simulated sampling according to time */
  Serial.println("Data:");
  PrintVector(vReal, SAMPLES, SCL_TIME);
}

void PrintTestResults()
{
  
}

void PrintVector(double *vData, uint16_t bufferSize, uint8_t scaleType)
{
  for (int i = 0; i < bufferSize; i++)
  {
    double abscissa;
    /* Print abscissa value */
    switch (scaleType)
    {
      case SCL_INDEX:
        abscissa = (i * 1.0);
  break;
      case SCL_TIME:
        abscissa = ((i * 1.0) / SAMPLING_FREQUENCY);
  break;
      case SCL_FREQUENCY:
        abscissa = ((i * 1.0 * SAMPLING_FREQUENCY) / SAMPLES);
  break;
    }
    Serial.print(abscissa, 6);
    if(scaleType==SCL_FREQUENCY)
      Serial.print("Hz");
    Serial.print(" ");
    Serial.println(vData[i], 4);
  }
  Serial.println();
}
