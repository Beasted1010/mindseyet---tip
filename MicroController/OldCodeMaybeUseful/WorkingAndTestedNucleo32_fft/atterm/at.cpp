
#include "mbed.h"
#include "at.h"

#define TX PA_9
#define RX PA_10

#define TX1 PB_6
#define RX1 PB_7

Serial atserial0(TX, RX); //UART1
Serial atserial1(TX1, RX1); //UART6
Serial terminal(USBTX, USBRX); //UART Terminal

atterm at0;
atterm at1;
atterm pc;

Timer t;

void CallBack0()
{
    at0.sign = atserial0.getc();


    if((at0.sign != '\r') && (at0.sign != '\n'))  {  //with no linefeed and carriage return, string is written in buffer
        at0.buffer[at0.a] = at0.sign;
        at0.a ++;
    } else {   //if cr and lf were received , the content of buffer is send to atcommand device

        if(at0.off == 0)  {
            atserial0.printf("Read: %s\r\n", at0.buffer); // via printf formatted as string for debug use
        }

    }
}

void CallBack1()
{
    at1.sign = atserial1.getc();


    if((at1.sign != '\r') && (at1.sign != '\n'))  {  //with no linefeed and carriage return, string is written in buffer
        at1.buffer[at1.a] = at1.sign;
        at1.a ++;
    } else {   //if cr and lf were received, the content of buffer is send to atcommand device

        if(at1.off == 0)  {
            atserial1.printf("Read: %s\r\n", at1.buffer); // via printf formatted as string for debug use
        }

    }
}

void CallBack2()
{
    pc.sign = terminal.getc();


    if((pc.sign != '\r') && (pc.sign != '\n'))  {  //with no linefeed and carriage return, string is written in buffer
        pc.buffer[pc.a] = pc.sign;
        pc.a ++;
    } else {   //if cr and lf were received, the content of buffer is send to atcommand device

        if(at1.off == 0)  {
            terminal.printf("Read: %s\r\n", pc.buffer); // via printf formatted as string for debug use
        }

    }
}

void atterm::debug_off(bool l)
{
    if(l) atterm::off = 1;
    if(!l) atterm::off = 0;
}

void atterm::at_send(char *format, char *buf)
{
    atserial0.printf(format, buf);
}

void atterm::clear()
{
    memset(atterm::buffer, 0, sizeof(atterm::buffer)); //set buffer to 0, for next clean read
    atterm::a = 0; //index pointer to zero

}

void atterm::device_init(unsigned long baud, _IRQ interrupt)
{
    atterm::debug_off(1);
    if(interrupt == ON)  {
        atserial0.attach(&CallBack0);
    }
    atserial0.baud(baud);

}

void atterm::at_send1(char *format, char *buf)
{
    atserial1.printf(format, buf);
}



void atterm::device_init1(unsigned long baud, _IRQ interrupt)
{
    atterm::debug_off(1);
    if(interrupt == ON)  {
        atserial1.attach(&CallBack1);
    }
    atserial1.baud(baud);
    at1.at_send1("AT\r\n", "");

}

void atterm::pc_send(char *format, char *buf)
{
    terminal.printf(format, buf);
}

void atterm::terminal_init(unsigned long baud, _IRQ interrupt)
{
    atterm::debug_off(1);
    if(interrupt == ON)  {
        terminal.attach(&CallBack2);
    }
    terminal.baud(baud);
    pc.pc_send("*****Terminal*******\r\n", "");

}


char *atterm::getAnswer(uint32_t timeout)
{
    int b = 0, b1 = 0, b2 = 0;
    uint32_t t_time = 0;
    char sign, sign1, sign2;

    memset(read_timed_buffer, 0, sizeof(read_timed_buffer));

    t.start();
    t_time = t.read_ms();

    while(1)  {
        if(atserial0.readable())  {
            sign = atserial0.getc();
            read_timed_buffer[b] = sign;
            b ++;
        }

        else if(atserial1.readable())  {
            sign1 = atserial1.getc();
            read_timed_buffer[b1] = sign1;
            b1 ++;
        }
        
        else if(terminal.readable())  {
            sign2 = terminal.getc();
            read_timed_buffer[b2] = sign2;
            b2 ++;
        }
        
        if((t.read_ms() - t_time) > timeout)  {
            t.stop();
            t.reset();
            break;

        }

    }
    return read_timed_buffer;
}
