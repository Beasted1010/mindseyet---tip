#include <mbed.h>
/* Include arm_math.h mathematic functions */
#include <arm_math.h>
/* Include mbed-dsp libraries */
#include <arm_common_tables.h>
#include <arm_const_structs.h>
#include <math_helper.h>

#include "Brainwave.h"



/* TODO: CURRENT UNKOWNS
 *
 * 		 Sample timing - Am I going too fast? Too slow? Is the math right? See samplingTimer.attach_us
 *		 ADC manipulation - Should I do a "- 0.5f"? or "* 1023..."? Why? (Saw examples online doing different things)
 *		 User input when doing USER_CONTROLLED_DELAY (ENTER in CoolTerm) causes 2 outputs of data (loops twice)
 *		 	Probably something to do with needing to flush the input buffer?
 *		 Data type for waveData... long? long unsigned?
 *
 *		 I think the UpdateBrainWaveData function is busted -> Something to do with FFT_SIZE vs REAL_AND_IMAG_SAMPLE_COUNT (I think I'm including uninitialized data in computation, output is really large value for moderate magnitudes)
 *		 		This comment may be outdated now
 *
 *		 Something strange is going on with samplingIsDone function in the if statement in main().
 *		 	When running it, the conditional seems to be skipped (as can be seen by placing breakpoints in samplingCallback and samplingIsDone)
 *
 *
 */

/* CONSIDERATIONS
 *
 *		 Performance Boosters
 *		 	Use _t types for more fine grained sizes (i.e. int i in for loop could be replaced with a uint8_t depending on needed size, but no need to be signed)
 *
 *
 *		 Brainwave Frequency enum data is being defined (called WaveData here and Band in Frequency_band.h) in multiple places, this could be simplified to only have one.
 *
 *
 * 		 Plotting? - Maybe throw up a plot? What all needed to do so? Do I have the libraries already?
 *
 */






/* FFT Parameters

    Source: https://electronics.stackexchange.com/questions/12407/what-is-the-relation-between-fft-length-and-frequency-resolution

   Sampling Frequency, or Sampling Rate: The rate at which we sample data for the FFT.
        By "Nyquist-Shannon" sampling theorem -> To ensure an accurate output, we want (Sampling Rate / 2)    -> ???
   Samples: This is the number of samples we collect for the FFT
        The number of FFT bins we will have is (Samples / 2) -> e.g. 128 samples -> 64 FFT bins

	NOTE: Number of Samples = FFT_SIZE in my implementation

   The Samples paired with the Sampling Frequency provide the bin width
      Bin Width = (Sampling Frequency / 2) / (Number of Samples / 2)

      Basically you can just do -> Sampling Rate / Number of Samples -> Since both numerator and denominator are halved.


   E.g. SAMPLES = 128, SAMPLING_FREQUENCY = 128
        # FFT Bins -> 64 Bins (128 Samples / 2)
        Frequency content -> 64 Hz (128 Sampling Frequency / 2)
        Bin width = (64 / 64) = 1 Hz


*/



/* FFT settings */
const int REAL_AND_IMAG_SAMPLE_COUNT 	= 128; // 1/2 are real parts and other half are imaginary parts -> A value of 128 works well with 300 Hz sampling rate
											   // FFT_SIZE must be power of 2 for efficiency in FFT computation, and thus REAL_AND_IMAG_SAMPLE_COUNT should be a power of 2 too
const int SAMPLING_RATE_HZ 				= 300;
const unsigned int FFT_SIZE 			= REAL_AND_IMAG_SAMPLE_COUNT / 2;

/* FFT Test Sampling Info */
#define USE_MOCK_DATA 0

// These are REAL values, imaginary values are all set to 0 (the offsets will be handled by BuildRawFromMockData)
// Current size is 64, this MUST match FFT_SIZE!!!!!!!!!
#define MOCK_DATA_10Hz {0, 10, 20, 28, 36, 42, 47, 49, 49, 47, 44, 38, 30, 22, 12, 2, -7, -17, -26, -35, \
						-41, -46, -49, -49, -48, -45, -39, -32, -24, -14, -4, 5, 15, 24, 33, 40, 45, 48, 49, 49, \
						46, 41, 34, 26, 17, 7, -2, -13, -22, -31, -38, -44, -48, -49, -49, -47, -42, -36, -28, -19, -9, 0, 10, 20}

const float SIGNAL_FREQUENCY 	= 10; // Frequency of signal that is being sampled
const uint8_t AMPLITUDE 		= 100; // Amplitude of signal that is being sampled


/* Timing constants */
#define SAMPLING_TIME_S			(1.0 / SAMPLING_RATE_HZ)
#define SAMPLING_TIME_US		SAMPLING_TIME_S * 1000000

/* Program Modes */
#define AT_COMMAND_MODE 			 	0
#define SOFTWARE_FREQUENCY_TEST_MODE 	0
#define BRAINWAVE_FREQUENCY_TEST_MODE   1

/* Bluetooth Defines */
#define BLUETOOTH_INQUIRE	0 // 1 = Put Bluetooth module into pairing mode (fast blinking red light on HC-05)


/* Brainwave generator info */

#define DEEP_SLEEP  Frequency_band::DELTA   // ~1-3  Hz
#define ASLEEP      Frequency_band::THETA   // ~4-7  Hz
#define DROWSY      Frequency_band::ALPHA   // ~8-12 Hz
#define RELAXED     Frequency_band::BETA    // 13-30 Hz
#define EXCITED     Frequency_band::GAMMA   // 31-50 Hz

#define BRAINWAVE_STATE  	DROWSY // Can be any of {DEEP_SLEEP, ASLEEP, DROWSY, RELAXED, EXCITED}

#define BRAINWAVE_TEST_DELAY     2000 // Milliseconds


/* Miscellaneous Defines */
#define LIVE_RUN_DELAY 2000 // Milliseconds

#define USER_CONTROLLED_DELAY 0
#define DEBUG 1

/* Function Prototypes */

/**Sampling*/
void samplingCallback();
void samplingBegin();
bool samplingIsDone();

/**Serial*/
void PrintToSerial();
void PrintRawFFTInputToSerial();
void PrintRawFFTOutputToSerial();
void PrintMaxFrequencyToSerial();
void PrintBrainWaveDataToSerial();
void WaitForBluetoothResponse();
void ReadBluetoothCharacter();
void PrintBluetoothCharacterToComputer();
void ReadComputerCharacter();
void PrintComputerCharacterToBluetooth();
void SendToDevice();
void PrepareBluetooth();

/**FFT*/
void SampleAnalog();
void ComputeFFT();
void TestFFT();

/**Microcontroller Configuration*/
void MyGPIOInit();

/**Brainwave*/
void PrintBrainWaveDataToSerial();
void InitializeBrainWaveData();
void UpdateBrainWaveData();
void GroupFrequencies(float* realFFTValues, long* waveData, int numWaves);
void TestBrainwaves( Brainwave* brainwave_signal );

/* Global variables */
/** MBED class APIs */
Serial myComputer(USBTX, USBRX);
Serial blue(PA_9, PA_10); // D1, D0
DigitalOut myled(LED1);

AnalogIn   myADC0(A0);
AnalogIn   myADC1(A1);
AnalogIn   myADC2(A2);
AnalogIn   myADC3(A3);

enum myADCs // To serve as index into samples (to indicate which analog channel to use)
{
	MY_ADC0,
	MY_ADC1,
	MY_ADC2,
	MY_ADC3,
	ADC_COUNT
};

#define ADC_PINS_IN_USE_COUNT 1 // TODO: Determine if I this #define is better/enough than uint8_t (I think so as of now... since it's just an index)

Ticker samplingTimer;

const static arm_cfft_instance_f32 *S;

float samples[ADC_PINS_IN_USE_COUNT ][REAL_AND_IMAG_SAMPLE_COUNT];
float magnitudes[ADC_PINS_IN_USE_COUNT][FFT_SIZE];
//float normalizedValues[FFT_SIZE];
unsigned int sampleCounter = 0;

/** Brainwave Data */
enum WaveData
{
  EPSILON, // <0.5 Hz
  DELTA, // 0.5-3 Hz
  THETA, // ~4-7 Hz
  //LOW_ALPHA, // ~8-9 Hz
  //HIGH_ALPHA, // ~10-12 Hz
  ALPHA, // ~8-12 Hz
  //LOW_BETA, // 13-17 Hz
  //HIGH_BETA, // 18-30 Hz
  BETA, // 13-30 Hz
  //LOW_GAMMA, // 31-40 Hz
  //MID_GAMMA, // 41-50 Hz
  GAMMA, // 31-50 Hz

  NUM_WAVES // Keeps track of how many brain waves are in this enumeration
};

long waveData[NUM_WAVES] = {0}; // TODO: May want this to be a long int? Even better, a long unsigned int


int main()
{
	// TODO: This may be breaking things on the HC-05, so look more into. Apparently I am doing something right on Arduino to set it into AT mode, so look into that to try and mimick (i.e. how am I setting EN/KEY high? Am I even?)
	//MyGPIOInit();
	//HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_6);
	//HAL_Delay(100);

    // Set up serial port.
    //myComputer.baud (9600);
	//myComputer.baud (38400);
	myComputer.baud(115200);

	blue.baud(38400); // Needs to be 38400 because this is HC_05's default baud rate and I haven't changed it with AT Command Mode

	//PrepareBluetooth(); // NOTE: Maybe I still want this?
	// TEMPORARY HACK (since my serial commands don't seem to be sending through CoolTerm (program that I'm using to get serial output from device))
	myComputer.printf("HC-05 SET\r\n");

#if BLUETOOTH_INQUIRE
	blue.printf("AT+INIT\r\n");
	//WaitForBluetoothResponse();

	blue.printf("AT+INQ\r\n");
	//WaitForBluetoothResponse();
#endif



	// TODO TODO TODO TODO TODO TOD -> I DON'T USE S anywhere... Should I?
    // Init arm_ccft_32
    switch (FFT_SIZE)
    {
    	case 16:
			S = & arm_cfft_sR_f32_len16;
			break;
		case 32:
			S = & arm_cfft_sR_f32_len32;
			break;
		case 64:
			S = & arm_cfft_sR_f32_len64;
			break;
		case 128:
			S = & arm_cfft_sR_f32_len128;
			break;
		case 256:
			S = & arm_cfft_sR_f32_len256;
			break;
		case 512:
			S = & arm_cfft_sR_f32_len512;
			break;
		case 1024:
			S = & arm_cfft_sR_f32_len1024;
			break;
		case 2048:
			S = & arm_cfft_sR_f32_len2048;
			break;
		case 4096:
			S = & arm_cfft_sR_f32_len4096;
			break;
    }

#if BRAINWAVE_FREQUENCY_TEST_MODE  // Keeping out of while loop. Maybe make global? But then may have error for undefined variable,
    							   //  							 I'd still want this #if since Brainwave is a heavy class (lots of data) and don't want it all in memory if not needed.
    Brainwave brainwave_signal( BRAINWAVE_STATE );
#endif

    while(1)
    {
#if AT_COMMAND_MODE
    	ATCommandModeDEBUGLoop();
#elif SOFTWARE_FREQUENCY_TEST_MODE
    	TestFFT();
#elif BRAINWAVE_FREQUENCY_TEST_MODE
    	TestBrainwaves( &brainwave_signal );
#else
    	if( 1 ) // Hat is on... TODO: Need a reliable way to know if this is the case
    	{
			/*SAMPLING*/
			SampleAnalog();

			// Calculate FFT and update brainwave data only if a full sample is available.
			if( samplingIsDone() )
			{
				/*HANDLE DC OFFSET*/ 							//-> TODO: NOT IMPLEMENTED YET
				//AdjustForDCOffset(magnitudes, REAL_AND_IMAG_SAMPLE_COUNT);

				/*FFT*/
				ComputeFFT();

				/*HANDLE WAVE DATA*/
				InitializeBrainWaveData();
				UpdateBrainWaveData();

#if DEBUG
				/*PRINT RESULTS TO SERIAL*/
				PrintToSerial();
// #elif PLOT -> TODO: Maybe throw up a plot? What all needed to do so? Do I have the libraries already?
#endif

				SendToDevice();
			}

#if DELAY
			HAL_Delay(LIVE_RUN_DELAY);
#endif
    	}
    	else
    	{
    		// Hat is off!
    	}
#endif

    }
}







/* SERIAL FUNCTIONS */

void PrintToSerial()
{
	PrintRawFFTOutputToSerial();
	PrintMaxFrequencyToSerial();
	PrintBrainWaveDataToSerial();
}

void PrintRawFFTInputToSerial()
{
	myComputer.printf("\r\n\nFFT INPUT (SAMPLES)\r\n");
	for( uint8_t adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{
		myComputer.printf("------Analog Pin #%i\n", adcPin);

		for( unsigned int i = 0; i < REAL_AND_IMAG_SAMPLE_COUNT; ++i )
			myComputer.printf("% 8.2f ", samples[adcPin][i]);

		myComputer.printf("\r\n\n");
	}
}

void PrintRawFFTOutputToSerial()
{
	myComputer.printf("\r\n\nFFT OUTPUT (MAGNITUDES)\r\n");
	for( uint8_t adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{
		myComputer.printf("------Analog Pin #%i\n", adcPin);

		for( unsigned int i = 0; i < FFT_SIZE; ++i )
			myComputer.printf("% 8.2f ", magnitudes[adcPin][i]);
		myComputer.printf("\r\n\n");
	}
}

void PrintMaxFrequencyToSerial()
{
	// For each of the analog pins in use, find the maximum
	for( uint8_t adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{
		float maxValue = 0.0f;
		unsigned long int maxIndex = 0;
		arm_max_f32(magnitudes[adcPin], FFT_SIZE, &maxValue, &maxIndex);
		myComputer.printf("MAX value at magnitudes[%i][%lu] (%f Hz) : %+ 4.2f\r\n\n", adcPin, maxIndex, (maxIndex * 1.0 * SAMPLING_RATE_HZ) / FFT_SIZE, maxValue);
	}
}

void WaitForBluetoothResponse()
{
	HAL_Delay(500);
	char buffer[100];
	while(blue.readable())
	{
		blue.scanf("%s", buffer);
		myComputer.printf("%s", buffer);
	}
}

void ReadBluetoothCharacter()
{
	char buffer[100];
	while(blue.readable())
	{
		blue.scanf("%s", buffer);
		myComputer.printf("%s", buffer);
	}
}

void PrintBluetoothCharacterToComputer()
{
	while(blue.readable())
	{
		myComputer.putc(blue.getc());
		//myComputer.printf("%c", blue.getc());
	}
}

void ReadComputerCharacter()
{
	char buffer[100];
	while(myComputer.readable())
	{
		myComputer.scanf("%s", buffer);
		myComputer.printf("%s", buffer);
		//blue.printf("%s", buffer);
	}
}

void PrintComputerCharacterToBluetooth()
{
	char buffer[10] = {0};
	while(myComputer.readable())
	{
		myComputer.scanf("%9s", buffer);
		myComputer.printf("Data: %s\r\n", buffer);
		blue.printf("%s\r\n", buffer);


		//blue.puts(buffer);
	}
}

void SendToDevice()
{
	blue.printf("C");
	blue.printf(",");
	for(int i = 0; i < NUM_WAVES; i++)
	{
		blue.printf("%ld", waveData[i]);
		if( i != NUM_WAVES - 1 )
		{
			blue.printf(",");
		}
	}
	blue.printf(";\r\n");

	myComputer.printf("Sent to device...\r\n");
}

void PrepareBluetooth()
{
  myComputer.printf("Checking response with AT...");
  blue.printf("AT\r\n");
  WaitForBluetoothResponse();

  myComputer.printf("Setting name...");
  blue.printf("AT+NAME=MindSeyeT\r\n"); // Sets name to "MindSeyeT"
  WaitForBluetoothResponse();

  myComputer.printf("Setting password...");
  blue.printf("AT+PSWD=1234\r\n"); // Sets password to "1234"
  WaitForBluetoothResponse();

  myComputer.printf("End of setup");
  myComputer.printf("Enter AT commands:");
}



/* SAMPLING FUNCTIONS */

void SampleAnalog()
{
#if USER_CONTROLLED_DELAY
	// Wait for user confirmation to restart sampling.
	myComputer.getc();
#endif

	samplingBegin();
}

void samplingCallback()
{
	// Read from each of the analog pins
	for( uint8_t adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{

		// Read from the ADC and store the sample data
		samples[adcPin][sampleCounter] = (1023 * myADC0.read()) - 511.0;
		//samples[sampleCounter] = myADC0.read();// - 0.5f;

		// Complex FFT functions require a coefficient for the imaginary part of the input.
		// Since we only have real data, set this coefficient to zero.
		samples[adcPin][sampleCounter+1] = 0.0;
	}

    // Update sample buffer position and stop after the buffer is filled
    sampleCounter += 2;
    if ( sampleCounter >= FFT_SIZE*2 ) {
        samplingTimer.detach();
    }
}


void samplingBegin()
{
    // Reset sample buffer position and start callback at necessary rate.
    sampleCounter = 0;
    samplingTimer.attach_us(&samplingCallback, 1000000/SAMPLING_RATE_HZ);
}

bool inline samplingIsDone()
{
    return sampleCounter >= FFT_SIZE*2;
}



/* FFT FUNCTIONS */

void ComputeFFT()
{
	for( uint8_t adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{
		// Run FFT on sample data.
		arm_cfft_f32(S, samples[adcPin], 0, 1);
	}

#if DEBUG
	PrintRawFFTInputToSerial();
#endif

	for( uint8_t adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{
		// Calculate magnitude of complex numbers output by the FFT.
		arm_cmplx_mag_f32(samples[adcPin], magnitudes[adcPin], FFT_SIZE);
		magnitudes[adcPin][0] = 0; // HACK: DC OFFSET THING? It is causing issues with max frequency
	}
}


/* FFT TEST FUNCTION */

// How first component of vector should be interpreted (index/time/freq), i.e. what should the abscissa (x-coord) be interpreted as (an INDEX, s.a. 0,1,2,3,... or TIME i.e. 1/Hz or as FREQUENCY s.a. 0hz, 1hz, 2hz,...)
#define SCL_INDEX 0x00
#define SCL_TIME 0x01
#define SCL_FREQUENCY 0x02

#define SCL_PLOT 0x03 // Not used yet

// PI is built in the arm_math.h library
#define TWO_PI ((float) PI * 2.0f)

/* Prototypes for testing functions */
void BuildRawData();
void BuildRawFromMockData( float* mockReals );
void ExampleComputeFFTWithOutput();
void PrintTestResults();
void PrintVector(float *vData, uint16_t bufferSize, uint8_t scaleType);

// TODO: Have parameters to specify types of functions/amplitudes/whatnot? to test
void TestFFT()
{
#if USE_MOCK_DATA
	float mock_data[] = MOCK_DATA_10Hz;

	BuildRawFromMockData( mock_data );
#else
	BuildRawData();
#endif

	myComputer.printf("FFT TEST INPUT DATA (SAMPLES)\r\n");
	for( uint8_t adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{
		myComputer.printf("------Analog Pin #%i\n", adcPin);

		/* Print the results of the simulated sampling according to time */
		PrintVector(samples[adcPin], REAL_AND_IMAG_SAMPLE_COUNT, SCL_TIME);
	}

	ExampleComputeFFTWithOutput();

	//PrintTestResults();

	// Run once
	while(1);
}

void ExampleComputeFFTWithOutput()
{
	for( uint8_t adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{
		arm_cfft_f32(S, samples[adcPin], 0, 1);
		arm_cmplx_mag_f32(samples[adcPin], magnitudes[adcPin], FFT_SIZE);
		//magnitudes[adcPin] = 0; // HACK: DC OFFSET THING? It is causing issues with max frequency
	}

	PrintTestResults();
}

void BuildRawFromMockData( float* mockReals )
{
	for( uint8_t adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{
		for( unsigned int i = 0; i < FFT_SIZE; i++ )
		{
			samples[adcPin][i*2] = mockReals[i];

			samples[adcPin][(i*2)+1] = 0.0f; // Imaginary part = 0
		}
	}
}

void BuildRawData()
{
	/* Build raw data */
	double cycles = (((FFT_SIZE-1) * SIGNAL_FREQUENCY) / SAMPLING_RATE_HZ); //Number of signal cycles that the sampling will read

	for( uint8_t adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{
		for( int i = 0; i < REAL_AND_IMAG_SAMPLE_COUNT; i += 2 )
		{
			// i/2 because I want to go from 0 to 1 to 2 to 3 ... not 0,2,4...
			samples[adcPin][i] = int8_t((AMPLITUDE * (sin(( (i/2) * (TWO_PI * cycles) ) / FFT_SIZE))) / 2.0);/* Build data with positive and negative values*/
			//samples[i] = uint8_t((AMPLITUDE * (sin((i * (twoPi * cycles)) / FFT_SIZE) + 1.0)) / 2.0);/* Build data displaced on the Y axis to include only positive values*/

			samples[adcPin][i+1] = 0.0; //Imaginary part must be zeroed in case of looping to avoid wrong calculations and overflows
		}
	}

	//PrintRawFFTInputToSerial();
}

void PrintTestResults()
{
	PrintRawFFTOutputToSerial();
	PrintMaxFrequencyToSerial();
	//PrintBrainWaveDataToSerial(); // Currently not testing brainwave data things -> TODO: Will probably want to later...
}

void PrintVector(float *vData, uint16_t bufferSize, uint8_t scaleType)
{
	for( int i = 0; i < bufferSize; i++ ) // Skipping over all imaginary parts -> TODO: Maybe I want to print vector and give it bufferSize and mean it (not reduced)...
	{
		float abscissa; // The "x coordinate"

		/* Print abscissa value */
		switch (scaleType)
		{
			case SCL_INDEX:
			{
				abscissa = (i * 1.0);
				myComputer.printf("%f Index", abscissa);
			} break;

			case SCL_TIME:
			{
				abscissa = ((i * 1.0) / (float) SAMPLING_RATE_HZ);
				myComputer.printf("%2.5f Seconds", abscissa);
			} break;

			case SCL_FREQUENCY:
			{
				abscissa = ((i * 1.0 * SAMPLING_RATE_HZ) / (float) FFT_SIZE);
				myComputer.printf("%4.3f Hz", abscissa);
			} break;
		}

		myComputer.printf("                       %f\r\n", vData[i]); // \t (tabs) don't seem to work in this printf, so using manual spaces (:
	}
	myComputer.printf("\r\n");
}



/* SPECIFIC MICROCONTROLLER CONFIGURATION FUNCTIONS */

void MyGPIOInit()
{
	GPIO_InitTypeDef GPIO_InitStruct;

	//Configure GPIO pin : PB_6 Pin
	__GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin = GPIO_PIN_6;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_SET);
}

/* BRAINWAVE FUNCTIONS */

void PrintBrainWaveDataToSerial()
{
	myComputer.printf("\nBrain Wave Values...\n");

	myComputer.printf("Epsilon Wave : %ld\n", waveData[EPSILON]);

	myComputer.printf("Delta Wave : %ld\n", waveData[DELTA]);

	myComputer.printf("Theta Wave : %ld\n", waveData[THETA]);

	/* Currently not handling these brainwaves
	myComputer.printf("Low Alpha Wave : %ld\n", waveData[LOW_ALPHA]);

	myComputer.printf("High Alpha Wave : %ld\n", waveData[HIGH_ALPHA]);

	myComputer.printf("Low BetaWave : %ld\n", waveData[LOW_BETA]);

	myComputer.printf("High BetaWave : %ld\n", waveData[HIGH_BETA]);

	myComputer.printf("Low Gamma Wave : %ld\n", waveData[LOW_GAMMA]);

	myComputer.printf("Mid Gamma Wave : %ld\n", waveData[MID_GAMMA]);
	*/

	myComputer.printf("Alpha Wave : %ld\n", waveData[ALPHA]);

	myComputer.printf("Beta Wave : %ld\n", waveData[BETA]);

	myComputer.printf("Gamma Wave : %ld\n", waveData[GAMMA]);

	myComputer.printf("\n");
}

void InitializeBrainWaveData()
{
	for ( int i = 0; i < NUM_WAVES; i++ )
	{
		waveData[i] = 0;
	}
}

void UpdateBrainWaveData()
{
	for( uint8_t adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{
		GroupFrequencies(magnitudes[adcPin], waveData, NUM_WAVES); // This handles the full Root Mean Square for all of the frequency bands
	}

}

void GroupFrequencies(float* realFFTValues, long* waveData, int numWaves)
{
	float frequency;
	float normalized_value;
	// TODO: Make this more scalable (i.e. don't use hardcoded values)
	//float wave_frequencies[numWaves] = {0.5, 3, 7, 9, 12, 17, 30, 40, 50};
	float wave_frequencies[NUM_WAVES] = {0.5, 3, 7, 12, 30, 50};

	int numValuesInBand[ADC_PINS_IN_USE_COUNT][NUM_WAVES];
	memset( numValuesInBand, 0, sizeof(numValuesInBand) ); // Initialize all to 0

	long waveDataAccumulator[ADC_PINS_IN_USE_COUNT][NUM_WAVES];
	memset( waveDataAccumulator, 0, sizeof(waveDataAccumulator) ); // Initialize all to 0

	for( unsigned int i = 0; i < FFT_SIZE / 2; i++ )
	{
		frequency = (i * 1.0 * SAMPLING_RATE_HZ) / FFT_SIZE;

		// TODO: DC Offset hack -> Neglecting Epsilon and Delta wave :(
		// TODO: Add a more sophisticated approach to handling the DC offset
		/*if ( frequency < 2 )
		{
			continue; // Frequency of 0.0 has noise peak due to DC offset, frequencies < 2 have inflated values as well
		}*/

		// Gather all frequencies within the frequency band for a brain wave
		// Find the frequency band to update
		for( int j = 0; j < numWaves; j++ )
		{
			// We are starting from the smallest frequencies and always increasing,
			// so we will catch the smallest frequency band that this frequency fits in to
			if ( frequency < wave_frequencies[j] )
			{
				// Loop over each of the analog pins and accumulate some wave data for the current frequency
				for( uint8_t adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )				// TODO: Can definitely optimize this...
				{
					// TODO: If magnitudes are small such that the numerator < FFT_SIZE...
					//			then smaller than an int (1) and ultimately will be seen as 0 or very small to waveDataAccumulator
					normalized_value = ( (2 * magnitudes[adcPin][i]) / FFT_SIZE );//* 0.5; // Multiply by 0.5 since using Hann windowing function

					//waveData[j] += realFFTValues[i] * realFFTValues[i];
					waveDataAccumulator[adcPin][j] += normalized_value * normalized_value; // Same wave, different pin each inner iter

					numValuesInBand[adcPin][j]++;
				}
				break;
			}
		}

	}

	for( int adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{
		for ( int j = 0; j < numWaves; j++ )
		{
			// Divide by number of accumulated values to complete the calculation of the average
			if( !numValuesInBand[adcPin][j] )
			{
				// TODO: Handle this better...? I'm aiming to not divide by zero, but i haven't had a problem with this yet.
				numValuesInBand[adcPin][j] = 1;
			}
			waveDataAccumulator[adcPin][j] = waveDataAccumulator[adcPin][j] / numValuesInBand[adcPin][j]; // Integer division..

			// Finally, take the square root to complete the calculation of the root mean square for the brain wave signal
			waveDataAccumulator[adcPin][j] = sqrt( waveDataAccumulator[adcPin][j] );

			//waveDataAccumulator[adcPin][i] = sqrt( waveDataAccumulator[adcPin][i] ); // HACK to get smaller values
		}
	}


	// Actual recorded waveData is AVERAGE over all of the channels
	// TODO TODO TODO -> This is probably not a good approach and I should rethink
	for( int i = 0; i < numWaves; i++ )
	{
		for( int adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
		{
			waveData[i] += waveDataAccumulator[adcPin][i];
		}

		waveData[i] /= ADC_PINS_IN_USE_COUNT; // Integer division...
	}

}


/* BRAINWAVE TEST FUNCTION */

void TestBrainwaves( Brainwave* brainwave_signal )
{
	for( int adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{
		float brainwave_frequencies_reals[FFT_SIZE];
		brainwave_signal->simulation( FFT_SIZE, SAMPLING_RATE_HZ, brainwave_frequencies_reals );
		BuildRawFromMockData( brainwave_frequencies_reals );
	}

	ComputeFFT(); // This handles printing raw fft input (what's in the samples' array) if DEBUG = 1

	InitializeBrainWaveData();
	UpdateBrainWaveData();

#if DEBUG
	PrintToSerial(); // Print output of FFT, maximum, and Brainwave data
#endif

	SendToDevice();

	HAL_Delay( BRAINWAVE_TEST_DELAY );
}





/*
void BuildRawBrainwaveData()
{
	// Build raw data
	double cycles = (((FFT_SIZE-1) * SIGNAL_FREQUENCY) / SAMPLING_RATE_HZ); //Number of signal cycles that the sampling will read

	for( uint8_t adcPin = 0; adcPin < ADC_PINS_IN_USE_COUNT; adcPin++ )
	{
		for( int i = 0; i < REAL_AND_IMAG_SAMPLE_COUNT; i += 2 )
		{
			// i/2 because I want to go from 0 to 1 to 2 to 3 ... not 0,2,4...
			samples[adcPin][i] = int8_t((AMPLITUDE * (sin(( (i/2) * (TWO_PI * cycles) ) / FFT_SIZE))) / 2.0);// Build data with positive and negative values
			//samples[i] = uint8_t((AMPLITUDE * (sin((i * (twoPi * cycles)) / FFT_SIZE) + 1.0)) / 2.0);// Build data displaced on the Y axis to include only positive values

			samples[adcPin][i+1] = 0.0; //Imaginary part must be zeroed in case of looping to avoid wrong calculations and overflows
		}
	}

	//PrintRawFFTInputToSerial();
}

*/




