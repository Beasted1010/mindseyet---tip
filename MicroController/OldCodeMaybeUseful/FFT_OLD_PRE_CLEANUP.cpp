#include "mbed.h"
/* Include arm_math.h mathematic functions */
#include "arm_math.h"
/* Include mbed-dsp libraries */
#include "arm_common_tables.h"
#include "arm_const_structs.h"
#include "math_helper.h"

/* FFT settings */
//#define SAMPLES                 128             /* 64 real parts and 64 imaginary parts */
//#define FFT_SIZE                SAMPLES / 2     /* FFT size is always the same size as we have samples, so 256 in our case */
//#define SAMPLING_RATE_HZ		300

const int SAMPLES 				= 128;
const int SAMPLING_RATE_HZ 		= 300;
const unsigned int FFT_SIZE 	= SAMPLES / 2;


#define SAMPLING_TIME_S			(1.0 / SAMPLING_RATE_HZ)
#define SAMPLING_TIME_US		SAMPLING_TIME_S * 1000000

/* Global variables */
float32_t Input[SAMPLES];
float32_t sampler[SAMPLES]={0};
float32_t Output[FFT_SIZE];
bool      trig=0;

/*Function Prototypes */
void sample();

/* MBED class APIs */
DigitalOut myled(LED1);
AnalogIn   myADC(A0);
AnalogOut  myDAC(A3);
Ticker     timer;

void sample(){
    trig=1;
}
    
/*int main() {

    int16_t  i,j;
    float maxValue;            // Max FFT value is stored here
    uint32_t maxIndex;         // Index in Output array where max value is
    
    printf("FFT Example\r\n");
   
    //arm_cfft_instance_f32 S;   // ARM CFFT module
    //float maxValue;            // Max FFT value is stored here
    //uint32_t maxIndex;         // Index in Output array where max value is
    
    while(1) {
            //timer.attach_us(&sample,30); //30us 30,30 250KHz sampling rate ********
    		//timer.attach_us(&sample, SAMPLING_TIME_US);
    		timer.attach(&sample, SAMPLING_TIME_S);

            for (i = 0; i < SAMPLES; i += 2) {
                while (trig==0){}
                trig=0;  
                Input[i] = myADC.read() - 0.5f; //Real part NB removing DC offset
                Input[i + 1] = 0;               //Imaginary Part set to zero
            }
            timer.detach();
            // Init the Complex FFT module, intFlag = 0, doBitReverse = 1
            // NB using predefined arm_cfft_sR_f32_lenXXX, in this case XXX is 256
            arm_cfft_f32(&arm_cfft_sR_f32_len256, Input, 0, 1);

            // Complex Magniture Module put results into Output(Half size of the Input)
            arm_cmplx_mag_f32(Input, Output, FFT_SIZE);
        
            Output[0]=0;
        
        
            //Calculates maxValue and returns corresponding value
            arm_max_f32(Output, FFT_SIZE, &maxValue, &maxIndex);

            printf("Max: %f\r\n", maxValue);
            
        
            /*myDAC=1.0f; //SYNC here!! for Oscillscope at max DAC output for triggering purposes
            wait_us(20);//All FFT values will be lower since max scaling is 0.9 as below
            myDAC=0.0f;//set trigger level on oscilloscope between 0.9 to 1.0 (*3.3Volts) i.e. 3.0 to 3.3Volts
        
            for (i=2; i<FFT_SIZE/2 ; i++){
                myDAC=(Output[i]/maxValue)*0.9f; // All Values scaled to 0.0 to 0.9 for output to DAC
            //    if (Output[i]>0.2){printf("%d %f\r\n",i,(Output[i]/maxValue)*0.9f);}
                wait_us(10);
            }
            myDAC=0.0f;*/
//        }
//}



#include "arm_const_structs.h"

Serial serial_pc(USBTX, USBRX);

const static arm_cfft_instance_f32 *S;
Ticker samplingTimer;
float samples[FFT_SIZE*2];
float magnitudes[FFT_SIZE];
int sampleCounter = 0;


void samplingCallback()
{
    // Read from the ADC and store the sample data
    samples[sampleCounter] = (1023 * myADC) - 511.0;
    // Complex FFT functions require a coefficient for the imaginary part of the input.
    // Since we only have real data, set this coefficient to zero.
    samples[sampleCounter+1] = 0.0;
    // Update sample buffer position and stop after the buffer is filled
    sampleCounter += 2;
    if (sampleCounter >= FFT_SIZE*2) {
        samplingTimer.detach();
    }
}


void samplingBegin()
{
    // Reset sample buffer position and start callback at necessary rate.
    sampleCounter = 0;
    samplingTimer.attach_us(&samplingCallback, 1000000/SAMPLING_RATE_HZ);
}

bool samplingIsDone()
{
    return sampleCounter >= FFT_SIZE*2;
}

int main()
{
    // Set up serial port.
    //serial_pc.baud (9600);
	//serial_pc.baud (38400);
	serial_pc.baud (115200);

    // Init arm_ccft_32
    switch (FFT_SIZE)
    {
    case 16:
        S = & arm_cfft_sR_f32_len16;
        break;
    case 32:
        S = & arm_cfft_sR_f32_len32;
        break;
    case 64:
        S = & arm_cfft_sR_f32_len64;
        break;
    case 128:
        S = & arm_cfft_sR_f32_len128;
        break;
    case 256:
        S = & arm_cfft_sR_f32_len256;
        break;
    case 512:
        S = & arm_cfft_sR_f32_len512;
        break;
    case 1024:
        S = & arm_cfft_sR_f32_len1024;
        break;
    case 2048:
        S = & arm_cfft_sR_f32_len2048;
        break;
    case 4096:
        S = & arm_cfft_sR_f32_len4096;
        break;
    }
    float maxValue = 0.0f;
    unsigned long int testIndex = 0;

    // Begin sampling audio
    samplingBegin();

    while(1)
    {
        // Calculate FFT if a full sample is available.
        if (samplingIsDone())
        {
            // Run FFT on sample data.
            arm_cfft_f32(S, samples, 0, 1);
            printf("\n\nSAMPLES\r\n");
            for(int i = 0;i < FFT_SIZE*2;++i)
                printf("% 8.2f ",samples[i]);
            printf("\r\n\n");
            // Calculate magnitude of complex numbers output by the FFT.
            arm_cmplx_mag_f32(samples, magnitudes, FFT_SIZE); 							magnitudes[0] = 0; // HACK DC OFFSET THING?
            printf("\n\nMAGNITUDES\r\n");
            for(int i = 0;i < FFT_SIZE;++i)
                printf("% 8.2f ",magnitudes[i]);
            printf("\r\n\n");
            arm_max_f32(magnitudes, FFT_SIZE, &maxValue, &testIndex);
            printf("MAX value at magnitudes[%d] (%f Hz) : %+ 4.2f\r\n\n", testIndex, (testIndex * 1.0 * SAMPLING_RATE_HZ) / SAMPLES, maxValue);
            // Wait for user confirmation to restart audio sampling.
            serial_pc.getc();
            samplingBegin();
        }
    }
}
















