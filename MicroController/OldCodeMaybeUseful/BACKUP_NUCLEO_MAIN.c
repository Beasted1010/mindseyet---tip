#include "mbed.h"
/* Include arm_math.h mathematic functions */
#include "arm_math.h"
/* Include mbed-dsp libraries */
#include "arm_common_tables.h"
#include "arm_const_structs.h"
#include "math_helper.h"



/* TODO: CURRENT UNKOWNS
 *
 * 		 Sample timing - Am I going too fast? Too slow? Is the math right? See samplingTimer.attach_us
 *		 ADC manipulation - Should I do a "- 0.5f"? or "* 1023..."? Why? (Saw examples online doing different things)
 *		 User input when doing USER_CONTROLLED_DELAY (ENTER in CoolTerm) causes 2 outputs of data (loops twice)
 *		 	Probably something to do with needing to flush the input buffer?
 *		 Data type for waveData... long? long unsigned?
 *
 *		 My use of "SAMPLE_COUNT" may be misleading. This is the number of samples * 2... Since we include real and imaginary in this value.
 *		 	Consider renaming. FFT_SIZE here is the actual size of the FFT (i.e. the number of total samples to get, combining imaginary and real into 1)
 *
 *		 I think the UpdateBrainWaveData function is busted -> Something to do with FFT_SIZE vs SAMPLE_COUNT (I think I'm including uninitialized data in computation, output is really large value for moderate magnitudes)
 *
 */

/* CONSIDERATIONS
 *
 *		 Performance Boosters
 *		 	Use _t types for more fine grained sizes (i.e. int i in for loop could be replaced with a uint8_t depending on needed size, but no need to be signed)
 *
 *
 * 		 Plotting? - Maybe throw up a plot? What all needed to do so? Do I have the libraries already?
 *
 */




/* FFT settings */
const int SAMPLE_COUNT 			= 128; // 64 real parts and 64 imaginary parts
const int SAMPLING_RATE_HZ 		= 300; // FFT size is always the same size as we have samples, so 256 in our case
const unsigned int FFT_SIZE 	= SAMPLE_COUNT / 2;

/* FFT Test Sampling Info */
#define USE_MOCK_DATA 0

// These are REAL values, imaginary values are all set to 0 (the offsets will be handled by BuildRawFromMockData)
// Current size is 64, this MUST match FFT_SIZE!!!!!!!!!
#define MOCK_DATA_10Hz {0, 10, 20, 28, 36, 42, 47, 49, 49, 47, 44, 38, 30, 22, 12, 2, -7, -17, -26, -35, \
						-41, -46, -49, -49, -48, -45, -39, -32, -24, -14, -4, 5, 15, 24, 33, 40, 45, 48, 49, 49, \
						46, 41, 34, 26, 17, 7, -2, -13, -22, -31, -38, -44, -48, -49, -49, -47, -42, -36, -28, -19, -9, 0, 10, 20}

const float SIGNAL_FREQUENCY 	= 10; // Frequency of signal that is being sampled
const uint8_t AMPLITUDE 		= 100; // Amplitude of signal that is being sampled


/* Timing constants */
#define SAMPLING_TIME_S			(1.0 / SAMPLING_RATE_HZ)
#define SAMPLING_TIME_US		SAMPLING_TIME_S * 1000000

/* Program Modes */
#define AT_COMMAND_MODE 0
#define TEST_MODE 0

/* Miscellaneous Defines */
#define DELAY 0 // Milliseconds

#define USER_CONTROLLED_DELAY 0
#define DEBUG 1

/* Function Prototypes */

/**Sampling*/
void samplingCallback();
void samplingBegin();
bool samplingIsDone();

/**Serial*/
void PrintToSerial();
void PrintRawFFTInputToSerial();
void PrintRawFFTOutputToSerial();
void PrintMaxFrequencyToSerial();
void PrintBrainWaveDataToSerial();
void WaitForBluetoothResponse();
void ReadBluetoothCharacter();
void PrintBluetoothCharacterToComputer();
void ReadComputerCharacter();
void PrintComputerCharacterToBluetooth();
void SendToDevice();
void PrepareBluetooth();

/**FFT*/
void SampleAnalog();
void ComputeFFT();
void TestFFT();

/**Microcontroller Configuration*/
void MyGPIOInit();

/**Brainwave*/
void PrintBrainWaveDataToSerial();
void InitializeBrainWaveData();
void UpdateBrainWaveData();
void GroupFrequencies(float* realFFTValues, long* waveData, int numWaves);

/* Global variables */
/** MBED class APIs */
Serial myComputer(USBTX, USBRX);
Serial blue(PA_9, PA_10); // D1, D0
DigitalOut myled(LED1);

AnalogIn   myADC0(A0);
AnalogIn   myADC1(A1);
AnalogIn   myADC2(A2);
AnalogIn   myADC3(A3);

enum myADCs // To serve as index into samples (to indicate which analog channel to use)
{
	MY_ADC0,
	MY_ADC1,
	MY_ADC2,
	MY_ADC3,
	ADC_COUNT
};

const uint8_t ADC_PINS_IN_USE_COUNT = 1;

Ticker samplingTimer;

const static arm_cfft_instance_f32 *S;

float TEMP[ADC_PINS_IN_USE_COUNT][SAMPLE_COUNT];
//float samples[ADC_PINS_IN_USE_COUNT][SAMPLE_COUNT];
float samples[SAMPLE_COUNT];
float magnitudes[FFT_SIZE];
float normalizedValues[FFT_SIZE];
unsigned int sampleCounter = 0;

/** Brainwave Data */
enum WaveData
{
  EPSILON, // <0.5 Hz
  DELTA, // 0.5-3 Hz
  THETA, // ~4-7 Hz
  //LOW_ALPHA, // ~8-9 Hz
  //HIGH_ALPHA, // ~10-12 Hz
  ALPHA, // ~8-12 Hz
  //LOW_BETA, // 13-17 Hz
  //HIGH_BETA, // 18-30 Hz
  BETA, // 13-30 Hz
  //LOW_GAMMA, // 31-40 Hz
  //MID_GAMMA, // 41-50 Hz
  GAMMA, // 31-50 Hz

  NUM_WAVES // Keeps track of how many brain waves are in this enumeration
};

long waveData[NUM_WAVES]; // TODO: May want this to be a long int? Even better, a long unsigned int


int main()
{
	MyGPIOInit();

	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_6);
	HAL_Delay(100);

    // Set up serial port.
    //myComputer.baud (9600);
	//myComputer.baud (38400);
	myComputer.baud(115200);

	blue.baud(38400); // Needs to be 38400 because this is HC_05's default baud rate and I haven't changed it with AT Command Mode

	//PrepareBluetooth(); // NOTE: Maybe I still want this?
	// TEMPORARY HACK (since my serial commands don't seem to be sending through CoolTerm (program that I'm using to get serial output from device))
	myComputer.printf("HC-05 SET\r\n");

	blue.printf("AT+INIT\r\n");
	WaitForBluetoothResponse();

	blue.printf("AT+INQ\r\n");
	WaitForBluetoothResponse();



	// TODO TODO TODO TODO TODO TOD -> I DON'T USE S anywhere... Should I?
    // Init arm_ccft_32
    switch (FFT_SIZE)
    {
    	case 16:
			S = & arm_cfft_sR_f32_len16;
			break;
		case 32:
			S = & arm_cfft_sR_f32_len32;
			break;
		case 64:
			S = & arm_cfft_sR_f32_len64;
			break;
		case 128:
			S = & arm_cfft_sR_f32_len128;
			break;
		case 256:
			S = & arm_cfft_sR_f32_len256;
			break;
		case 512:
			S = & arm_cfft_sR_f32_len512;
			break;
		case 1024:
			S = & arm_cfft_sR_f32_len1024;
			break;
		case 2048:
			S = & arm_cfft_sR_f32_len2048;
			break;
		case 4096:
			S = & arm_cfft_sR_f32_len4096;
			break;
    }

    while(1)
    {
#if AT_COMMAND_MODE
    	ATCommandModeDEBUGLoop();
#elif TEST_MODE
    	TestFFT();
#else
    	if( 1 ) // Hat is on... TODO: Need a reliable way to know if this is the case
    	{
			/*SAMPLING*/
			SampleAnalog();

			// Calculate FFT and update brainwave data only if a full sample is available.
			if( samplingIsDone() )
			{
				/*HANDLE DC OFFSET*/ 							//-> TODO: NOT IMPLEMENTED YET
				//AdjustForDCOffset(magnitudes, SAMPLE_COUNT);

				/*FFT*/
				ComputeFFT();

				/*HANDLE WAVE DATA*/
				InitializeBrainWaveData();
				UpdateBrainWaveData();

#if DEBUG
				/*PRINT RESULTS TO SERIAL*/
				PrintToSerial();
// #elif PLOT -> TODO: Maybe throw up a plot? What all needed to do so? Do I have the libraries already?
#endif

				SendToDevice();
			}

#if DELAY
			HAL_Delay(DELAY);
#endif
    	}
    	else
    	{
    		// Hat is off!
    	}
#endif

    }
}







/* SERIAL FUNCTIONS */

void PrintToSerial()
{
	PrintRawFFTOutputToSerial();
	PrintMaxFrequencyToSerial();
	PrintBrainWaveDataToSerial();
}

void PrintRawFFTInputToSerial()
{
	myComputer.printf("\r\n\nFFT INPUT (SAMPLES)\r\n");
	for( unsigned int i = 0; i < SAMPLE_COUNT; ++i )
		myComputer.printf("% 8.2f ", samples[i]);
	myComputer.printf("\r\n\n");
}

void PrintRawFFTOutputToSerial()
{
	myComputer.printf("\r\n\nFFT OUTPUT (MAGNITUDES)\r\n");
	for( unsigned int i = 0; i < FFT_SIZE; ++i )
		myComputer.printf("% 8.2f ", magnitudes[i]);
	myComputer.printf("\r\n\n");
}

void PrintMaxFrequencyToSerial()
{
	float maxValue = 0.0f;
	unsigned long int maxIndex = 0;
	arm_max_f32(magnitudes, FFT_SIZE, &maxValue, &maxIndex);
	myComputer.printf("MAX value at magnitudes[%lu] (%f Hz) : %+ 4.2f\r\n\n", maxIndex, (maxIndex * 1.0 * SAMPLING_RATE_HZ) / FFT_SIZE, maxValue);
}

void WaitForBluetoothResponse()
{
	HAL_Delay(500);
	char buffer[100];
	while(blue.readable())
	{
		blue.scanf("%s", buffer);
		myComputer.printf("%s", buffer);
	}
}

void ReadBluetoothCharacter()
{
	char buffer[100];
	while(blue.readable())
	{
		blue.scanf("%s", buffer);
		myComputer.printf("%s", buffer);
	}
}

void PrintBluetoothCharacterToComputer()
{
	while(blue.readable())
	{
		myComputer.putc(blue.getc());
		//myComputer.printf("%c", blue.getc());
	}
}

void ReadComputerCharacter()
{
	char buffer[100];
	while(myComputer.readable())
	{
		myComputer.scanf("%s", buffer);
		myComputer.printf("%s", buffer);
		//blue.printf("%s", buffer);
	}
}

void PrintComputerCharacterToBluetooth()
{
	char buffer[10] = {0};
	while(myComputer.readable())
	{
		myComputer.scanf("%9s", buffer);
		myComputer.printf("Data: %s\r\n", buffer);
		blue.printf("%s\r\n", buffer);


		//blue.puts(buffer);
	}
}

void SendToDevice()
{
	blue.printf("C");
	blue.printf(",");
	for(int i = 0; i < NUM_WAVES; i++)
	{
		blue.printf("temp");
		if( i != NUM_WAVES - 1 )
		{
			blue.printf(",");
		}
	}
	blue.printf(";\r\n");

	myComputer.printf("Sent to device...\r\n");
}

void PrepareBluetooth()
{
  myComputer.printf("Checking response with AT...");
  blue.printf("AT\r\n");
  WaitForBluetoothResponse();

  myComputer.printf("Setting name...");
  blue.printf("AT+NAME=MindSeyeT\r\n"); // Sets name to "MindSeyeT"
  WaitForBluetoothResponse();

  myComputer.printf("Setting password...");
  blue.printf("AT+PSWD=1234\r\n"); // Sets password to "1234"
  WaitForBluetoothResponse();

  myComputer.printf("End of setup");
  myComputer.printf("Enter AT commands:");
}



/* SAMPLING FUNCTIONS */

void SampleAnalog()
{
#if USER_CONTROLLED_DELAY
	// Wait for user confirmation to restart sampling.
	myComputer.getc();
#endif

	samplingBegin();
}

void samplingCallback()
{
    // Read from the ADC and store the sample data
    samples[sampleCounter] = (1023 * myADC0.read()) - 511.0;
    //samples[sampleCounter] = myADC0.read();// - 0.5f;

    // Complex FFT functions require a coefficient for the imaginary part of the input.
    // Since we only have real data, set this coefficient to zero.
    samples[sampleCounter+1] = 0.0;
    // Update sample buffer position and stop after the buffer is filled
    sampleCounter += 2;
    if ( sampleCounter >= FFT_SIZE*2 ) {
        samplingTimer.detach();
    }
}


void samplingBegin()
{
    // Reset sample buffer position and start callback at necessary rate.
    sampleCounter = 0;
    samplingTimer.attach_us(&samplingCallback, 1000000/SAMPLING_RATE_HZ);
}

bool samplingIsDone()
{
    return sampleCounter >= FFT_SIZE*2;
}



/* FFT FUNCTIONS */

void ComputeFFT()
{
	// Run FFT on sample data.
	arm_cfft_f32(S, samples, 0, 1);

#if DEBUG
	PrintRawFFTInputToSerial();
#endif

	// Calculate magnitude of complex numbers output by the FFT.
	arm_cmplx_mag_f32(samples, magnitudes, FFT_SIZE);
	magnitudes[0] = 0; // HACK: DC OFFSET THING? It is causing issues with max frequency
}


/* FFT TEST FUNCTION */

// How first component of vector should be interpreted (index/time/freq), i.e. what should the abscissa (x-coord) be interpreted as (an INDEX, s.a. 0,1,2,3,... or TIME i.e. 1/Hz or as FREQUENCY s.a. 0hz, 1hz, 2hz,...)
#define SCL_INDEX 0x00
#define SCL_TIME 0x01
#define SCL_FREQUENCY 0x02

#define SCL_PLOT 0x03 // Not used yet

// PI is built in the arm_math.h library
#define TWO_PI ((float) PI * 2.0f)

/* Prototypes for testing functions */
void BuildRawData();
void BuildRawFromMockData( float* mockReals );
void ExampleComputeFFTWithOutput();
void PrintTestResults();
void PrintVector(float *vData, uint16_t bufferSize, uint8_t scaleType);

// TODO: Have parameters to specify types of functions/amplitudes/whatnot? to test
void TestFFT()
{
#if USE_MOCK_DATA
	float mock_data[] = MOCK_DATA_10Hz;

	BuildRawFromMockData( mock_data );
#else
	BuildRawData();
#endif

	/* Print the results of the simulated sampling according to time */
	myComputer.printf("Data:\r\n");
	PrintVector(samples, SAMPLE_COUNT, SCL_TIME);

	ExampleComputeFFTWithOutput();

	//PrintTestResults();

	// Run once
	while(1);
}

void ExampleComputeFFTWithOutput()
{
	arm_cfft_f32(S, samples, 0, 1);
	arm_cmplx_mag_f32(samples, magnitudes, FFT_SIZE);
	//magnitudes[0] = 0; // HACK: DC OFFSET THING? It is causing issues with max frequency

	PrintTestResults();
}

void BuildRawFromMockData( float* mockReals )
{
	for( unsigned int i = 0; i < FFT_SIZE; i++ )
	{
		samples[i*2] = mockReals[i];

		samples[(i*2)+1] = 0.0f; // Imaginary part = 0
	}
}

void BuildRawData()
{
	/* Build raw data */
	double cycles = (((FFT_SIZE-1) * SIGNAL_FREQUENCY) / SAMPLING_RATE_HZ); //Number of signal cycles that the sampling will read

	for( int i = 0; i < SAMPLE_COUNT; i += 2 )
	{
		// i/2 because I want to go from 0 to 1 to 2 to 3 ... not 0,2,4...
		samples[i] = int8_t((AMPLITUDE * (sin(( (i/2) * (TWO_PI * cycles) ) / FFT_SIZE))) / 2.0);/* Build data with positive and negative values*/
		//samples[i] = uint8_t((AMPLITUDE * (sin((i * (twoPi * cycles)) / FFT_SIZE) + 1.0)) / 2.0);/* Build data displaced on the Y axis to include only positive values*/

		samples[i+1] = 0.0; //Imaginary part must be zeroed in case of looping to avoid wrong calculations and overflows
	}

	//PrintRawFFTInputToSerial();
}

void PrintTestResults()
{
	PrintRawFFTOutputToSerial();
	PrintMaxFrequencyToSerial();
	//PrintBrainWaveDataToSerial(); // Currently not testing brainwave data things -> TODO: Will probably want to later...
}

void PrintVector(float *vData, uint16_t bufferSize, uint8_t scaleType)
{
	for( int i = 0; i < bufferSize; i++ ) // Skipping over all imaginary parts -> TODO: Maybe I want to print vector and give it bufferSize and mean it (not reduced)...
	{
		float abscissa; // The "x coordinate"

		/* Print abscissa value */
		switch (scaleType)
		{
			case SCL_INDEX:
			{
				abscissa = (i * 1.0);
				myComputer.printf("%f Index", abscissa);
			} break;

			case SCL_TIME:
			{
				abscissa = ((i * 1.0) / (float) SAMPLING_RATE_HZ);
				myComputer.printf("%2.5f Seconds", abscissa);
			} break;

			case SCL_FREQUENCY:
			{
				abscissa = ((i * 1.0 * SAMPLING_RATE_HZ) / (float) FFT_SIZE);
				myComputer.printf("%4.3f Hz", abscissa);
			} break;
		}

		myComputer.printf("                       %f\r\n", vData[i]); // \t (tabs) don't seem to work in this printf, so using manual spaces (:
	}
	myComputer.printf("\r\n");
}



/* SPECIFIC MICROCONTROLLER CONFIGURATION FUNCTIONS */

void MyGPIOInit()
{
	GPIO_InitTypeDef GPIO_InitStruct;

	//Configure GPIO pin : PB_6 Pin
	__GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin = GPIO_PIN_6;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_SET);
}


/* BRAINWAVE FUNCTIONS */

void PrintBrainWaveDataToSerial()
{
	myComputer.printf("\nBrain Wave Values...\n");

	myComputer.printf("Epsilon Wave : %ld\n", waveData[EPSILON]);

	myComputer.printf("Delta Wave : %ld\n", waveData[DELTA]);

	myComputer.printf("Theta Wave : %ld\n", waveData[THETA]);

	/* Currently not handling these brainwaves
	myComputer.printf("Low Alpha Wave : %ld\n", waveData[LOW_ALPHA]);

	myComputer.printf("High Alpha Wave : %ld\n", waveData[HIGH_ALPHA]);

	myComputer.printf("Low BetaWave : %ld\n", waveData[LOW_BETA]);

	myComputer.printf("High BetaWave : %ld\n", waveData[HIGH_BETA]);

	myComputer.printf("Low Gamma Wave : %ld\n", waveData[LOW_GAMMA]);

	myComputer.printf("Mid Gamma Wave : %ld\n", waveData[MID_GAMMA]);
	*/

	myComputer.printf("Alpha Wave : %ld\n", waveData[ALPHA]);

	myComputer.printf("Beta Wave : %ld\n", waveData[BETA]);

	myComputer.printf("Gamma Wave : %ld\n", waveData[GAMMA]);

	myComputer.printf("\n");
}

void InitializeBrainWaveData()
{
	for ( int i = 0; i < NUM_WAVES; i++ )
	{
		waveData[i] = 0;
	}
}

void UpdateBrainWaveData()
{
	GroupFrequencies(magnitudes, waveData, NUM_WAVES); // This handles the full Root Mean Square for all of the frequency bands
}

void GroupFrequencies(float* realFFTValues, long* waveData, int numWaves)
{
	float frequency;
	// TODO: Make this more scalable (i.e. don't use hardcoded values)
	//float wave_frequencies[numWaves] = {0.5, 3, 7, 9, 12, 17, 30, 40, 50};
	float wave_frequencies[numWaves] = {0.5, 3, 7, 12, 30, 50};
	int numValuesInBand[numWaves] = {0};

	for (unsigned int i = 0; i < FFT_SIZE / 2; i++)
	{
		frequency = (i * 1.0 * SAMPLING_RATE_HZ) / FFT_SIZE;

		// TODO: DC Offset hack -> Neglecting Epislon and Delta wave :(
		// TODO: Add a more sophisticated approach to handling the DC offset
		if ( frequency < 2 )
		{
			continue; // Frequency of 0.0 has noise peak due to DC offset, frequencies < 2 have inflated values as well
		}

		// Gather all frequencies within the frequency band for a brain wave

		// Find the frequency band to update
		for ( int j = 0; j < numWaves; j++ )
		{
			normalizedValues[i] = ( (2 * magnitudes[i]) / FFT_SIZE ) * 0.5; // Multiply by 0.5 since using Hann windowing function

			// We are starting from the smallest frequencies and always increasing,
			// so we will catch the smallest frequency band that this frequency fits in to
			if ( frequency < wave_frequencies[j] )
			{
				//waveData[j] += realFFTValues[i] * realFFTValues[i];
				waveData[j] += normalizedValues[i] * normalizedValues[i];
				numValuesInBand[j]++;
				break;
			}
		}
	}


	for ( int i = 0; i < numWaves; i++ )
	{
		// Divide by number of accumulated values to complete the calculation of the average
		if ( !numValuesInBand[i] )
		{
			// TODO: Handle this better...? I'm aiming to not divide by zero, but i haven't had a problem with this yet.
			numValuesInBand[i] = 1;
		}
		waveData[i] = waveData[i] / numValuesInBand[i]; // Integer division..

		// Finally, take the square root to complete the calculation of the root mean square for the brain wave signal
		waveData[i] = sqrt( waveData[i] );

		//waveData[i] = sqrt( waveData[i] ); // HACK to get smaller values
	}

}
























