/*
 * This is a moving average filter with 5 moving points.
 * A moving average filter is used to smooth the siganl.
 * It is especially useful for high frequency addition noise
 * and sudden spikes. This is useful for our applicantions for:
 * 1. any high frequency signal that is intorduced to the signal 
 *    sice the brainwave signal is supposed to be low frequency.
 * 2. In case of sudden movement caused by moving the hat around
 *    which results in spikes in the signal.
 * A moving-average filter deals with both these symptoms.
 * 
 * The following is the function decleration:
 * void moving_avg_filter( float samples[], int num_of_samples)
 * 
 * Only pass in the arry that holds the samples as first argument
 * and pass its size i.e. the number of samples as the second.
 * 
 * Calling the function will replace the old values with the filter values.
 * 
 * This implementation gets rid of dynamic memory allocation as an 
 * alternative if the microcontroller gives issues. 
 * Refer to moving_avg_dm.hpp for alternate implementation.
 * 
 * For more info on the moving average, see Mo's journal, WEEK 4/25
 * 
 */
#include <iostream>
void moving_avg_filter( float samples[], int num_of_samples)
{
    const int AVG_BUFFER_SIZE = 5;
    float avg_buffer[AVG_BUFFER_SIZE] = {0};
    float sum_buffer[AVG_BUFFER_SIZE] = {0};
    float moving_sum = 0;
    bool buff_flag = false;
    int avg_indx = 0;
    for(int i = 0 ; i < num_of_samples; i++)
    {
        // this starts when the first sample is no longer used to compute the avg so it can be overwritten
        if (buff_flag){
            // overwriting is done with a backward offset of the buffer size
            samples[i - AVG_BUFFER_SIZE] = avg_buffer[avg_indx];
        }
        //Subtract the last number added and add current element to moving_sum
        moving_sum = moving_sum - sum_buffer[avg_indx] + samples[i];
        //replace the oldest num in the beffer by the current element
        sum_buffer[avg_indx] = samples[i];
        //store current filtered value/average in temp array
        avg_buffer[avg_indx] = moving_sum / AVG_BUFFER_SIZE;
        //reset avg_indx to circle the avg_buffer
        avg_indx++;
        if (avg_indx >= AVG_BUFFER_SIZE){
            avg_indx = 0;
            buff_flag = true;
        }
    }

    // when end of loop is reached, overwite the rest by what is left in the buffer
    for (int j = num_of_samples - AVG_BUFFER_SIZE ; j < num_of_samples; j++){
        samples[j] = avg_buffer[avg_indx++];
        // circle the buffer
        if (avg_indx >= AVG_BUFFER_SIZE){
            avg_indx = 0;
        }
    }
}