/*
 * This is a moving average filter with 5 moving points.
 * A moving average filter is used to smooth the siganl.
 * It is especially useful for high frequency addition noise
 * and sudden spikes. This is useful for our applicantions for:
 * 1. any high frequency signal that is intorduced to the signal 
 *    sice the brainwave signal is supposed to be low frequency.
 * 2. In case of sudden movement caused by moving the hat around
 *    which results in spikes in the signal.
 * A moving-average filter deals with both these symptoms.
 * 
 * The following is the function decleration:
 * void moving_avg_filter( float samples[], int num_of_samples)
 * 
 * Only pass in the arry that holds the samples as first argument
 * and pass its size i.e. the number of samples as the second.
 * 
 * Calling the function will replace the old values with the filter values.
 * 
 * This implementation uses dynamic memory allocation. For an implementation
 * that does not use dynamic memory, refer to moving_avg_ndm.hpp
 * 
 * For more info on the moving average, see Mo's journal, WEEK 4/25
 * 
 */

void moving_avg_filter( float samples[], int num_of_samples)
{
    const int AVG_BUFFER_SIZE = 5;
    float avg_buffer[AVG_BUFFER_SIZE] = {0};
    float *filtered_arr = new float[num_of_samples];
    float moving_sum = 0;

    for(int i = 0, avg_indx = 0; i < num_of_samples; i++)
    {
        //Subtract the last number added and add current element to moving_sum
        moving_sum = moving_sum - avg_buffer[avg_indx] + samples[i];
        //replace the oldest num in the beffer by the current element
        avg_buffer[avg_indx] = samples[i];
        //store current filtered value/average in temp array
        filtered_arr[i] = moving_sum / AVG_BUFFER_SIZE;
        //reset avg_indx to circle the avg_buffer
        avg_indx++;
        if (avg_indx >= AVG_BUFFER_SIZE){
            avg_indx = 0;
        }
    }

    // overwrite old values
    for(int i = 0; i < num_of_samples; i++){
        samples[i] = filtered_arr[i];
    }

    // clean up
    delete[] filtered_arr;
}