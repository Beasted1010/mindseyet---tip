#include <iostream>
//#include "moving_avg_dm.hpp" // uses dynamic memory allocation, see header file for info
#include "moving_avg_ndm.hpp" // no dynamic memory allocation, see header file for info

int main()
{
    float samples[] = {20, 10, 18, 50, 20, 100, 18, 10, 13, 400, 50, 40, 300};
    int num_of_samples = sizeof(samples)/sizeof(float);

    moving_avg_filter(samples, num_of_samples);

    for(int i = 0; i < num_of_samples; i++)
        std::cout << samples[i] << std::endl;

  

  return 0;
}
