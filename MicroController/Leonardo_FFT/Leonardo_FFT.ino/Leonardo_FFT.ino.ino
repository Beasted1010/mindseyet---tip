#include <arduinoFFT.h>
#include <SoftwareSerial.h>

int sensorPin = A0; // This will be connected to the output of the Amplifier
int sensorValue = 0;

/* FFT Parameters
 *  
 *  Source: https://electronics.stackexchange.com/questions/12407/what-is-the-relation-between-fft-length-and-frequency-resolution
 * 
 * Sampling Frequency, or Sampling Rate: The rate at which we sample data for the FFT.
 *      By "Nyquist-Shannon" sampling theorem -> To ensure an accurate output, we want (Sampling Rate / 2)    -> ???
 * Samples: This is the number of samples we collect for the FFT 
 *      The number of FFT bins we will have is (Samples / 2) -> e.g. 128 samples -> 64 FFT bins
 * 
 * The Samples paired with the Sampling Frequency provide the bin width
 *    Bin Width = (Sampling Frequency / 2) / (Number of Samples / 2)
 *    
 *    Basically you can just do -> Sampling Rate / Number of Samples -> Since both numerator and denominator are halved.
 *    
 *  
 * E.g. SAMPLES = 128, SAMPLING_FREQUENCY = 128
 *      # FFT Bins -> 64 Bins (128 Samples / 2)
 *      Frequency content -> 64 Hz (128 Sampling Frequency / 2)
 *      Bin width = (64 / 64) = 1 Hz
 *      
 *  
 */
#define SAMPLES 128             //Must be a power of 2
//#define SAMPLES 32
#define SAMPLING_FREQUENCY 128 //Hz, must be less than 10000 due to ADC
//#define SAMPLING_FREQUENCY 150 // 3 times max expected frequency

#define HAT_OFF_THRESHOLD_MIN 0
#define HAT_OFF_THRESHOLD_MAX 500

#define MAX_NUMBER_ALLOWED_THRESHOLD_BREACHES 3

//#define DEBUG 1


arduinoFFT FFT = arduinoFFT();

unsigned int sampling_period_us;
unsigned long microseconds;

double vReal[SAMPLES];
double vImag[SAMPLES];

bool hatOn = true;

enum WaveData
{
  EPSILON, // <0.5 Hz
  DELTA, // 0.5-3 Hz
  THETA, // ~4-7 Hz
  //LOW_ALPHA, // ~8-9 Hz
  //HIGH_ALPHA, // ~10-12 Hz
  ALPHA, // ~8-12 Hz
  //LOW_BETA, // 13-17 Hz
  //HIGH_BETA, // 18-30 Hz
  BETA, // 13-30 Hz
  //LOW_GAMMA, // 31-40 Hz
  //MID_GAMMA, // 41-50 Hz
  GAMMA, // 31-50 Hz
  
  NUM_WAVES // Keeps track of how many brain waves are in this enumeration
};

int waveData[NUM_WAVES]; // TODO: May want this to be a long int? Even better, a long unsigned int

void InitializeBrainWaveData()
{
  for( int i = 0; i < NUM_WAVES; i++ )
  {
    waveData[i] = 0;
  }
}

void SampleAnalog()
{
  hatOn = true;
  int num_breached_threshold = 0;
  
  for (int i = 0; i < SAMPLES; i++)
  {
    microseconds = micros();    // TODO: Overflows after around 70 minutes!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?

    // analogRead documentation: https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/ -> We could use this function to read from pins hooked up to a sine wave generator
    vReal[i] = analogRead(sensorPin); // Read in values from the sensor analog pin -> Will map input voltages between 0 and 5 V into integer values between 0 and 1023, and thus converts analog to digital

    if( vReal[i] < HAT_OFF_THRESHOLD_MIN || vReal[i] > HAT_OFF_THRESHOLD_MAX )
    {
      num_breached_threshold++;
      hatOn = false;
    }
    
    vImag[i] = 0;

    while (micros() < (microseconds + sampling_period_us)) {
    }
  }

  if( num_breached_threshold > MAX_NUMBER_ALLOWED_THRESHOLD_BREACHES )
  {
    hatOn = false;
  }
}

void ComputeFFT()
{
  FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
  FFT.Compute(vReal, vImag, SAMPLES, FFT_FORWARD);
  FFT.ComplexToMagnitude(vReal, vImag, SAMPLES);
}

void GroupFrequencies(double* realFFTValues, int* waveData, int numWaves)
{
  float frequency;
  // TODO: Make this more scalable (i.e. don't use hardcoded values)
  //float wave_frequencies[numWaves] = {0.5, 3, 7, 9, 12, 17, 30, 40, 50};
  float wave_frequencies[numWaves] = {0.5, 3, 7, 12, 30, 50};
  int numValuesInBand[numWaves] = {0};
  
  for (int i = 0; i < (SAMPLES / 2); i++)
  {
    frequency = (i * 1.0 * SAMPLING_FREQUENCY) / SAMPLES;

    // TODO: DC Offset hack -> Neglecting Epislon and Delta wave :(
    // TODO: Add a more sophisticated approach to handling the DC offset
    if( frequency < 2 )
    {
      continue; // Frequency of 0.0 has noise peak due to DC offset, frequencies < 2 have inflated values as well
    }

    // Gather all frequencies within the frequency band for a brain wave

    // Find the frequency band to update
    for( int j = 0; j < numWaves; j++ )
    {
      // We are starting from the smallest frequencies and always increasing, 
      // so we will catch the smallest frequency band that this frequency fits in to
      if( frequency < wave_frequencies[j] )
      {
        waveData[j] += realFFTValues[i] * realFFTValues[i];
        numValuesInBand[j]++;
        break;
      }
    }
  }


  for( int i = 0; i < numWaves; i++ )
  {
    // Divide by number of accumulated values to complete the calculation of the average
    if( !numValuesInBand[i] )
    {
      // TODO: Handle this better...? I'm aiming to not divide by zero, but i haven't had a problem with this yet.
      numValuesInBand[i] = 1;
    }
    waveData[i] = waveData[i] / numValuesInBand[i]; // Integer division..

    // Finally, take the square root to complete the calculation of the root mean square for the brain wave signal
    waveData[i] = sqrt( waveData[i] );
  }

}

int CalculateMean(int* valueArray, int arraySize)
{
  int mean = 0;
  for( int i = 0; i < arraySize; i++ )
  {
    mean += valueArray[i];
  }

  return mean;
}

int ComputeRootMeanSquare(int* valueArray, int arraySize)
{
  float rms = 0;

  // Sum of squares
  for( int i = 0; i < arraySize; i++ )
  {
    rms += valueArray[i] * valueArray[i];
  }

  // Average of the sum of squares
  rms /= arraySize;

  // Square root the result
  rms = sqrt(rms);

  return rms;
}

void UpdateBrainWaveData()
{
  GroupFrequencies(vReal, waveData, NUM_WAVES); // This handles the full Root Mean Square for all of the frequency bands
}

int FindMaxMagnitude(double* realFFTValues, int numSamples)
{
  if( numSamples < 1 )
  {
    // ERROR
    Serial.print("ERROR: There are no samples to find the max magnitude");
  }
  
  int max_magnitude = realFFTValues[0];
  for( int i = 1; i < numSamples; i++ )
  {
    max_magnitude = realFFTValues[i] > max_magnitude ? realFFTValues[i] : max_magnitude;
    /*if( realFFTValues[i] > max_magnitude )
    {
      max_magnitude = realFFTValues[i];
    }*/
  }

  return max_magnitude;
}

int FindMinMagnitude(double* realFFTValues, int numSamples)
{
  if( numSamples < 1 )
  {
    // ERROR
    Serial.print("ERROR: There are no samples to find the min magnitude");
  }
  
  int min_magnitude = realFFTValues[0];
  for( int i = 1; i < numSamples; i++ )
  {
    min_magnitude = realFFTValues[i] > min_magnitude ? realFFTValues[i] : min_magnitude;
    /*if( realFFTValues[i] < min_magnitude )
    {
      min_magnitude = realFFTValues[i];
    }*/
  }
  
  return min_magnitude;
}

void AdjustForDCOffset(double* realFFTValues, int numSamples)
{
  int max_magnitude = FindMaxMagnitude(realFFTValues, numSamples);
  int min_magnitude = FindMinMagnitude(realFFTValues, numSamples);

  int adjustment = max_magnitude - min_magnitude;

  for( int i = 0; i < numSamples; i++ )
  {
    realFFTValues[i] -= adjustment;
  }
  
}

void PrintRawFFTOutputToSerial()
{
  double peak = FFT.MajorPeak(vReal, SAMPLES, SAMPLING_FREQUENCY);
  
  Serial.print("FREQUENCY MOST DOMINANT IS... ");
  Serial.println(peak);     //Print out what frequency is the most dominant.

  // There are SAMPLE/2 bins... So iterate over each bin and print out the REAL value, which corresponds to the amplitude
  Serial.println("\nThe recorded samples...");
  for (int i = 0; i < (SAMPLES / 2); i++)
  {
    /*View all these three lines in serial terminal to see which frequencies has which amplitudes*/
    /*NOTE: Due to DC offset there will be a peak at 0*/

    Serial.print("Frequency of... ");
    Serial.print((i * 1.0 * SAMPLING_FREQUENCY) / SAMPLES, 1);
    Serial.print(" has amplitude of... ");
    Serial.println(vReal[i], 1);    //View only this line in serial plotter to visualize the bins
  }
}


void PrintBrainWaveDataToSerial()
{
  Serial.println();
  Serial.println("Brain Wave Values...");
  Serial.println();
  
  Serial.print("Epsilon Wave : ");
  Serial.println(waveData[EPSILON]);
  
  Serial.print("Delta Wave : ");
  Serial.println(waveData[DELTA]);
  
  Serial.print("Theta Wave : ");
  Serial.println(waveData[THETA]);
  
  /*Serial.print("Low Alpha Wave : ");
  Serial.println(waveData[LOW_ALPHA]);
  
  Serial.print("High Alpha Wave : ");
  Serial.println(waveData[HIGH_ALPHA]);
  
  Serial.print("Low BetaWave : ");
  Serial.println(waveData[LOW_BETA]);
  
  Serial.print("High BetaWave : ");
  Serial.println(waveData[HIGH_BETA]);
  
  Serial.print("Low Gamma Wave : ");
  Serial.println(waveData[LOW_GAMMA]);
  
  Serial.print("Mid Gamma Wave : ");
  Serial.println(waveData[MID_GAMMA]);*/

  Serial.print("Alpha Wave : ");
  Serial.println(waveData[ALPHA]);

  Serial.print("Beta Wave : ");
  Serial.println(waveData[BETA]);

  Serial.print("Gamma Wave : ");
  Serial.println(waveData[GAMMA]);

  Serial.println();
}

void PrintToSerial()
{
  PrintRawFFTOutputToSerial();
  PrintBrainWaveDataToSerial();

  Serial.println("DONE PRINTING TO SERIAL!");
}

// Allow module time to receive command and reply
void WaitForResponse()
{
  delay(500);
  while(Serial1.available()) {
    Serial.write(Serial1.read());
  }
}

// OLD // Order sent: SYNC, EPSILON, DELTA, THETA, LOW_ALPHA, HIGH_ALPHA, LOW_BETA, HIGH_BETA, LOW_GAMMA, MID_GAMMA
// Order sent: SYNC, EPSILON, DELTA, THETA, ALPHA, BETA, GAMMA
void SendToDevice()
{
  Serial1.print("SYNC");
  Serial1.print(",");
  for(int i = 0; i < NUM_WAVES; i++)
  {
    Serial1.print(waveData[i]);
    if( i != NUM_WAVES - 1 )
      Serial1.print(",");
  }
  Serial1.print(";");

  Serial.println("Sent to device...");
}

void ATCommandModeDEBUGSetup() {
  /*                TODO : UPDATE AND COMPLETE
   * HOW TO CONNECT
   *  1. Once in pairing mode you can pair
   *  2. AT+INIT -> Initialize ?
   *  3. AT+INQ -> Start being available to other devices
   * 
   */
 
  String setName = String("AT+NAME=MindSeyeT\r\n"); // Sets name to "MindSeyeT"
  String setPassword = String("AT+PSWD=1234\r\n"); // Sets password to "1234"
  
  pinMode(9, OUTPUT);  // this pin will pull the HC-05 pin 34 (key pin) HIGH to switch module to AT mode
  digitalWrite(9, HIGH);
  
  Serial.begin(9600);
  while(!Serial) ; // Needed for Arduino Micro/Leonardo since Software USB is being used instead of hardware USB-to-Serial adapter
  
  Serial.println("Enter AT commands:");
  Serial1.begin(38400);  // HC-05 default speed in AT command more
  
  Serial.println("Checking response with AT...");
  Serial1.print("AT\r\n");
  WaitForResponse();
  
  Serial.print("Setting name with "); Serial.println(setName);
  Serial1.print(setName); // Send command to change name
  WaitForResponse();

  Serial.print("Setting password with "); Serial.println(setPassword);
  Serial1.print(setPassword); // Send command to change password
  WaitForResponse();

  Serial.println("End of setup");
  Serial.println("Enter in AT Commands:");
}

void ATCommandModeDEBUGLoop() {
  // Keep reading from HC-05 and send to Arduino Serial Monitor
  if (Serial1.available()) {
    Serial.write(Serial1.read()); //Serial.write("RECEIVED "); 
  }

  // Keep reading from Arduino Serial Monitor and send to HC-05
  if (Serial.available()) {
    Serial1.write(Serial.read()); //Serial.write("SENT ");
  }
}







#define BT_SETUP 0
#define AT_COMMAND_MODE 0

void setup() {
  sampling_period_us = round(1000000 * (1.0 / SAMPLING_FREQUENCY));

  Serial1.begin(38400);
  while(!Serial1) ;

  // DELETE ME (comment me out) when wanting Bluetooth (HC-05) to work!
  //Serial.begin(9600);
  //while(!Serial);
  
#if AT_COMMAND_MODE
  Serial.begin(9600); // NOTE: Doing this causes Bluetooth module to not work correctly when not directly ran from IDE (i.e. hooked up to wall)
  while(!Serial) ; // Needed for Arduino Micro/Leonardo since Software USB is being used instead of hardware USB-to-Serial adapter
  Serial.write("In AT Command Mode...");
  ATCommandModeDEBUGSetup();
#endif
  
  InitializeBrainWaveData();
}

void loop() {

#if AT_COMMAND_MODE
    ATCommandModeDEBUGLoop();
#else
  if( 1 )// hatOn )
  {
    if (Serial1.available() > 0) // Check whether data is available in serial port to be read
    {
      // Read data from serial port -> Only if data is coming in (not necessary here)
      //Serial1.write(Serial.read());
    }
  
    // This will read in data from the amplifier circuit
    //sensorValue = analogRead(sensorPin);
  
    /*SAMPLING*/
    SampleAnalog();
  
    /*HANDLE DC OFFSET*/
    //AdjustForDCOffset(vReal, SAMPLES);
  
    /*FFT*/
    ComputeFFT();
  
    /*HANDLE WAVE DATA*/
    InitializeBrainWaveData();
    UpdateBrainWaveData();
  
    /*PRINT RESULTS TO SERIAL*/
    //PrintToSerial();
  
    /* BLUETOOTH STUFF */
    SendToDevice();

    delay(1000);  // Let the device breath for a second. Is this necessary? TRY WITHOUT
    //while (1);      //Run code once
  }
  else
  {
    // Hat is off!
  }
#endif
}



