#ifndef AT_H
#define AT_H

enum _IRQ {ON, OFF};

class  atterm
{
public:
    char buffer[128];
    char read_timed_buffer[128];
    char *getAnswer(uint32_t timeout);
    void device_init(unsigned long baud, _IRQ interrupt);
    void device_init1(unsigned long baud, _IRQ interrupt);
    void terminal_init(unsigned long baud, _IRQ interrupt);
    void clear();
    void at_send(char *format, char *buf);
    void at_send1(char *format, char *buf);
    void pc_send(char *format, char *buf);
    void debug_off(bool l);
    bool off;
    int a;
    char sign;    
} extern at0, at1, pc;

#endif