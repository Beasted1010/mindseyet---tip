################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_copy_f32.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_copy_q15.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_copy_q31.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_copy_q7.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_fill_f32.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_fill_q15.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_fill_q31.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_fill_q7.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_float_to_q15.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_float_to_q31.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_float_to_q7.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_q15_to_float.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_q15_to_q31.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_q15_to_q7.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_q31_to_float.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_q31_to_q15.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_q31_to_q7.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_q7_to_float.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_q7_to_q15.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/arm_q7_to_q31.c \
../mbed-dsp/cmsis_dsp/SupportFunctions/math_helper.c 

OBJS += \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_copy_f32.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_copy_q15.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_copy_q31.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_copy_q7.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_fill_f32.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_fill_q15.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_fill_q31.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_fill_q7.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_float_to_q15.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_float_to_q31.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_float_to_q7.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q15_to_float.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q15_to_q31.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q15_to_q7.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q31_to_float.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q31_to_q15.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q31_to_q7.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q7_to_float.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q7_to_q15.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q7_to_q31.o \
./mbed-dsp/cmsis_dsp/SupportFunctions/math_helper.o 

C_DEPS += \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_copy_f32.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_copy_q15.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_copy_q31.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_copy_q7.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_fill_f32.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_fill_q15.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_fill_q31.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_fill_q7.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_float_to_q15.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_float_to_q31.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_float_to_q7.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q15_to_float.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q15_to_q31.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q15_to_q7.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q31_to_float.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q31_to_q15.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q31_to_q7.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q7_to_float.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q7_to_q15.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/arm_q7_to_q31.d \
./mbed-dsp/cmsis_dsp/SupportFunctions/math_helper.d 


# Each subdirectory must supply rules for building sources it contributes
mbed-dsp/cmsis_dsp/SupportFunctions/%.o: ../mbed-dsp/cmsis_dsp/SupportFunctions/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -std=c99 '-DDEVICE_CRC=1' '-D__MBED__=1' '-DDEVICE_I2CSLAVE=1' '-D__FPU_PRESENT=1' '-DDEVICE_PORTOUT=1' '-DDEVICE_PORTINOUT=1' -DTARGET_RTOS_M4_M7 '-DDEVICE_RTC=1' '-DDEVICE_MPU=1' '-DDEVICE_SERIAL_ASYNCH=1' -D__CMSIS_RTOS -DTOOLCHAIN_GCC '-DDEVICE_CAN=1' -DTARGET_CORTEX_M '-DDEVICE_I2C_ASYNCH=1' -DTARGET_LIKE_CORTEX_M4 '-DDEVICE_ANALOGOUT=1' -DTARGET_M4 -DTARGET_STM32L4 '-DDEVICE_SPI_ASYNCH=1' '-DDEVICE_LPTICKER=1' '-DDEVICE_PWMOUT=1' -DMBED_TICKLESS -DTARGET_STM32L432xC '-DCOMPONENT_PSA_SRV_IMPL=1' -DTARGET_CORTEX '-DDEVICE_I2C=1' '-DTRANSACTION_QUEUE_SIZE_SPI=2' -D__CORTEX_M4 '-DDEVICE_STDIO_MESSAGES=1' -DTARGET_FAMILY_STM32 '-DDEVICE_PORTIN=1' -DTARGET_RELEASE '-DTARGET_NAME=NUCLEO_L432KC' -DTARGET_STM -DTARGET_STM32L432KC '-DDEVICE_SERIAL_FC=1' '-DCOMPONENT_PSA_SRV_EMUL=1' '-DDEVICE_USTICKER=1' '-DDEVICE_TRNG=1' -DTARGET_LIKE_MBED -D__MBED_CMSIS_RTOS_CM '-DDEVICE_SLEEP=1' -DTOOLCHAIN_GCC_ARM '-DDEVICE_SPI=1' '-DCOMPONENT_NSPE=1' '-DDEVICE_INTERRUPTIN=1' '-DDEVICE_SPISLAVE=1' '-DDEVICE_ANALOGIN=1' '-DDEVICE_SERIAL=1' '-DMBED_BUILD_TIMESTAMP=1554653342.58' '-DDEVICE_FLASH=1' -DTARGET_NUCLEO_L432KC -DARM_MATH_CM4 -DMBED_DEBUG '-DMBED_TRAP_ERRORS_ENABLED=1' -DMBED_DEBUG '-DMBED_TRAP_ERRORS_ENABLED=1' -DNDEBUG -DNDEBUG -I"C:/Users/die20/workspace/CMSIS_FFT_mbed_DAC" -I"C:/Users/die20/workspace/CMSIS_FFT_mbed_DAC/MAX7219" -I"C:/Users/die20/workspace/CMSIS_FFT_mbed_DAC/atterm" -I"C:/Users/die20/workspace/CMSIS_FFT_mbed_DAC/mbed-dsp" -I"C:/Users/die20/workspace/CMSIS_FFT_mbed_DAC/mbed-dsp/cmsis_dsp" -I"C:/Users/die20/workspace/CMSIS_FFT_mbed_DAC/mbed-dsp/dsp" -I"..//usr/src/mbed-sdk" -I"C:/Users/die20/workspace/CMSIS_FFT_mbed_DAC/mbed" -I"C:/Users/die20/workspace/CMSIS_FFT_mbed_DAC/mbed/TARGET_NUCLEO_L432KC/TOOLCHAIN_GCC_ARM" -I"C:/Users/die20/workspace/CMSIS_FFT_mbed_DAC/mbed/drivers" -I"C:/Users/die20/workspace/CMSIS_FFT_mbed_DAC/mbed/hal" -I"C:/Users/die20/workspace/CMSIS_FFT_mbed_DAC/mbed/platform"  -includeC:/Users/die20/workspace/CMSIS_FFT_mbed_DAC/mbed_config.h -O2 -funsigned-char -fno-delete-null-pointer-checks -fomit-frame-pointer -fmessage-length=0 -fno-builtin -g3 -Wall -Wextra -Wno-unused-parameter -Wno-missing-field-initializers -ffunction-sections -fdata-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


