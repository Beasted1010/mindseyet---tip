/*                TODO : UPDATE AND COMPLETE
 * HOW TO CONNECT
 *  1. Once in pairing mode you can pair
 *  2. AT+INIT -> Initialize ?
 *  3. AT+INQ -> Start being available to other devices
 * 
 */

 
// Allow module time to receive command and reply
void WaitForResponse()
{
  delay(500);
  while(Serial1.available()) {
    Serial.write(Serial1.read());
  }
}

void setup() 
{
  String setName = String("AT+NAME=MindSeyeT\r\n"); // Sets name to "MindSeyeT"
  String setPassword = String("AT+PSWD=1234\r\n"); // Sets password to "1234"
  
  pinMode(9, OUTPUT);  // this pin will pull the HC-05 pin 34 (key pin) HIGH to switch module to AT mode
  digitalWrite(9, HIGH);
  
  Serial.begin(9600);
  while(!Serial) ; // Needed for Arduino Micro/Leonardo since Software USB is being used instead of hardware USB-to-Serial adapter
  
  Serial.println("Enter AT commands:");
  Serial1.begin(38400);  // HC-05 default speed in AT command more
  
  Serial.println("Checking response with AT...");
  Serial1.print("AT\r\n");
  WaitForResponse();
  
  Serial.print("Setting name with "); Serial.println(setName);
  Serial1.print(setName); // Send command to change name
  WaitForResponse();

  Serial.print("Setting password with "); Serial.println(setPassword);
  Serial1.print(setPassword); // Send command to change password
  WaitForResponse();

  Serial.println("End of setup");
}
void loop()
{
  
  //Serial1 is the physical Serial Connections on TX and RX pins
  if (Serial1.available()) Serial.write(Serial1.read());

  // Serial is from my understanding the virtual connection with computer via USB 
  if (Serial.available()) Serial1.write(Serial.read());
}
 
/*
#include <SoftwareSerial.h>

//SoftwareSerial BTSerial(10, 11); // Arduino -> (Rx, Tx) - CONNECT BT RX PIN TO ARDUINO 11 PIN (as transmit)| CONNECT BT TX PIN TO ARDUINO 10 PIN
//SoftwareSerial BTSerial(6, 10); // Arduino -> (Rx, Tx) - CONNECT BT RX PIN TO ARDUINO 11 PIN | CONNECT BT TX PIN TO ARDUINO 10 PIN
//SoftwareSerial BTSerial(2, 3); // Arduino -> (Rx, Tx) - CONNECT BT RX PIN TO ARDUINO 11 PIN | CONNECT BT TX PIN TO ARDUINO 10 PIN

#define rxPin 6
#define txPin 10

SoftwareSerial BTSerial(rxPin, txPin); // RX, TX

// AT+NAME? -> Should return "MindSeyeT" currently
// AT+ADDR? -> Should return Mac Address: "+ADDR:14:3:67772" (can not change)


// TODO: Set password


// Allow module time to receive command and reply
void WaitForResponse()
{
  delay(500);
  while(BTSerial.available()) {
    Serial.write(BTSerial.read());
  }
}

void setup() 
{
  String setName = String("AT+NAME=MindSeyeT\r\n"); // Sets name to "MindSeyeT"
  String setPassword = String("AT+PSWD=1234\r\n"); // Sets password to "1234"
  
  pinMode(9, OUTPUT);  // this pin will pull the HC-05 pin 34 (key pin) HIGH to switch module to AT mode
  digitalWrite(9, HIGH);
  
  Serial.begin(9600);
  while(!Serial) ; // Needed for Arduino Micro/Leonardo since Software USB is being used instead of hardware USB-to-Serial adapter
  
  Serial.println("Enter AT commands:");
  BTSerial.begin(38400);  // HC-05 default speed in AT command more
  
  Serial.println("Checking response with AT...");
  BTSerial.print("AT\r\n");
  WaitForResponse();
  
  Serial.print("Setting name with "); Serial.println(setName);
  BTSerial.print(setName); // Send command to change name
  WaitForResponse();

  Serial.print("Setting password with "); Serial.println(setPassword);
  BTSerial.print(setPassword); // Send command to change password
  WaitForResponse();

  Serial.println("End of setup");
}

void loop()
{
  // Keep reading from HC-05 and send to Arduino Serial Monitor
  if (BTSerial.available()) {
    Serial.write(BTSerial.read()); //Serial.write("RECEIVED "); 
  }

  // Keep reading from Arduino Serial Monitor and send to HC-05
  if (Serial.available()) {
    BTSerial.write(Serial.read()); //Serial.write("SENT ");
  }
}
*/


/*
#include <SoftwareSerial.h>

#define rxPin 2
#define txPin 3

SoftwareSerial mySerial(rxPin, txPin); // RX, TX
char myChar ; 

void setup() {
  Serial1.begin(9600);   
  Serial1.println("Goodnight moon!");

  mySerial.begin(9600);
  mySerial.println("Hello, world?");
}

void loop(){
  while(mySerial.available()){
    myChar = mySerial.read();
    Serial1.print(myChar);
  }

  while(Serial1.available()){
   myChar = Serial1.read();
   mySerial.print(myChar);
  }
}
*/
