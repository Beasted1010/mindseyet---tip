
#include <SPI.h>
#include <SD.h>

#include <TMRpcm.h>

File myFile;
TMRpcm tmrpcm;

#define CHIP_SELECT_PIN 10


// REAL DATA FILES
#define EYES_CLOSED_RELAXING_1_FILE_NAME "EYESCL~1.WAV"
#define EYES_CLOSED_RELAXING_2_FILE_NAME "EYESCL~2.WAV"
#define DROWSY_STATE_1_FILE_NAME "EYESOP~1.WAV"
#define FNT_10HZ_AND_BELOW_FREQUENCIES_1_FILE_NAME "FNT10H~1.WAV"
#define FNT_DROWSY_EYES_CLOSED_1_FILE_NAME "FNTDRO~1.WAV"
#define FNT_LIE_DOWN_EYES_CLOSED_1_FILE_NAME "FNTELI~1.WAV"
#define FNT_EYE_MOVEMENT_1_FILE_NAME "FNTEYE~1.WAV"
#define FNT_LIE_DOWN_EYES_CLOSED_1_FILE_NAME "FNTLIE~1.WAV"
#define FNT_DROWSY_RANDOM_1_FILE_NAME "FRNTDR~1.WAV"
#define CASUAL_THINKING_EYES_OPEN_1_FILE_NAME "CASUAL~1.WAV"
#define CASUAL_THINKING_EYES_OPEN_2_FILE_NAME "CASUAL~2.WAV"
#define COMPUTER_ACTIVITY_1_FILE_NAME "COMPUT~1.WAV"
#define COMPUTER_ACTIVITY_2_FILE_NAME "COMPUT~2.WAV"
#define DEEP_BREATHING_EYES_CLOSED_1_FILE_NAME "DEEPBR~1.WAV"
// NOTE: Not sure which files these exactly line up to (listing files truncates the file name, the actual file name on SD card is something longer and with more details)
#define DROWSY_1_FILE_NAME "DROWSY~1.WAV"
#define DROWSY_2_FILE_NAME "DROWSY~2.WAV"
#define DROWSY_3_FILE_NAME "DROWSY~3.WAV"


// TEST DATA FILES
#define WAVE_10HZ_1 "WAVE_1~1.WAV"
#define WAVE_10HZ_2 "WAVE_1~2.WAV"

#define WAVE_1000HZ_1 "1000HZ_1.WAV"
#define WAVE_1000HZ_2 "1000HZ_2.WAV"
#define _TEST1 "_TEST1~1.WAV" // 1000 Hz I think?

#define TEST1 "TEST1.WAV" // 10 Hz 192000 samples in file
#define TEST2 "TEST2.WAV" // 10 Hz 19200 samples in file 


#define PLAY_FILE FNT_10HZ_AND_BELOW_FREQUENCIES_1_FILE_NAME


#define BUTTON_1_PIN 7
#define BUTTON_2_PIN 6
#define BUTTON_3_PIN 5
#define BUTTON_4_PIN 4
#define BUTTON_5_PIN 3

#define OUTPUT_PIN 9



#define LIST_FILES 0
#define TEST_MODE 1

#if LIST_FILES
  File root;
#endif

// List files in a directory
void printDirectory(File dir, int numTabs);


void setup() {
  tmrpcm.speakerPin = OUTPUT_PIN;

  pinMode(BUTTON_1_PIN, INPUT);
  pinMode(BUTTON_2_PIN, INPUT);
  pinMode(BUTTON_3_PIN, INPUT);
  pinMode(BUTTON_4_PIN, INPUT);
  pinMode(BUTTON_5_PIN, INPUT);
  
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Serial.print("Initializing SD card... ");

  if (!SD.begin(CHIP_SELECT_PIN)) {
    Serial.println("Initialization failed!");
    return;
  }
  Serial.println("Initialization done.");

#if LIST_FILES
  Serial.println("Listing files...");

  root = SD.open("/");
  printDirectory(root, 0);

  Serial.println("Done listing files!");
#endif

  /*myFile = SD.open(PLAY_FILE);
  if (myFile) {
    Serial.print(PLAY_FILE);
    Serial.println(": ");

    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      Serial.write(myFile.read());
    }
    
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.print("error opening ");
    Serial.println(PLAY_FILE);
  }*/
}

char* fileName = "";
uint8_t pinRead = 0;

void loop() {

  //while( tmrpcm.isPlaying() ) ; // Busy wait while audio file playing. This prevents the built in asynchronous functionality

  if( pinRead = digitalRead(BUTTON_1_PIN) ) {
#if TEST_MODE
  fileName = WAVE_10HZ_1;
#else
  fileName = EYES_CLOSED_RELAXING_1_FILE_NAME;
#endif
  }
  else if( pinRead = digitalRead(BUTTON_2_PIN) ) {
#if TEST_MODE
  fileName = WAVE_10HZ_2;
#else
  fileName = FNT_10HZ_AND_BELOW_FREQUENCIES_1_FILE_NAME;
#endif
  }
  else if( pinRead = digitalRead(BUTTON_3_PIN) ) {
#if TEST_MODE
  fileName = TEST1;
#else
  fileName = DROWSY_STATE_1_FILE_NAME;
#endif
  }
  else if( pinRead = digitalRead(BUTTON_4_PIN) ) {
#if TEST_MODE
  fileName = TEST2;
#else
  fileName = FNT_LIE_DOWN_EYES_CLOSED_1_FILE_NAME;
#endif
  }
  else if( pinRead = digitalRead(BUTTON_5_PIN) ) {
#if TEST_MODE
  fileName = _TEST1;
#else
  fileName = FNT_EYE_MOVEMENT_1_FILE_NAME;
#endif
  }

  if( pinRead && fileName && fileName != "" ) {
    
    Serial.print("Playing: "); Serial.println(fileName);
    tmrpcm.play(fileName);

    delay(1000); // Wait a second as a means to debounce, helps prevent bouncing
    
  }

  /*if(Serial.available()) {

    if(Serial.read() == '1') {
      Serial.print("Playing: "); Serial.println(PLAY_FILE);
      
      tmrpcm.play(PLAY_FILE);
    }
    
  }*/
  
}



void printDirectory(File dir, int numTabs) {
  while (true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println("/");
      printDirectory(entry, numTabs + 1);
    } else {
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}

