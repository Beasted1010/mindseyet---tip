



void SPI_MasterInit()
{
  /* NOTE: Many of these registers default to a value of 0, but I am making explicit (instead of leaving out) for learning purposes */

  DDRB = (1 << DDB3); // Set Port B Pin 3 (MOSI) to be output pin
  DDRB = (0 << DDB4); // Set Port B Pin 4 (MISO) to be input pin -> This gets overwritten by simply enabling SPI when it is a master
  DDRB = (1 << DDB5); // Set Port B Pin 5 (SCK) to be output pin

  SPCR = (1 << SPIE); // Enable SPI Interrupts
  SPCR = (1 << SPE);  // Enable SPI Operations
  SPCR = (1 << MSTR); // Set device to master

  // Do I need this? May only be necessary if I plan on sending out data?
  //SPCR = (1 << DORD); // Do I want 1?
  SPCR = (0 << CPOL); // Do I want 0? (idle low)
  SPCR = (0 << CPHA); // Do I want 0? (Leading edge = sample, trailing edge = setup)

  //SPCR = (0 << SPR1); // Do I want 0?
  //SPCR = (0 << SPR0); // Do I want 0?

}

void setup() {
  Serial.begin(9600);

  while( !Serial ); // Wait until serial connects (Needed for native USB port only)
  
  PRR = (0 << PRSPI); // Enable the SPI Module (i.e. don't "power reduce" (PR) by disabling SPI module)

  SPI_MasterInit();

  Serial.println(PRR && (1 << PRSPI));
  Serial.println(SREG && (1 << 7));
  Serial.println(SPCR && (1 << SPE));
  Serial.println(SPCR && (1 << SPIE));
  SPCR = (1 << SPIE); // Enable SPI Interrupts
  SPCR = (1 << SPE);  // Enable SPI Operations

}

ISR(SPI_STC_vect)
{
  Serial.println("IN ISR");
  byte c = SPDR;

  sei();
  reti();
}

char SPI_MasterReceive()
{
  // NOTE: Per docs, When configured as master: SPI interface has no auto control of ~SS line. This must be handled by SW before communication can start
  //       Master signals end of packet by pulling high the Slave Select (~SS) line -> Maybe I need to pull this high since I am not sending from master to slave?
  PORTB = (0 << PORTB7); // Pull low SS pin (active low) -> Needed? -> Pulling low initiates communication, after each data packet, Master will synchronize with slave by pulling high SS line (deactivating it) -> Active low or high?

  // NOTE: SPIF is set once SPI clock generator stops (after shifting one byte from master into slave) -> This isn't necessarily receiving from slave!
  // Wait for transmission to complete
  while ( !(SPSR && (1 << SPIF)) ); // While not end of single byte shift (SPIF is set after shifting one byte, the SPI clock generator also stops)

  return SPDR; // Return content of the SPI Data Register
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("Before");
  Serial.println(SPI_MasterReceive());
  Serial.println("After");
}












