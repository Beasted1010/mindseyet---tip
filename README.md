# MindSeyeT - <name>

Senior Design Project - NAME


UserInterface folder: The Android Studio Project corresponding to the code for the Mobile Application, you should be able to go to File->Open in Android Studio and open this folder to get access to the project.

MicroController: The microcontroller code which will be what runs on the microcontroller. This will primarily include FFT and data transmission.


BlueToothTesting:
NOTE: the "Client" code of the app is set to only accept connections from "DEVICE_NAME". Make sure you set it and recompile before trying to connect to something from the app.

After pulling the BlueToothTesting Branch you will have a lot of buttons on the screen! Here's a quick rundown

**Server** - Tap here to get the device to act as a Bluetooth server. Use this if you need to test two instances of the app and you need one to pretend to be the "server".

**Send** - Once the server is connected, tap this to generate a random int and send it to the "client", along with a simply awake or drowsy message.

**Stop** - Close the connection from the "client" side. Doesn't work too well at the moment

**Clear** - Use this to clear the messages that have built up in the text view.

**Client** - Use this to establish a connection to a "server" device. Be sure you set the DEVICE_NAME in the code. This could be another instance of the app where you have tapped "Server" or it could be an arduino.

**Listen** - Tap this after establishing a connection with the **client** button to start printing anything from the input buffer to the screen.