package com.example.die20.mindseyet;

import android.graphics.Color;
import android.os.Handler;
import android.view.View;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

public class DrowsinessGraph {

    private final Handler mHandler = new Handler();
    private int RefreshRate;
    private Runnable mTimer;
    private double graphLastXValue = 0;
    private LineGraphSeries<DataPoint> mSeries;
    private SigInterpreter siginterp;

    public void onViewCreate(View view) {
        GraphView graph = (GraphView) view.findViewById(R.id.drowsiness_graph);
        initGraph(graph);
        RefreshRate = view.getResources().getInteger(R.integer.refresh_rate_ms);
    }

    public void initGraph(GraphView graph) {
        siginterp = SigInterpreter.getInstance();

        graph.setTitle("Drowsiness Percentages");
        graph.setTitleTextSize(48.0f);

        GridLabelRenderer glr = graph.getGridLabelRenderer();
        glr.setVerticalAxisTitle("Drowsiness Level (%)");
        glr.setVerticalAxisTitleTextSize(32.0f);
        glr.setHorizontalAxisTitle("Time Period (Seconds)");
        glr.setHorizontalAxisTitleTextSize(32.0f);
        glr.setGridColor(Color.LTGRAY);

        Viewport vp = graph.getViewport();
        vp.setXAxisBoundsManual(true);
        vp.setYAxisBoundsManual(true);
        vp.setMinX(0);
        vp.setMaxX(20);
        vp.setMaxY(100);

        graph.getGridLabelRenderer().setLabelVerticalWidth(100);

        // first mSeries is a line
        mSeries = new LineGraphSeries<>();
        mSeries.setDrawDataPoints(true);
        mSeries.setDrawBackground(true);
        mSeries.setAnimated(false);
        mSeries.setColor(Color.parseColor("#d0e1f9"));
        graph.addSeries(mSeries);
    }

    public void onResume() {
        mTimer = new Runnable() {
            @Override
            public void run() {
                float drowsiness_perc = siginterp.getDrowsinessLevel() * 100;
                graphLastXValue += RefreshRate / 1000.0;
                mSeries.appendData(new DataPoint(graphLastXValue, drowsiness_perc), true, 100);
                mHandler.postDelayed(this, RefreshRate);
            }
        };
        mHandler.postDelayed(mTimer, 1500);
    }

    public void onPause() {
        mHandler.removeCallbacks(mTimer);
    }
}

