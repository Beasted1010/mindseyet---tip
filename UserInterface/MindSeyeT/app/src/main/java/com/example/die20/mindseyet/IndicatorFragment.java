package com.example.die20.mindseyet;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.github.anastr.speedviewlib.SpeedView;

public class IndicatorFragment extends Fragment {
    private SpeedView drowsinessGauge;
    private Button alert;
    private View currentFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // and save it for local methods
        currentFragment =  inflater.inflate(R.layout.indicator, container, false);
        return currentFragment;
    }

    public void updateGauge(int percentage) {
        this.drowsinessGauge = currentFragment.findViewById(R.id.drowsiness_gauge);
        this.drowsinessGauge.speedTo(percentage, 500);
    }

    public void showAlert(boolean val) {
        this.alert = currentFragment.findViewById(R.id.alert_signal);
        int visibility = val ? View.VISIBLE : View.GONE;
        this.alert.setVisibility(visibility);
    }
}
