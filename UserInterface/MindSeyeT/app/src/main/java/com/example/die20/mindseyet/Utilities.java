package com.example.die20.mindseyet;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.jjoe64.graphview.series.DataPoint;


public class Utilities {
    private final static String TAG = "Utilities";
    private final static boolean LOG = false;

    final static int NUM_WAVES = 6;
    final static String HEAD = "C";
    final static String TAIL = ";";
    final static String SIGNAL_DELIM = ",";
    final static String TRANSMISSION_DELIM = ";";

    // Data transmission starts with SYNC, and each component separated by comma, ends with semicolon
    // Protocol: 'SYNC,EPSILON,DELTA,THETA,ALPHA,BETA,GAMMA;'
    static int[] ParseRawSignal(String raw_data)
    {
        int[] wave_data = new int[NUM_WAVES];  // Default value by language spec is all 0s

        // Assert raw data starts with correct value
        if (raw_data.indexOf(HEAD) != 0) {
            if (LOG) Log.d(TAG, String.format("Raw signal '%s' doesn't start with '%s'", raw_data, HEAD));
            return null;
        } else {
            raw_data = raw_data.replaceAll(String.format("%s%s", HEAD, SIGNAL_DELIM), "");
        }

        // Assert raw data ends with correct value
        if (raw_data.indexOf(TAIL) != raw_data.length() - 1) {
            if (LOG) Log.d(TAG, String.format("Raw signal '%s' doesn't end with '%s'", raw_data, TAIL));
            return null;
        } else {
            raw_data = raw_data.replace(TAIL, "");
        }

        // Assert number of values matches the number of waves
        List<String> str_wave_values = Arrays.asList(raw_data.split(SIGNAL_DELIM));
        if (str_wave_values.size() != NUM_WAVES) {
            if (LOG) Log.d(TAG, String.format("Raw signal '%s' doesn't contain enough " +
                    "wave values: %s", raw_data, NUM_WAVES));
            return null;
        }

        // Convert string values to integers
        int i = 0;
        for (String s: str_wave_values) {
            wave_data[i] = Integer.valueOf(s);
            i++;
        }
        return wave_data;
    }

    static List<int[]> ParseRawTransmission(String raw_data) {
        List<int[]> signals = new ArrayList<>();
        String pattern = String.format("(?<=%s)", TRANSMISSION_DELIM);
        List<String> raw_signals = Arrays.asList(raw_data.split(pattern));
        for (String raw_signal : raw_signals) {
            int[] wave_data = ParseRawSignal(raw_signal);
            if (wave_data != null) signals.add(wave_data);
        }
        return signals;
    }


    static DataPoint[] arrayToDataPoints(int[] brainwaves) {
        DataPoint[] result = new DataPoint[brainwaves.length];
        for (int i = 0; i < brainwaves.length; i++) {
            result[i] = new DataPoint(i + 1, brainwaves[i]);
        }
        return result;
    }

    static String brainwavesToString(List<int[]> brainwaves) {
        String result = "";
        for (int[] waves: brainwaves) {
            String waves_str = "";
            for (int i = 0; i < waves.length; i++) {
                waves_str = waves_str.concat(Integer.toString(waves[i]) + ",");
            }
            result = result.concat("[" + waves_str + "]\n");
        }
        return result;
    }

    static List<int[]> strToBrainwaves(String str) {
        List<int[]> result = new LinkedList<>();
        List<String> str_waves = Arrays.asList(str.split("\n"));
        for (String s: str_waves) {
             List<String> str_wave_values = Arrays.asList(s.split(","));
             int[] waves = new int[str_wave_values.size()];
             for (int i = 0; i < waves.length; i++) {
                 waves[i] = Integer.parseInt(str_wave_values.get(i));
             }
             result.add(waves);
        }
        return result;
    }

    static List<Integer> intArrToList(int[] arr) {
        List<Integer> newArray = new LinkedList<>();
        for (int value : arr) {
            newArray.add(value);
        }
        return newArray;
    }

    // Returns the index of the largest value in the array.
    static int maxWaveIndex(int[] array) {
        int max_idx = 0;
        for (int i = 0; i < array.length; i++) {
            max_idx = array[i] > array[max_idx] ? i : max_idx;
        }
        return max_idx;
    }

    // Generates a list of integers starting from 0 to (not including) last_val.
    static List<Integer> generateRange(int last_val) {
        List<Integer> range = new ArrayList<>();
        for (int i = 0; i < last_val; i++) {
            range.add(i);
        }
        return range;
    }

    // Swap two values in array.
    static int[] swap(int a_idx, int b_idx, int[] array) {
        int temp = array[a_idx];
        array[a_idx] = array[b_idx];
        array[b_idx] = temp;
        return array;
    }
}
