package com.example.die20.mindseyet;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

public class NotificationService extends IntentService {

    //Update this variable to change the message sent
    public String message = "Alert! You're drowsy!";

    // This harnesses the device's default notification sound for the event
    Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

    // A constructor is required, and must call the super IntentService(String)</a></code>
    // constructor with a name for the worker thread.
    public NotificationService() {

        super("NotificationService");
    }

    // The IntentService calls this method from the default worker thread with
    // the intent that started the service. When this method returns, IntentService
    // stops the service, as appropriate.
    @Override
    protected void onHandleIntent(Intent intent) {

        //Creating and sending a notification in the primary thread
        createAndSendNotification();
    }

    @Override
    public void onCreate(){

        // Start with creating the notification channel on newer devices
        createNotificationChannel();

        Log.i("notification", "Notification Service created");
        super.onCreate();
    }

    // Code for sending a basic notifcation goes here
    private void createNotificationChannel()
    {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(getString(R.string.channel_id), name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void createAndSendNotification ()
    {
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, getString(R.string.channel_id))
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle("MindSeyeT")
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSound(notificationSound)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(001, builder.build());
    }
}

