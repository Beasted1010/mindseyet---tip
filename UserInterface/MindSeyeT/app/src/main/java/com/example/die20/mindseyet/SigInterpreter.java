package com.example.die20.mindseyet;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class SigInterpreter implements Serializable {

    private static SigInterpreter siginterp;

    final int NUM_WAVES = 6;
    final String TAG = "SigInterp";
    private final static boolean LOG = true;


    // Bounds that identify limits within the 0% - 100% drowsiness range.
    // Lower bounds
    final float GAMMA_BOUNDS = 0.0f;  // 0% - 0% (Not taking into account)
    final float BETA_BOUNDS = 0.0f;  // 0% - 35%
    final float ALPHA_BOUNDS = 0.35f;  // 35% - 70%
    final float THETA_BOUNDS = 0.7f;  // 70% - 85%
    final float DELTA_BOUNDS = 0.85f;  // 85% - 100%
    final float EPSILON_BOUNDS = 1.0f;  // 100% - 100% (Not taking into account)
    float[] PERCENTAGE_RANGES = {EPSILON_BOUNDS, DELTA_BOUNDS, THETA_BOUNDS,
            ALPHA_BOUNDS, BETA_BOUNDS, GAMMA_BOUNDS};

    private List<int[]> brainwaves;
    private Boolean new_waves = true;
    private float latest_drowsiness_level = 0.0f;
    private int queue_len = 25;
    public int sample_len = 10;

    public SigInterpreter() {
        this.brainwaves = new LinkedList<>();
    }

    public static SigInterpreter getInstance() {
        if (siginterp == null) siginterp = new SigInterpreter();
        return siginterp;
    }

    // Expecting brainwave values in the following order:
    // EPSILON, DELTA, THETA, ALPHA, BETA, GAMMA
    public void updateWaves(int[] brainwaves) {
        if (brainwaves == null) {
            if (LOG) Log.d(TAG, "Invalid brainwaves: null");
        } else if (brainwaves.length != NUM_WAVES) {
            if (LOG) Log.d(TAG, String.format("Invalid brainwaves: %s", Arrays.toString(brainwaves)));
        }

        if (this.brainwaves.size() >= queue_len) {
            this.brainwaves.remove(queue_len - 1);
        }
        if (LOG) Log.d(TAG, String.format("Brainwaves added: %s", Arrays.toString(brainwaves)));
        this.brainwaves.add(0, brainwaves);
        this.new_waves = true;
    }

    public void updateWaves(List<int[]> brainwaves) {
        // Expects oldest at the head (index 0)
        for (int[] waves: brainwaves) {
            updateWaves(waves);
        }
    }

    public float getDrowsinessLevel() {
        /* Currently an arbitrary test.
        Returns the index of max value from the latest brainwave entered.

        Ex:
            if latest brainwaves are: {1, 2, 3, 4, 5, 6, 7, 8, 9}
            then the output will be 0.8f since the 8th elem is the greatest.
         */

        // First check if any new waves have been added
        if (!this.new_waves) return this.latest_drowsiness_level;

        // Step 1. Collect dominant wave of each entry in the sample.
        // Step 2. Calculate the ratio of the next frequency range for each entry.
        List<List<Float>> dominant_ratios = this._getDominantRatios();

        // Step 3. Average out the ratios of each frequency range within the sample.
        float[] ave_ratios = this._calcAveRatios(dominant_ratios);

        // Step 4. Calculate the drowsy percentage of each average frequency range ratio.
        float[] drowsy_percentages = this._calcDrowsyPercentages(ave_ratios);

        // Step 5. Calculate the weighted average of the drowsy percentages to arrive at the
        //         final drowsy percentage.
        this.new_waves = false;
        this.latest_drowsiness_level = this._calcFinalDrowsyPercentage(drowsy_percentages, dominant_ratios);
        return this.latest_drowsiness_level;
    }

    public int[] getLatestWaves() {
        if (brainwaves.size() == 0) { return new int[NUM_WAVES]; }
        return brainwaves.get(0);
    }

    // Collects the ratio of the of the dominant waves for each entry.
    // See Journal for more information.
    public List<List<Float>> _getDominantRatios() {
        int sublist_len = this.sample_len > this.brainwaves.size() ? this.brainwaves.size() : this.sample_len;
        List<int[]> sample_waves = this.brainwaves.subList(0, sublist_len);
        List<List<Float>> dominant_ratios = new ArrayList<>();

        int i = 0;
        while (i < NUM_WAVES) {
            dominant_ratios.add(new ArrayList<Float>());
            i++;
        }

        for (int[] entry : sample_waves) {
            int dominant_wave_idx = Utilities.maxWaveIndex(entry);
            Float ratio = 0.0f;
            if (dominant_wave_idx > 0) {
                ratio = (float) entry[dominant_wave_idx-1] / (float) entry[dominant_wave_idx];
            }
            List<Float> curr_dom_ratios = dominant_ratios.get(dominant_wave_idx);
            curr_dom_ratios.add(ratio);
            dominant_ratios.set(dominant_wave_idx, curr_dom_ratios);
        }
        return dominant_ratios;
    }

    // Calculate the average ratios for each dominant brainwave.
    public float[] _calcAveRatios(List<List<Float>> dominant_ratios) {
        float[] ave_ratios = new float[NUM_WAVES];
        for (int i = 0; i < NUM_WAVES; i++) {
            List<Float> ratios = dominant_ratios.get(i);
            float sum = 0.0f;
            for (float value : ratios) {
                sum += value;
            }
            ave_ratios[i] = ratios.size() > 0 ? sum / ratios.size() : sum;
        }
        return ave_ratios;
    }

    // Calculate the drowsy percentages based on the average ratios for each brainwave.
    public float[] _calcDrowsyPercentages(float[] ave_ratios) {
        float[] drowsy_percentages = new float[NUM_WAVES];

        // Calcs Delta through Gamma
        // TODO: Make more legible
        for (int i = 1; i < NUM_WAVES - 1; i++) {
            float lower_bound = PERCENTAGE_RANGES[i];
            float wave_interval = PERCENTAGE_RANGES[i-1] - PERCENTAGE_RANGES[i];
            float local_percent = ave_ratios[i] * wave_interval;
            drowsy_percentages[i] = local_percent > 0 ? local_percent + lower_bound : 0.0f;
        }

        // Assign Epsilon to 1.0f if ave_ratios greater than one.
        // Shouldn't be used as Epsilon waves shouldn't be encountered.
        drowsy_percentages[0] = ave_ratios[0] > 0 ? 1.0f : 0.0f;
        return drowsy_percentages;
    }

    // Calculate the final drowsy percentage from the brainwave drowsy percentages.
    public float _calcFinalDrowsyPercentage(float[] drowsy_percentages, List<List<Float>> dominant_ratios) {
        float drowsy_percentage = 0.0f;
        for (int i = 0; i < drowsy_percentages.length; i++) {
            float wave_weight = (float) dominant_ratios.get(i).size() / sample_len;
            drowsy_percentage += drowsy_percentages[i] * wave_weight;
        }
        return drowsy_percentage;
    }

    // Sets the bounds for the drowsiness range.
    // Expecting brainwave values in the following order:
    // EPSILON, DELTA, THETA, ALPHA, BETA, GAMMA
    public void setDrowsinessRangeBounds(float[] bounds) {
        if (bounds.length >= this.PERCENTAGE_RANGES.length) {
            this.PERCENTAGE_RANGES = bounds;
        }
    }

    public static void main (String[] args ) {

        SigInterpreter sig_int = new SigInterpreter();
        sig_int.sample_len = 10;

        // Generator values
        int sample_len = 10;
        int sleep_stage = 3;
        int value_range = 100;
        float ss_ratio = 0.8f;
        boolean limit_to_ss = true;
        boolean shuffle = false;

        List<int[]> brainwaves = TestFunctional.generateWaves(
                sample_len, sleep_stage, value_range, ss_ratio, limit_to_ss, shuffle);
        sig_int.updateWaves(brainwaves);

        String output = String.format("Sample Length: %s\n" +
                        "Sleep Stage: %s\n" +
                        "Value Range: 0 - %s\n" +
                        "Sleep Stage Ratio: %s\n" +
                        "Limit to Sleep Stage? %s\n" +
                        "Shuffle Brainwave Entries? %s\n\n" +
                        "Generated Brainwaves: \n%s\n" +
                        "Drowsiness Percentage: %s",
                sample_len, sleep_stage, value_range, ss_ratio, limit_to_ss, shuffle,
                Utilities.brainwavesToString(brainwaves), sig_int.getDrowsinessLevel());
        System.out.print(output);
    }
}
