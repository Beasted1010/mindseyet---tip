//TO-DO move all of this into a different activity called debug or something
//Then have this main activity replaced with a big graphic or something that shows "Drowsy or not"
package com.example.die20.mindseyet;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.util.Log;

public class MainActivity extends AppCompatActivity implements BluetoothControlsFragment.FragmentCommunicator {

    // Create the layout that allows for a navigation drawer
    private DrawerLayout mDrawerLayout;
    private final static String TAG = "MainActivity";

    // Values for the drowsiness meter
    private String drowsiness_output = "0%%";
    private float alarm_threshold = 70.0f;
    private int curr_drowsiness_percentage = 0;
    private final Handler mHandler = new Handler();
    private int RefreshRate;
    private Runnable mTimer;

    //The SigInterpreter object is needed for dealing with the brainwaves data
    SigInterpreter sigInterp = new SigInterpreter();

    ImageView btConnectionIndic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        RefreshRate = getApplicationContext().getResources().getInteger(R.integer.refresh_rate_ms);

        //Load the indicator fragment to begin
        Fragment initFragment = new IndicatorFragment();
        FragmentTransaction initTransaction = getSupportFragmentManager().beginTransaction();
        initTransaction.add(R.id.content_frame, initFragment, "IndicatorFragment");
        initTransaction.commit();

        //Get the sig interpreter instance
        sigInterp = SigInterpreter.getInstance();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
            new NavigationView.OnNavigationItemSelectedListener() {

                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem) {

                    // set item as selected to persist highlight
                    menuItem.setChecked(true);
                    // close drawer when item is tapped
                    mDrawerLayout.closeDrawers();

                    // Swap the UI fragments, so the user sees the controls they need
                    switch (menuItem.getItemId()) {
                        case R.id.nav_bluetooth :
                            Fragment btFragment = new BluetoothControlsFragment();
                            FragmentTransaction btTransaction = getSupportFragmentManager().beginTransaction();
                            // Replace whatever is in the fragment_container view with this fragment,
                            // and add the transaction to the back stack
                            btTransaction.replace(R.id.content_frame, btFragment, "BTControlsFragment");
                            btTransaction.addToBackStack(null);
                            // Commit the transaction
                            btTransaction.commit();
                            break;

                        case R.id.nav_home :
                            Fragment indicFragment = new IndicatorFragment();
                            FragmentTransaction indicTransaction = getSupportFragmentManager().beginTransaction();
                            // Replace whatever is in the fragment_container view with this fragment,
                            // and add the transaction to the back stack
                            indicTransaction.replace(R.id.content_frame, indicFragment, "IndicatorFragment");
                            indicTransaction.addToBackStack(null);
                            // Commit the transaction
                            indicTransaction.commit();
                            // Execute it immediately so we can start calling update meter.
                            getSupportFragmentManager().executePendingTransactions();
                            updateMeter();
                            break;

                        case R.id.nav_brainwave :
                            Fragment bwFragment = new BrainwaveDataGraphFragment();
                            FragmentTransaction bwTransaction = getSupportFragmentManager().beginTransaction();
                            // Replace whatever is in the fragment_container view with this fragment,
                            // and add the transaction to the back stack
                            bwTransaction.replace(R.id.content_frame, bwFragment, "BrainwaveFragment");
                            bwTransaction.addToBackStack(null);
                            // Commit the transaction
                            bwTransaction.commit();
                            break;
                    }
                return true;
                }
            }
        );

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        btConnectionIndic = (ImageView) findViewById(R.id.connection_icon);

        getSupportFragmentManager().executePendingTransactions();
        updateMeter();
    }

    // These functions hook up to the buttons in the Bluetooth fragment
    public void BTListen(){

        // Launch the BlueTooth Background Service here
        Log.i("bluetooth", "Attempting to start service");
        Intent btServiceIntent = new Intent(this, BluetoothClientService.class);
        try {
            startService(btServiceIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateMeter() {

        // This creates the intent for the notification service and needs to be called once,
        // before the repeating thread starts
        final Intent notificationServiceIntent = new Intent(this, NotificationService.class);

        // Get the indicator fragment so we can update it.
        final IndicatorFragment mainFragment = (IndicatorFragment) getSupportFragmentManager().findFragmentByTag("IndicatorFragment");

        mTimer = new Runnable() {
            @Override
            public void run() {
                try {
                    int curr_drowsiness_percentage = (int) (sigInterp.getDrowsinessLevel() * 100);
                    mainFragment.updateGauge(curr_drowsiness_percentage);
                    drowsiness_output = String.format("%s%%\n", curr_drowsiness_percentage);
                    if (curr_drowsiness_percentage >= alarm_threshold) {
                        drowsiness_output += "Drowsy Alarm";
                        // TODO - Check to see that no notficiation has been sent in the last 5 seconds or so
                        // OR exploit this bug to be super annoying!!
                        Log.i("notification", "Calling notification service.");
                        startService(notificationServiceIntent);
                        mainFragment.showAlert(true);
                    } else {
                        mainFragment.showAlert(false);
                    }
                    mHandler.postDelayed(this, 250);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        mHandler.postDelayed(mTimer, 1500);
    }
}
