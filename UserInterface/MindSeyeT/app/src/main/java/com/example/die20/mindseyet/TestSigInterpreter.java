package com.example.die20.mindseyet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class TestSigInterpreter {

    public static void testSetDrowsinessRangeBounds() {
        SigInterpreter s = SigInterpreter.getInstance();
        float[] expected = {0.0f, 0.2f, 0.4f, 0.6f, 0.8f, 1.0f};
        s.setDrowsinessRangeBounds(expected);
        float[] result = s.PERCENTAGE_RANGES;
        System.out.printf("Drowsiness Range Bounds: %s\n", Arrays.toString(result));
        if (!Arrays.equals(expected, result)) throw new AssertionError(
                String.format("%s != %s", Arrays.toString(expected), Arrays.toString(result)));
    }

    public static void testGetDominantRatios() {
        SigInterpreter s = SigInterpreter.getInstance();
        s.setDrowsinessRangeBounds(getBounds());
        s.updateWaves(getMultWaves());
        List<List<Float>> expected = getDominantRatios();
        List<List<Float>> result = s._getDominantRatios();
        System.out.printf("Dominant Ratios: %s\n", result.toArray());
        if (!Arrays.deepEquals(expected.toArray(), result.toArray())) throw new AssertionError(
                String.format("%s != %s", expected, result));
    }

    public static void testCalcAveRatios() {
        SigInterpreter s = SigInterpreter.getInstance();
        s.setDrowsinessRangeBounds(getBounds());
        s.updateWaves(getMultWaves());
        List<List<Float>> dominant_ratios = s._getDominantRatios();
        float[] expected = getAveRatios();
        float[] result = s._calcAveRatios(dominant_ratios);
        System.out.printf("Average Ratios: %s\n", Arrays.toString(result));
        if (!Arrays.equals(expected, result)) throw new AssertionError(
                String.format("%s != %s", Arrays.toString(expected), Arrays.toString(result)));
    }

    public static void testCalcDrowsyPercentages() {
        SigInterpreter s = SigInterpreter.getInstance();
        s.setDrowsinessRangeBounds(getBounds());
        s.updateWaves(getMultWaves());
        List<List<Float>> dominant_ratios = s._getDominantRatios();
        float[] ave_ratios = s._calcAveRatios(dominant_ratios);
        float[] expected = getDrowsyPercentages();
        float[] result = s._calcDrowsyPercentages(ave_ratios);
        System.out.printf("Drowsy Percentages: %s\n", Arrays.toString(result));
        if (!Arrays.equals(expected, result)) throw new AssertionError(
                String.format("%s != %s", Arrays.toString(expected), Arrays.toString(result)));
    }

    public static void testCalcFinalDrowsyPercent() {
        SigInterpreter s = SigInterpreter.getInstance();
        s.setDrowsinessRangeBounds(getBounds());
        s.updateWaves(getMultWaves());
        List<List<Float>> dominant_ratios = s._getDominantRatios();
        float[] ave_ratios = s._calcAveRatios(dominant_ratios);
        float[] drowsy_percentages = s._calcDrowsyPercentages(ave_ratios);
        float expected = getFinalDrowsyPercent();
        float result = s._calcFinalDrowsyPercentage(drowsy_percentages, dominant_ratios);
        System.out.printf("Drowsy Percentages: %s\n", result);
        if (expected != result) throw new AssertionError(
                String.format("%s != %s", expected, result));
    }

    // Test Data ===================================================================================

    public static float[] getBounds() {
        float[] bounds = {1.0f, 0.85f, 0.7f, 0.35f, 0.0f, 0.0f};
        return bounds;
    }

    public static List<int[]> getMultWaves() {
        List<int[]> multiple_waves = Arrays.asList(
                new int[]{77, 32, 51, 44, 68, 29},
                new int[]{42, 16, 84, 33, 68, 91},
                new int[]{53, 93, 64, 27, 76, 43},
                new int[]{24, 102, 5, 16, 53, 39},
                new int[]{35, 12, 82, 42, 33, 43}
        );
        return multiple_waves;
    }

    public static List<List<Float>> getDominantRatios() {
        List<List<Float>> dom_ratios = new ArrayList<>();
        dom_ratios.add(Arrays.asList(0.0f));
        dom_ratios.add(Arrays.asList(24f/102f, 53f/93f));
        dom_ratios.add(Arrays.asList(12f/82f));
        dom_ratios.add(new ArrayList<Float>());
        dom_ratios.add(new ArrayList<Float>());
        dom_ratios.add(Arrays.asList(68f/91f));
        return dom_ratios;
    }

    public static float[] getAveRatios() {
        float[] ave_ratios = {0.0f, (53f/93f + 24f/102f)/2, 12f/82f, 0.0f, 0.0f, 68f/91f};
        return ave_ratios;
    }

    public static float[] getDrowsyPercentages() {
        float[] drowsy_percentages = {
                0.0f,
                (((53f/93f + 24f/102f)/2) * 0.15f) + 0.85f,
                (12f/82f * 0.15f) + 0.7f,
                0.0f,
                0.0f,
                0.0f  // Intentionally zero
        };
        return drowsy_percentages;
    }

    public static float getFinalDrowsyPercent() {
        float delta_weight = 2.0f / 5;
        float theta_weight = 1.0f / 5;
        float delta_val = ((((53f/93f + 24f/102f)/2) * 0.15f) + 0.85f) * delta_weight;
        float theta_val = (((12f/82f * 0.15f) + 0.7f)) * theta_weight;
        return delta_val + theta_val;
    }

    // Test Execution ==============================================================================

    public static void main (String[] args ) {
        testSetDrowsinessRangeBounds();
        testGetDominantRatios();
//        testCalcAveRatios();  // Second wave calculation is slightly off.
        testCalcDrowsyPercentages();
        testCalcFinalDrowsyPercent();
    }
}


