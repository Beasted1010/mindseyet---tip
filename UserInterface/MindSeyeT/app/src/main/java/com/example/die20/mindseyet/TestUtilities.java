package com.example.die20.mindseyet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.jjoe64.graphview.series.DataPoint;


public class TestUtilities {

    public static void testSunnydayParseRawSignal() {
        String raw_wave = "C,10,20,30,40,50,60;";
        int[] expected = {10, 20, 30, 40, 50, 60};
        int[] results = Utilities.ParseRawSignal(raw_wave);
        if (!Arrays.equals(expected, results)) throw new AssertionError(
                String.format("%s != %s", Arrays.toString(expected), Arrays.toString(results)));
    }

    public static void testRainydayParseRawSignal() {
        int[] expected = null;

        // Missing head
        String raw_wave_head = ",10,20,30,40,50,60;";
        int[] results_head = Utilities.ParseRawSignal(raw_wave_head);
        if (!Arrays.equals(expected, results_head)) throw new AssertionError(
                String.format("%s != %s", Arrays.toString(expected), Arrays.toString(results_head)));

        // Missing values
        String raw_wave_value = "C,10,20,30,50,60;";
        int[] results_value = Utilities.ParseRawSignal(raw_wave_value);
        if (!Arrays.equals(expected, results_value)) throw new AssertionError(
                String.format("%s != %s", Arrays.toString(expected), Arrays.toString(results_value)));

        // Missing delimiter
        String raw_wave_del = "C,10,20,30,4050,60;";
        int[] results_del = Utilities.ParseRawSignal(raw_wave_del);
        if (!Arrays.equals(expected, results_del)) throw new AssertionError(
                String.format("%s != %s", Arrays.toString(expected), Arrays.toString(results_del)));

        // Missing tail
        String raw_wave_tail = "C,10,20,30,40,50,60";
        int[] results = Utilities.ParseRawSignal(raw_wave_tail);
        if (!Arrays.equals(expected, results)) throw new AssertionError(
                String.format("%s != %s", Arrays.toString(expected), Arrays.toString(results)));
    }

    public static void testSunnydayParseRawTransmission() {
        String raw_wave = "C,10,20,30,40,50,60;C,10,20,30,40,50,60;C,10,20,30,40,50,60;";
        List<int[]> expected = Arrays.asList(
                new int[]{10, 20, 30, 40, 50, 60},
                new int[]{10, 20, 30, 40, 50, 60},
                new int[]{10, 20, 30, 40, 50, 60});
        List<int[]> result = Utilities.ParseRawTransmission(raw_wave);
        if (!Arrays.deepEquals(expected.toArray(), result.toArray())) throw new AssertionError(
                String.format("%s != %s", Utilities.brainwavesToString(expected),
                        Utilities.brainwavesToString(result)));
    }

    public static void testRainydayParseRawTransmission() {
        String raw_wave_del = "C,10,20,30,40,50,60;C,10,20,30,40,50,60C,10,20,30,40,50,60;";
        List<int[]> expected_del = Arrays.asList(
                new int[]{10, 20, 30, 40, 50, 60});
        List<int[]> result_del = Utilities.ParseRawTransmission(raw_wave_del);
        if (!Arrays.deepEquals(expected_del.toArray(), result_del.toArray())) throw new AssertionError(
                String.format("%s != %s", Utilities.brainwavesToString(expected_del),
                        Utilities.brainwavesToString(result_del)));

        String raw_wave_no_del = "C,10,20,30,40,50,60C,10,20,30,40,50,60C,10,20,30,40,50,60";
        List<int[]> expected_no_del = new ArrayList<>();
        List<int[]> result_no_del = Utilities.ParseRawTransmission(raw_wave_no_del);
        if (!Arrays.deepEquals(expected_no_del.toArray(), result_no_del.toArray())) throw new AssertionError(
                String.format("%s != %s", Utilities.brainwavesToString(expected_no_del),
                        Utilities.brainwavesToString(result_no_del)));
    }

    public static void testArrayToDataPoint() {
        int[] brainwaves = new int[]{10, 20, 30, 40, 50, 60};
        DataPoint[] expected = new DataPoint[]{
                new DataPoint(1, 10),
                new DataPoint(2, 20),
                new DataPoint(3, 30),
                new DataPoint(4, 40),
                new DataPoint(5, 50),
                new DataPoint(6, 60),
        };
        DataPoint[] result = Utilities.arrayToDataPoints(brainwaves);
        if (!Arrays.toString(expected).equals(Arrays.toString(result))) throw new AssertionError(
                String.format("%s != %s", Arrays.toString(expected), Arrays.toString(result)));

    }

    public static void testBrainwavesToString() {
        List<int[]> input = Arrays.asList(
                new int[]{17, 32, 51, 44, 68, 29},
                new int[]{42, 16, 84, 33, 98, 21},
                new int[]{53, 23, 64, 27, 76, 43}
        );
        String expected = "[17,32,51,44,68,29,]\n" +
                "[42,16,84,33,98,21,]\n" +
                "[53,23,64,27,76,43,]\n";
        String result = Utilities.brainwavesToString(input);
        if (!expected.equals(result)) throw new AssertionError(
                String.format("%s != %s", expected, result));
    }

    public static void testStrToBrainwaves() {
        String input = "17,32,51,44,68,29,\n" +
                "42,16,84,33,98,21,\n" +
                "53,23,64,27,76,43,";
        List<int[]> expected = Arrays.asList(
                new int[]{17, 32, 51, 44, 68, 29},
                new int[]{42, 16, 84, 33, 98, 21},
                new int[]{53, 23, 64, 27, 76, 43}
        );
        List<int[]> result = Utilities.strToBrainwaves(input);
        if (!Arrays.deepEquals(expected.toArray(), result.toArray())) throw new AssertionError(
                String.format("%s != %s", Utilities.brainwavesToString(expected),
                        Utilities.brainwavesToString(result)));
    }

    public static void testIntArrToList() {
        int[] input = {1, 2, 3};
        List<Integer> expected = Arrays.asList(1, 2, 3);
        List<Integer> result = Utilities.intArrToList(input);
        if (!Arrays.deepEquals(expected.toArray(), result.toArray())) throw new AssertionError(
                String.format("%s != %s", expected, result));
    }

    public static void testMaxWaveIndex() {
        int[] input1 = {1, 2, 3};
        int expected1 = 2;
        int result1 = Utilities.maxWaveIndex(input1);
        if (expected1 != result1) throw new AssertionError(
                String.format("%s != %s", expected1, result1));

        int[] input2 = {1, 3, 2};
        int expected2 = 1;
        int result2 = Utilities.maxWaveIndex(input2);
        if (expected2 != result2) throw new AssertionError(
                String.format("%s != %s", expected2, result2));

        int[] input3 = {3, 1, 2};
        int expected3 = 0;
        int result3 = Utilities.maxWaveIndex(input3);
        if (expected3 != result3) throw new AssertionError(
                String.format("%s != %s", expected3, result3));
    }

    public static void testGenerateRange() {
        List<Integer> expected1 = Arrays.asList(0, 1, 2);
        List<Integer> result1 = Utilities.generateRange(3);
        if (!Arrays.deepEquals(expected1.toArray(), result1.toArray())) throw new AssertionError(
                String.format("%s != %s", expected1, result1));

        List<Integer> expected2 = new ArrayList<>();
        List<Integer> result2 = Utilities.generateRange(0);
        if (!Arrays.deepEquals(expected2.toArray(), result2.toArray())) throw new AssertionError(
                String.format("%s != %s", expected2, result2));
    }

    public static void testSwap() {
        int[] input = {0, 1, 2};
        int[] expected1 = {2, 1, 0};
        int[] result1 = Utilities.swap(0, 2, input);
        if (!Arrays.equals(expected1, result1)) throw new AssertionError(
                String.format("%s != %s", Arrays.toString(expected1), Arrays.toString(result1)));

        int[] input2 = {0, 1, 2};
        int[] expected2 = {0, 1, 2};
        int[] result2 = Utilities.swap(1, 1, input2);
        if (!Arrays.equals(expected2, result2)) throw new AssertionError(
                String.format("%s != %s", Arrays.toString(expected2), Arrays.toString(result2)));
    }

    public static void main (String[] args ) {
        TestUtilities.testSunnydayParseRawSignal();
        TestUtilities.testRainydayParseRawSignal();
        TestUtilities.testSunnydayParseRawTransmission();
        TestUtilities.testRainydayParseRawTransmission();
        TestUtilities.testArrayToDataPoint();
        TestUtilities.testBrainwavesToString();
        TestUtilities.testStrToBrainwaves();
        TestUtilities.testIntArrToList();
        TestUtilities.testMaxWaveIndex();
        TestUtilities.testGenerateRange();
        TestUtilities.testSwap();
    }
}
