package com.example.die20.mindseyet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.lang.Math;

public class TestFunctional {

    private final static int NUM_WAVES = 6;
    private final static int[] UNCONSIDERED_WAVES = {0, 5};  // Epsilon and Gamma waves.

    /**
     * Generates sample brainwaves to mimic brainwaves from hardware.
     * @param sample_len    The number of brainwave entries to be generated.
     * @param sleep_stage   The approximate sleep stage desired observed by the majority of the
     *                      dominant waves. (0-5) as seen below.
     *                          0: Epsilon
     *                          1: Delta
     *                          2: Theta
     *                          3: Alpha
     *                          4: Beta
     *                          5: Gamma
     * @param value_range   The range in which each brainwave frequency can take from 0-(value_range)
     * @param ss_ratio      The percentage of entries in which the dominant wave matches the desired
     *                      sleep stage. The other entries will have randomly be dispersed between
     *                      the other values.
     * @param limit_to_ss   If True, no dominant waves will be Gamma or Epsilon. This is to better
     *                      match the fact that these shouldn’t be observed unless there is a glitch.
     * @param shuffle       If true, the brainwave entries will be shuffled.
     * @return List of generated brainwaves.
     */
    public static List<int[]> generateWaves(int sample_len, int sleep_stage, int value_range,
                                       float ss_ratio, boolean limit_to_ss, boolean shuffle) {

        List<int[]> brainwaves = new ArrayList<>();
        Random rand = new Random();

        // Generate entries with desired sleep stage max frequency.
        int ss_count = ss_ratio <= 1.0f ? (int) Math.ceil(ss_ratio * sample_len) : sample_len;
        for (int i = 0; i < ss_count; i++) {
            brainwaves.add(generateEntry(value_range, sleep_stage));
        }

        // Generate entries that are not desired sleep stage.
        List<Integer> max_options = Utilities.generateRange(NUM_WAVES);
        max_options.remove(sleep_stage);
        if (limit_to_ss) {
            for (int i : UNCONSIDERED_WAVES)
                max_options.remove(max_options.indexOf(i));
        }
        for (int i = 0; i < (sample_len - ss_count); i++) {
            int rand_max = max_options.get(rand.nextInt(max_options.size()));
            brainwaves.add(generateEntry(value_range, rand_max));
        }

        if (shuffle) Collections.shuffle(brainwaves);
        return brainwaves;
    }

    /**
     * Generates a set of frequencies that represents a single brainwave entry.
     * @param value_range   Upper bounds of values to be generated starting from 0.
     * @param max_wave      The frequency to contain the largest value.
     * @return Brainwave entry.
     */
    private static int[] generateEntry(int value_range, int max_wave) {
        Random rand = new Random();
        int[] entry = new int[NUM_WAVES];

        for (int i = 0; i < entry.length; i++) {
            entry[i] = rand.nextInt(value_range);
        }

        int max_idx = Utilities.maxWaveIndex(entry);
        return Utilities.swap(max_idx, max_wave, entry);
    }

    public static void main (String[] args ) {
        int sample_len = 10;
        int sleep_stage = 2;
        int value_range = 100;
        float ss_ratio = 0.7f;
        boolean limit_to_ss = true;
        boolean shuffle = false;
        List<int[]> brainwaves = generateWaves(
                sample_len, sleep_stage, value_range, ss_ratio, limit_to_ss, shuffle);

        System.out.print(String.format("Sample Length: %s\n" +
                "Sleep Stage: %s\n" +
                "Value Range: 0 - %s\n" +
                "Sleep Stage Ratio: %s\n" +
                "Limit to Sleep Stage? %s\n" +
                "Shuffle Brainwave Entries? %s\n\n" +
                "Generated Brainwaves: \n%s",
                sample_len, sleep_stage, value_range, ss_ratio, limit_to_ss, shuffle,
                Utilities.brainwavesToString(brainwaves)));
    }
}
