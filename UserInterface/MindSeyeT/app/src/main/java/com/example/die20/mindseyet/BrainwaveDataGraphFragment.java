package com.example.die20.mindseyet;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Random;

public class BrainwaveDataGraphFragment extends Fragment implements View.OnClickListener {
    private final static String TAG = "BrainwaveActivity";
    private DrowsinessGraph drows_graph;
    private BrainwaveGraph brainwave_graph;
    private Button betaButton;
    private Button alphaButton;
    private Button thetaButton;
    private Button deltaButton;


    Button test_graph_button;
    SigInterpreter siginterp;
    BrainwaveGenerator brain_gen = BrainwaveGenerator.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        drows_graph = new DrowsinessGraph();
        brainwave_graph = new BrainwaveGraph();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.brainwave_data_graph, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        //Initialize data for the fragment functions, i.e. initGraph() etc.
        siginterp = SigInterpreter.getInstance();
        drows_graph.onViewCreate(view);
        brainwave_graph.onViewCreate(view);

        betaButton = (Button) view.findViewById(R.id.beta_button);
        betaButton.setOnClickListener(this);
        alphaButton = (Button) view.findViewById(R.id.alpha_button);
        alphaButton.setOnClickListener(this);
        thetaButton = (Button) view.findViewById(R.id.theta_button);
        thetaButton.setOnClickListener(this);
        deltaButton = (Button) view.findViewById(R.id.delta_button);
        deltaButton.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        drows_graph.onResume();
        brainwave_graph.onResume();
    }

    @Override
    public void onPause() {
        drows_graph.onPause();
        brainwave_graph.onPause();
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.beta_button:
                updateWaveThread(SleepStage.BETA);
                break;
            case R.id.alpha_button:
                updateWaveThread(SleepStage.ALPHA);
                break;
            case R.id.theta_button:
                updateWaveThread(SleepStage.THETA);
                break;
            case R.id.delta_button:
                updateWaveThread(SleepStage.DELTA);
                break;
        }
    }

    /**
     * Update the wave generating thread to use the desired sleep stage.
     * If the same sleep stage is selected, the thread will be killed.
     * @param ss
     */
    private void updateWaveThread(SleepStage ss) {
        SleepStage curr_ss = brain_gen.currentSleepStage();
        if (curr_ss == null || curr_ss != ss) {
            brain_gen.generateWaves(ss);
        } else {
            brain_gen.killWaveThread();
        }
    }

    // Generates random wave data and adds to siginterp
    private void testGraph() {
        Random r = new Random();
        int[] rand_brainwaves = new int[siginterp.NUM_WAVES];
        for (int i = 0; i < siginterp.NUM_WAVES; i++) {
            rand_brainwaves[i] = r.nextInt(100);
        }
        siginterp.updateWaves(rand_brainwaves);
    }
}