package com.example.die20.mindseyet;

public enum SleepStage {
    EPSILON,
    DELTA,
    THETA,
    ALPHA,
    BETA,
    GAMMA
}
