package com.example.die20.mindseyet;

import android.graphics.Color;
import android.os.Handler;
import android.view.View;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

public class BrainwaveGraph {

    private final Handler mHandler = new Handler();
    private int RefreshRate;
    private Runnable mTimer;
    private BarGraphSeries<DataPoint> mSeries;
    private SigInterpreter siginterp;

    public void onViewCreate(View view) {
        GraphView graph = (GraphView) view.findViewById(R.id.latest_wave_graph);
        initGraph(graph);
        RefreshRate = view.getResources().getInteger(R.integer.refresh_rate_ms);
    }

    public void initGraph(GraphView graph) {
        siginterp = SigInterpreter.getInstance();

        graph.setTitle("Latest Brainwave Activity");
        graph.setTitleTextSize(48.0f);

        StaticLabelsFormatter sf = new StaticLabelsFormatter(graph);
        sf.setHorizontalLabels(new String[]{"", "Epsilon", "Delta",
                "Theta", "Alpha", "Beta", "Gamma", ""});

        GridLabelRenderer glr = graph.getGridLabelRenderer();
        glr.setVerticalAxisTitle("Amplitude");
        glr.setVerticalAxisTitleTextSize(28.0f);
        glr.setHorizontalAxisTitle("Brainwaves");
        glr.setHorizontalAxisTitleTextSize(28.0f);
        glr.setHorizontalLabelsAngle(155);
        glr.setNumHorizontalLabels(8);
        glr.setGridColor(Color.LTGRAY);
        glr.setLabelFormatter(sf);

        Viewport vp = graph.getViewport();
        vp.setXAxisBoundsManual(true);
        vp.setYAxisBoundsManual(true);
        vp.setMaxX(7);
        vp.setMaxY(100.0);

        DataPoint[] init_points = new DataPoint[siginterp.NUM_WAVES];
        int i = 0;
        for (int val: siginterp.getLatestWaves()) {
            init_points[i] = new DataPoint(i + 1, val);
            i++;
        }

        mSeries = new BarGraphSeries<>(init_points);
        mSeries.setColor(Color.parseColor("#d0e1f9"));
        mSeries.setSpacing(10);
        mSeries.setDrawValuesOnTop(true);
        mSeries.setValuesOnTopColor(Color.WHITE);
        mSeries.setValuesOnTopSize(25.0f);
        graph.addSeries(mSeries);
    }

    public void onResume() {
        mTimer = new Runnable() {
            @Override
            public void run() {
                mSeries.resetData(Utilities.arrayToDataPoints(siginterp.getLatestWaves()));
                mHandler.postDelayed(this, RefreshRate);
            }
        };
        mHandler.postDelayed(mTimer, 1500);
    }

    public void onPause() {
        mHandler.removeCallbacks(mTimer);
    }
}

