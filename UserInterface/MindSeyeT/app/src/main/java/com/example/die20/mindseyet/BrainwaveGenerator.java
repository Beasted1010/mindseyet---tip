package com.example.die20.mindseyet;

import java.util.List;

public class BrainwaveGenerator {

    private static BrainwaveGenerator brain_gen;
    private static SigInterpreter siginterp;
    private static boolean stopThread = false;
    private static Thread genThread;
    private static int refreshRate = 500;

    // Generator values
    private static int sample_len = 10;
    private static SleepStage sleep_stage;
    private static int value_range = 100;
    private static float ss_ratio = 0.9f;
    private static boolean limit_to_ss = true;
    private static boolean shuffle = true;

    public static BrainwaveGenerator getInstance() {
        if (brain_gen == null) brain_gen = new BrainwaveGenerator();
        return brain_gen;
    }

    public BrainwaveGenerator() {
        siginterp = SigInterpreter.getInstance();
    }

    // Generate brainwaves continuously for testing.
    public void generateWaves(final SleepStage ss) {

        // Ensure previous thread is killed
        killWaveThread();

        stopThread = false;
        sleep_stage = ss;
        genThread  = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopThread) {
                    try {
                        List<int[]> brainwaves = TestFunctional.generateWaves(
                                sample_len, sleep_stage.ordinal(),
                                value_range, ss_ratio,
                                limit_to_ss, shuffle);
                        siginterp.updateWaves(brainwaves);
                        Thread.sleep(refreshRate);
                    }
                    catch (Exception e) {
                        stopThread = true;
                    }
                }
            }
        });
        genThread.start();
    }

    public void killWaveThread() {
        if (genThread != null && genThread.isAlive()) {
            try {
                stopThread = true;
                genThread.join(500);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }

    public SleepStage currentSleepStage() {
        if (genThread == null || !genThread.isAlive()) {
            return null;
        } else {
            return sleep_stage;
        }
    }
}
