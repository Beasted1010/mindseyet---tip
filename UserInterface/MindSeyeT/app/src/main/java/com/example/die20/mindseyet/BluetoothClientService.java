package com.example.die20.mindseyet;

import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.UUID;

public class BluetoothClientService extends IntentService {

    // Objects necessary for handling bluetooth connection
    private BluetoothServerSocket mmServerSocket;
    private BluetoothSocket socket, serverSocket;
    private BluetoothAdapter btAdapter;
    private BluetoothDevice btDevice;
    private boolean deviceConnected;
    private boolean stopThread;
    private InputStream inputStream;
    byte buffer[];

    // Identifying information for the device being connected to
    //private final UUID PORT_UUID = UUID.fromString("7172bcd4-13bd-4617-98a2-ef201f782cfa");
    private final UUID PORT_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");//Serial Port Service ID for HC-05
    //private final String DEVICE_NAME="EnriquesGalaxy";
    private final String DEVICE_NAME="MindSeyeT";

    private SigInterpreter sigInterp;

    // Set the threshold drowsiness level for sending a notifcation
    private final double threshold = .50;

    // A constructor is required, and must call the super IntentService(String)</a></code>
    // constructor with a name for the worker thread.
    public BluetoothClientService() {

        super("BluetoothClientService");
    }

    // The IntentService calls this method from the default worker thread with
    // the intent that started the service. When this method returns, IntentService
    // stops the service, as appropriate.
    @Override
    protected void onHandleIntent(Intent intent) {

        // Do the work of the service here
        // TODO - Inflate a fragment here that will let us send messages to the screen
        // or maybe start a new intent to pass back to the main activity and the text view
        /*
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.toast_layout, null);
        */

        // Get the current instance of the SigInterpreter Class
        sigInterp = SigInterpreter.getInstance();

        // Acquire what we need for the bluetooth connection
        btAdapter = BluetoothAdapter.getDefaultAdapter();

        // establish a BT connection and start listening for data
        try {
            BTStartClient();
            BTListenForData();

        } catch (Exception e) {
            // Restore interrupt status.
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void onCreate(){
        Log.i("bluetooth", "BlueTooth Service created");
        super.onCreate();
    }

    public boolean BTInit() {

        boolean found=false;
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(),"Device doesnt Support Bluetooth",Toast.LENGTH_SHORT).show();
        }

        // If the bluetooth adapter is not enabled, we'll need to stop the service and send an error
        if(!bluetoothAdapter.isEnabled()) {
            Log.e("bluetooth", "Bluetooth adapter is not enabled");
            stopSelf();
            // TODO - Figure out how to send an error from here
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
        if(bondedDevices.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please Pair the Device first", Toast.LENGTH_SHORT).show();
        } else {
            for (BluetoothDevice iterator : bondedDevices) {
                if(iterator.getName().equals(DEVICE_NAME)) {
                    btDevice = iterator;
                    found = true;
                    break;
                }
            }
        }
        Log.i("bluetooth","Value of found: " + found);
        return found;
    }

    // This function creates a socket for listening over the bluetooth adapter and tries to connect
    public boolean BTConnect() {

        boolean connected = true;
        try {
            socket = btDevice.createRfcommSocketToServiceRecord(PORT_UUID);
            socket.connect();
        } catch (IOException e) {
            Log.e("bluetooth", "Could not connect the socket: ");
            e.printStackTrace();
            connected=false;
        }

        if(connected) {
            try {
                inputStream = socket.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
                connected = false;
            }
        } else {
            Log.e("bluetooth", "BT is Not Connected");
        }

        Log.i("bluetooth", "Connected is : " + connected);
        return connected;
    }

    // This says "You're a client, try to connect to the server"
    public void BTStartClient() {

        Log.i("bluetooth", "Trying to connect...");
        if(BTInit()) {
            if(BTConnect()) {
                deviceConnected=true;
                Log.i("bluetooth","Connection opened!");
                // change the connection indicator to be green
                // TODO - figure out how to send this to the UI fragment
                // SwitchConnectIndic();
                // TODO - Right now I can connect once and then after that there is a connection error
                // So either the server or the listener is not maintaining the connection correctly.
            } else {
                Log.e("bluetooth","BTConnect Failed");
            }

        } else {
            Log.e("bluetooth","BTInit Failed");
        }
    }

    // This function checks the inputStream for data and decides how to handle it
    public void BTListenForData() {

        Log.i("bluetooth", "Listening for data...");
        final Handler handler = new Handler();
        stopThread = false;
        buffer = new byte[1024];
        Thread thread  = new Thread(new Runnable() {
            public void run() {
                while(!Thread.currentThread().isInterrupted() && !stopThread) {
                    try {
                        int byteCount = inputStream.available();
                        if(byteCount > 0) {
                            //Log.i("bluetooth", "Message received from server.");
                            byte[] rawBytes = new byte[byteCount];
                            inputStream.read(rawBytes);
                            final String dataString = new String(rawBytes,"UTF-8");
                            // The code inside the handler will continue to run until the thread is stopped

                            // First we need to parse the raw transmission to extract the int values, then call update waves to update the graph
                            try {
                                Log.i("bluetooth", "Datastring is: " + dataString);

                                // Update the siginterp singleton for the graphs
                                sigInterp.updateWaves(Utilities.ParseRawTransmission(dataString));

                            } catch (Exception e) {
                                // Ignore all invalid inputs for now.
                                e.getStackTrace();
                                Log.e ("bluetooth", e.getMessage());
                            }
                        }
                        try {
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                        }
                    } catch (IOException ex) {
                        stopThread = true;
                        stopSelf();
                    }
                }
            }
        });
        thread.start();
    }
}

