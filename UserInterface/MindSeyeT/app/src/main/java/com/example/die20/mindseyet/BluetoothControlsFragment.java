package com.example.die20.mindseyet;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class BluetoothControlsFragment extends Fragment implements View.OnClickListener{
    public FragmentCommunicator fComm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.bluetooth_controls, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fComm = (FragmentCommunicator) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FragmentCommunicator");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //This code attaches listeners to all the buttons in the bluetooth fragment
        Button listen_button = (Button) getActivity().findViewById(R.id.listen_button);
        listen_button.setOnClickListener(this);
    }

    //This switch statement binds the buttons to the fragment communicator functions
    public void onClick(final View v) {

        switch (v.getId()) { //check for what button is pressed
            case R.id.listen_button:
                fComm.BTListen();
            break;
        }
    }

    //Override these methods in the Main Activity to have the bluetooth code running there,
    //so that it persists as we swap fragments out...
    public interface FragmentCommunicator{

        public void BTListen();
    }
}