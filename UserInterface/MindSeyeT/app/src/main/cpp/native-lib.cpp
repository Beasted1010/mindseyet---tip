#include <jni.h>
#include <string>

#include "AppState.h"
#include "common.h"

// TODO: An attempt at making it so we can just list what functions we want to be available in the JNI
// TODO: that way we don't have to have an ugly, long function name for each function we want
void testNative(){}
// List of JNINativeMethods
static JNINativeMethod methods[] = {
        {"testNative", "()", (void*)testNative},
};


// Forward declarations
const char* CreateString();



// Native Functions

// NOTE: The following is the typical means of creating a native-method able to be called by JNI
// extern "C"
//           - Allows linkage to JNI
//              - https://stackoverflow.com/questions/1041866/what-is-the-effect-of-extern-c-in-c
//           - REQUIRED
// JNIEXPORT
//           - Expands to compiler directives required to ensure given function is exported properly
//           - Ensures the function is visible in symbols table
//           - Is required if you don't use RegisterNatives family of functions
//           - TODO: May want to use env->RegisterNatives(...) to catch wrong function names earlier
//              - https://stackoverflow.com/questions/19422660/when-to-use-jniexport-and-jnicall-in-android-ndk
// <type>
//           - The return type of the function
//           - REQUIRED
// JNICALL
//           - Expands to compiler directives required to ensure function is treated properly for call
//           - Ensures function uses correct calling convention based on architecture
//           - REQUIRED


// NOTE: The following is the arguments to the JNI native method
//        1. - MUST START WITH: JNI interface pointer -> Of type JNIENV*
//              - We manipulate Java objects using this interface pointer
//        2. - Depends on whether function is static or nonstatic
//              - Nonstatic: Must be a reference (this) to the object
//              - Static: Must be a reference (this) to the java class
//      REST - Regular Java method arguments


// If want to use RegisterNatives instead of using JNICALL, add call to RegisterNative below
// TODO: If we use this, we would be able to name our methods as we wish without nasty long Java convention
// TODO: RegisterNatives currently doesn't work. I need to look more into this function
JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    jint jni_version = JNI_VERSION_1_6;

    JNIEnv* env;
    if(vm->GetEnv(reinterpret_cast<void**>(&env), jni_version ) != JNI_OK)
    {
        return -1;
    }

    // Get jclass with env->FindClass
    // Register methods with env->RegisterNatives
    jclass clazz = env->FindClass("com/example/die20/mindseyet/MainActivity");
    //env->RegisterNatives(clazz, methods, 1);

    return jni_version ;
}


extern "C"
JNIEXPORT void JNICALL Java_com_example_die20_mindseyet_MainActivity_cppMain(
        JNIEnv* env,
        jobject /* this */)
{
    // TODO: Thea app creation/deletion are placeholders, we will probably want to make these
    // TODO: individual native functions so that we can call these during app start/close
    // TODO: We likely won't even use them anyways, Java will probably handle this all
    // TODO: It is looking like a lot of our code will be put in the Java files and only handle
    // TODO: optimizations and heavy calculations in C++ code.
    // TODO: We could do everything in C++ but I still need to figure out how to best structure that


    // Create the application state
    struct AppState state = CreateAppState();

    // Destroy the application state
    // TODO: Placeholder only -> This should be done upon closing the application
    DestroyAppState(&state);
}

extern "C"
JNIEXPORT jstring JNICALL Java_com_example_die20_mindseyet_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */)
{
    std::string hello = "Hello from C++";

    return env->NewStringUTF(CreateString());
}


/* Definitions of Forward Declarations */

const char* CreateString()
{
    LOG_INFO("Hello there everyone! I was logged by a macro!");
    return "ANDROID IS AWESOME!";
}

