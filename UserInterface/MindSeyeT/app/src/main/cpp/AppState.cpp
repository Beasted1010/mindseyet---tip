//
// Created by die20 on 9/15/2018.
//


#include "AppState.h"
#include "common.h"

struct AppState CreateAppState()
{
    LOG_INFO("Initializing AppState...");
    struct AppState state;

    state.userInterface.alerts = NULL;
    state.userInterface.controls = NULL;

    LOG_INFO("AppState Initialized...");
    return state;
}


void DestroyAppState(struct AppState* state)
{
    LOG_INFO("Destroying AppState...");



    LOG_INFO("AppState Destroyed...");
}
