/* Created on 9/15/2018.
Summary: All the commonly used macros, functions, symbols, structures, etc. throughout project
DETAILS: This file contains things that potentially all files could use. Serves as simple #include
*/

#ifndef MINDSEYET_COMMON_H
#define MINDSEYET_COMMON_H

#include <android/log.h>

#define LOG_TAG "APP_LOGGING"

// #defining these so that we can port to another platform without being tied to Android's logging
// Comment out things after "LOG_DEBUG(...)" or "LOG_ERROR(...)" to remove logging
#ifdef DEBUG
    #define LOG_DEBUG(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#else
    #define LOG_DEBUG(...)
#endif

#ifdef ERROR
    #define LOG_ERROR(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#else
    #define LOG_ERROR(...)
#endif

#ifdef WARN
    #define LOG_WARNING(...) __android_log_print(ANDROID_LOG_WARNING, LOG_TAG, __VA_ARGS__)
#endif

#ifdef VERBOSE
    #define LOG_INFO(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#else
    #define LOG_INFO(...)
#endif



#endif //MINDSEYET_COMMON_H
