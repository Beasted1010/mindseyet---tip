/* Created on 9/15/2018.
Summary: Maintains the state of the application.
DETAILS: Defines exactly what the app should do in any given state from frame to frame.

TODO: This file is meant to be modified, I just threw it together to serve as a start.
TODO: Since we are using Android Studio a lot of the application UI logic will be done in Java.
*/

#ifndef MINDSEYET_APPSTATE_H
#define MINDSEYET_APPSTATE_H

#include "stdint.h"

// Constants
// ...



// Structures

// Potential application locations, not necessary that all be used
enum UI_Locations
{
    LOGIN,
    SETTINGS,
    ALERT_CENTER,
    LIVE_DATA,
    LIVE_INSIGHTS,
    INTERPRETED_INSIGHTS,
    NUM_LOCATIONS // Tracks the number of locations we have. Add any new locations above this value.
};

// Potential controls that would populate a UserInterface
enum Controls
{
    PUSH_BUTTON,
    CHECK_BOX,
    SLIDER,
    TEXT_BOX,
    DISPLAY_BOX,
    // ...
    NUM_CONTROLS // Tracks the number of controls we have. Add any new locations above this value.
};

struct UserInterface
{
    enum UI_Locations userLocation;

    // NOTE: This is by no means final, a perhaps better idea is to define individual structures for each location
    char** controls; // e.g. [ ['LIVE_DATA', 'PUSH_BUTTON', 'TEXT_BOX', 'SLIDER', 'DISPLAY_BOX'], ['LIVE_INSIGHT', 'DISPLAY_BOX'], ['ALERT_CENTER', 'DISPLAY_BOX'], ['SETTINGS', 'SLIDER', ...] ]

    char** alerts;
};

struct Hardware
{
    uint8_t signalStrength;
    float* channelData;
    uint8_t numChannels;
    uint8_t numSamples;
};

struct WaveData
{
    uint32_t epsilon; // <0.5 Hz
    uint32_t delta; // 0.5-3 Hz
    uint32_t theta; // ~4-7 Hz
    uint32_t lowAlpha; // ~8-9 Hz
    uint32_t highAlpha; // ~10-12 Hz
    uint32_t lowBeta; // 13-17 Hz
    uint32_t highBeta; // 18-30 Hz
    uint32_t lowGamma; // 31-40 Hz
    uint32_t midGamma; // 41-50 Hz
};

struct AppState
{
    struct UserInterface userInterface;

    struct WaveData waveData;

    struct Hardware hardware;


};

//void InitializeAppState(struct AppState* state); // Start of application
struct AppState CreateAppState(); // Start of application
void UpdateAppState(); // Every frame
void ReadWaveData();
void DestroyAppState(struct AppState* state); // End of application





#endif //MINDSEYET_APPSTATE_H
