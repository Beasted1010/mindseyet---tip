#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

/*
	NOTE TO USER: The algorithms used here to find "StageOne", "StageTwo", etc.
		sleep states are all drastically simplified and will need modiciation.
		For example, Stage One sleep has a high amount of theta activity
		(it is the dominant wave), but we would want to ensure that this wave
		stays dominant for some amount of time, there may also be other things
		to consider in the algorithm of determining "InStageOne".
		
		This is meant to represent only how to compare the brain wave data
		that will be received from the bluetooth module, NOT how to actually
		interpret the waves.
		
		Jargon clarification:
			Stage 1 = "N1", Stage 2 = "N2", Stage 3 and Stage 4 = "N3"
            
    DATA FORMAT RECEIVED:
        epsilon, delta, theta, lowAlpha, highAlpha, lowBeta, highBeta, lowGamma, midGamma
        
    DATA RECEPTION RATE:
        Once every 4 seconds -> Subject to change (shouldn't matter though, whenever new data is available on serial port you'd grab it, but details are wavy at the moment)
    
    EXAMPLE DATA RECEPTION OVER BLUETOOTH ON ANDROID:
        https://stackoverflow.com/questions/13450406/how-to-receive-serial-data-using-android-bluetooth
        This includes sample code.
			
	RECOMMENDED USE:
		Every time updated wave data comes across bluetooth, 2 main things should happen
			0. InitializeWaveData -> Some known initial values
			
			LOOP
			1. UpdateWaveData() -> Updates waveData with new values from the microcontroller
			2. SortWaveDominance() -> A useful sorted array of wave indicies
			3. Run interpretation algorithm -> Update understanding of user's mental state
				We will want to track their state over some time
				Because a spike in Delta waves for one reading doesn't necessarily
				mean that the user is in deep sleep. Before sending an alert we want
				to be confident in the user's state, so say if we have 10 our of the past 12
				readings return Theta as the dominant wave (and Beta is relatively low), 
				then perhaps we can be confident in saying that the user is at 
				least approaching Stage 1 sleep.
*/


// Take in wave data, return yes/no
bool EyesClosed(const uint32_t* waveData);
bool InStageOne(const uint32_t* waveData);
bool InStageTwo(const uint32_t* waveData);

// Take in wave data, return frequency
uint8_t FindDominantWave(const uint32_t* waveData);

// Sort the brain wave array based on relative magnitudes, use out parameter
void SortWaveDominance(const uint32_t* waveData, uint8_t* outSortedDataIndexes);

// Provide some initial values for wave data (instead of gibberish)
void InitializeWaveData(uint32_t* waveData);

// This should be called each time new wave data is available over bluetooth
void UpdateWaveData(uint32_t* waveData);

// This structure is here in case you wanta more explicit collection of waveData
// This is currently not used
/*struct WaveData
{
	uint16_t maxFrequency;
	
	// Values within represent relative magnitudes
	uint32_t epsilon; // <0.5 Hz
	uint32_t delta; // 0.5-3 Hz
	uint32_t theta; // ~4-7 Hz
	uint32_t lowAlpha; // ~8-9 Hz
	uint32_t highAlpha; // ~10-12 Hz
	uint32_t lowBeta; // 13-17 Hz
	uint32_t highBeta; // 18-30 Hz
	uint32_t lowGamma; // 31-40 Hz
	uint32_t midGamma; // 41-50 Hz
};*/

enum WaveData
{
	EPSILON, // <0.5 Hz
	DELTA, // 0.5-3 Hz
	THETA, // ~4-7 Hz
	LOW_ALPHA, // ~8-9 Hz
	HIGH_ALPHA, // ~10-12 Hz
	LOW_BETA, // 13-17 Hz
	HIGH_BETA, // 18-30 Hz
	LOW_GAMMA, // 31-40 Hz
	MID_GAMMA, // 41-50 Hz
	
	NUM_WAVES // Keeps track of how many brain waves are in this enumeration
};

int main(int argc, char** argv)
{
	uint32_t waveData[NUM_WAVES];
	
	InitializeWaveData(waveData);
	
	return 0;
}

// TODO: May want to change this to something more appropriate
//		 Ultimately, it may not matter.
void InitializeWaveData(uint32_t* waveData)
{
	for( int i = 0; i < NUM_WAVES; i++ )
	{
		waveData[i] = 0;
	}
}

uint8_t FindDominantWave(const uint32_t* waveData)
{
	uint8_t dominantWave = 0; // Index
	
	// Loop through all of the brain waves, find the one with highest magnitude
	for( int i = 1; i < NUM_WAVES; i++ )
	{
		// Does the current brain wave have a higher magnitude than the current dominant wave?
		if( waveData[i] > waveData[dominantWave] )
		{
			dominantWave = i;
		}
	}
}

// Sort the waveData based on relative magnitudes
void SortWaveDominance(const uint32_t* waveData, uint8_t* outSortedDataIndexes)
{
	/* Some sorting algorithm goes in here
	
	outSortedDataIndexes will have the sorted indecies from waveData
		  E.g. so you can use the array of indicies like ...
				outSortedDataIndexes[0] = THETA
					Here if index 0 has the smallest value, then THETA is the weakest wave
		Due to the enum we have, we can then easily test for most dominant wave
			as well as search through sorted indicies for a specific brain wave
			e.g. for(...) if(sortedIndexes[i] == BETA) { return i }
				Now we know the relative magnitude of BETA
					i.e. where does the index fall in the range of 0 to NUM_WAVES
					E.g. If 0 is the smallest magnitude (weakest wave) and BETA came out to be
						at index 6 and we have 9 individual waves, then it is more dominant than not
	*/
	
}

// Further interpretaion needed, consider https://en.wikipedia.org/wiki/Alpha_wave
// Essentially, high alpha activity means eyes are closed, further development of this function may favor HIGH_ALPHA over LOW_ALPHA frequencies (or vice versa)
bool EyesClosed(const uint32_t* waveData)
{
	uint8_t dominantWave = FindDominantWave(waveData);
	return (dominantWave == LOW_ALPHA || dominantWave == HIGH_ALPHA);
}

// Drastic simplified version of noticing a stage one sleep pattern
bool InStageOne(const uint32_t* waveData)
{
	// Is the dominant wave the theta wave? If so that is a sign of Stage 1 sleep.
	return FindDominantWave(waveData) == THETA;
}



// Information about Sleep Spindles and K-Complexes: https://www.tuck.com/sleep-spindles/

// Detect Sleep Spindle Patterns
bool FoundSleepSpindles(const uint32_t* waveData)
{
	
}

// Detect K-Complex Patterns
bool FoundKComplexes(const uint32_t* waveData)
{
	
}

bool InStageTwo(const uint32_t* waveData)
{
	return FoundSleepSpindles(waveData) && FoundKComplexes(waveData);
}













